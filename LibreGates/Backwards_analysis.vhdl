-- LibreGates/Backwards_analysis.vhdl
-- created mar. août 25 02:51:59 CEST 2020 by whygee@f-cpu.org
-- version mar. sept.  1 03:53:22 CEST 2020
--
-- The "backwards" component adds the necessary properties to a
-- latche's loopback wire to allow a proper analysis of logic depth,
-- observability and controllability by breaking the feedback loop
-- of certain latches and flip-flops.
-- Basically it creates a new sub-net with depth -1

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity backwards is
  generic( Gate_Number : integer := Back_Census);
  port( A : in std_logic;
        Y : out std_logic);
end backwards;

architecture analysis of backwards is
  signal back : integer := 0; -- dummy number
begin
  back <= RegisterBack(back'instance_name, Gate_Number);
  Y <= GoBackwards(A, Gate_Number);
end analysis;
