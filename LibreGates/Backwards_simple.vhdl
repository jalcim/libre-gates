-- LibreGates/Backwards_simple.vhdl
-- created mar. août 25 02:51:59 CEST 2020 by whygee@f-cpu.org
-- version mar. sept.  1 04:02:57 CEST 2020
--
-- The "backwards" component is just a wire for most purposes but
-- it can be replaced by a meta-component that adds the necessary
-- properties to the wire to allow a proper analysis of logic depth,
-- observability and controllability by breaking the feedback loop
-- of certain latches and flip-flops.

Library ieee;
    use ieee.std_logic_1164.all;

entity backwards is
  port( A : in std_logic;
        Y : out std_logic);
end backwards;

architecture simple of backwards is
begin
  Y <= A; -- just a wire.
end simple;
