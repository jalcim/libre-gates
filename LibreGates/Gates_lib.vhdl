-- LibreGates/Gates_lib.vhdl
-- created 20190618
-- version 20190809 (restart)
-- version sam. août 10 08:06:58 CEST 2019
-- version dim. août 11 17:36:51 CEST 2019
-- version lun. août 12 18:36:04 CEST 2019
-- version mar. août 13 23:14:14 CEST 2019
-- version mar. nov. 19 07:04:27 CET 2019 : off-by-one in histogram display
-- version jeu. nov. 21 03:50:23 CET 2019 : changing from fast/trace to run_mode,
--    lookitupX_fast merged,  Gate_Census now with no parameter list
-- version dim. déc.  1 14:00:01 CET 2019 : preparing for 64 bits for histograms and changes
-- version ven. déc. 13 17:07:51 CET 2019 : adding 16-entries LUTs
-- version lun. mars 23 03:47:01 CET 2020 : v2.8 adds "simple" gate types (no LUT)
-- version mar. juil. 14 23:02:32 CEST 2020 : added function ComputeLUT16()
-- version sam. août 29 23:57:38 CEST 2020 : v2.10 with the new internal representation system
-- version lun. août 31 22:17:01 CEST 2020 : functions trim_name and trim_spaces moved to SLV_utils
--     avoiding naming collision with display_histogram => display_gates_histograms
--      moved all the PA3-specific code to PA3_gategen.vhdl which is now Gates_lib.vhdl
-- version jeu. sept.  3 03:30:57 CEST 2020 : implement the Backwards mechanics
-- version mer. nov. 11 14:20:25 CET 2020 : renamed some
--      variable names to remove "declaration hides variable" warnings
-- version mar. déc.  1 22:28:09 CET 2020 : new netlist probe.
-- version mer. déc.  9 04:28:51 CET 2020 : netlist probe version 2B, "OR" replaces "AND" logic
--
-- Released under the GNU AGPLv3 license
--
-- This file is the root library that is used by most other files
-- that support analysis/tracing/etc. such as Netlist_lib.vhdl,
-- PA3_gategen.vhdl, PA3_components.vhdl and PA3_definitions.vhdl.

Library ieee;
    use ieee.std_logic_1164.all;
use STD.textio.all;
Library SLV;
    use SLV.SLV_utils.all;
Library Histo;
    use Histo.Histograms.all;

package Gates_lib is

  type integer_array is array(integer range <>) of integer;
  type integer_array_ptr is access integer_array;

  subtype LookupType1  is std_logic_vector(0 to  1);
  subtype LookupType2  is std_logic_vector(0 to  3);
  subtype LookupType4  is std_logic_vector(0 to  7);
  subtype LookupType8  is std_logic_vector(0 to 15);
  subtype LookupType16 is std_logic_vector(0 to 31);

  type enum_run_modes is (
    fast, trace, probe
  );
  shared variable run_mode : enum_run_modes := fast;

  type enum_gate_kind is (
    Gate_boolean,
    Gate_register,
    Gate_backwards
  );

  type KindShorthandType is array(enum_gate_kind) of string(1 to 4);
  constant KindShorthands : KindShorthandType := ( "Bool", "Reg.", "Back");

  ---------------------------- Netlist and Probe ----------------------------

  -- Definition of a sink and a driver :

  type sink_number_type      is range integer'low to integer'high;
    -- when > 0  : gate*4 + input#
    -- when <= 0 : output#

  -- this different type should not be compared directly with a sink number (because of *4)
  type driver_number_type      is range integer'low to integer'high;
    -- when > 0  : gate#
    -- when <= 0 : input#

  type gate_port_number_type is range 0 to 3;
  type gate_sink_array       is array (gate_port_number_type) of driver_number_type;

  type driver_record is
    record
      sink_address : integer; -- can be either:
         -- * when fanout > 1 : an index in the unified sinks table sinks_lists
         -- * when fanout < 2 : a sink_number_type
      fanout : integer;
    end record;

  ----------------------- The gate -----------------------

  -- The representation of a gate (and also the storage area for the histograms)

  --  should be better coded ! (but works well so far)
  constant SPECIAL : natural := 32; -- the index of the counter of "non-binary" codes
  type type_Histo16 is array (0 to SPECIAL) of big_sint_t; -- can store a LUT16, the "shadow/meta" LUT16 plus the "special"s counter

  type Gate_Descriptor is
    record
      -- general info:
      gate_kind  : enum_gate_kind; -- boolean, DFF or backwards ?
      gatename   : line;         -- full path & name of the gate

      -- Lookup & histograms :
      index      : natural;      -- the index of the last lookup
      last       : natural;      -- (size of the LUT) the last valid index (can be 0 for GND and VCC)
      inputs     : integer;      -- O to 4 inputs
      histo      : type_Histo16; -- the histogram
      curOut,                    -- the last result of lookup
      prevOut    : std_logic;    -- the previous result of the lookup {probing: check flag for repeats}
      changes    : big_uint_t;   -- how many times the output changed, 0-based {probing: decumulator}
      LUT        : std_logic_vector(0 to 15); -- cache of the gate's LUT. Fixed size because... VHDL ?

      -- for the netlist & probe:
      sinks      : gate_sink_array; -- {also accumulator during probe}
      driver     : driver_record;

--      depth_min  : integer;      -- how many gates to traverse to reach the closest input.
--      depth_max  : integer;      -- how many gates to traverse to reach the farthest input. <0 if zombie.
    end record;
  type Gate_Desc_Array is array(integer range <>) of Gate_Descriptor;
  type Gate_Desc_Access is access Gate_Desc_Array;
  shared variable GateList : Gate_Desc_Access;   -- The main part of the netlist

  -- the summarised list of registered gates, because the general list of gates contains holes,
  -- such as GND/VCC (LUT0) gates that are not listed and work a bit like unalterable ports.
  -- We want to process the histograms of gates with more than 1 input :
  shared variable indexPtr : integer_array_ptr; -- shoud be renamed...

  -- the sizing variables for the arrays described above :
  shared variable
    flipflop_instance_counter,
    backwards_instance_counter,
    gate_instance_counter,       --=> GateList'length
    registered_instance_counter  --=> indexPtr
       : integer := 0;


  -- just a little lookup table
  constant inputs_LUT: integer_array(0 to 16) := (
         0 =>  0,
         1 =>  1,
         3 =>  2,
         7 =>  3,
        15 =>  4,
    others => -1);

  -- from the generics and command line parameters :
  shared variable gate_select_number : integer := 0;
  shared variable bit_select_number : integer := -1;
  shared variable cycle_counter : big_uint_t := 0;
  shared variable flip_LUT : integer := 0;

  -- manage the file of exclusions:
  file exclude_file : TEXT;
  shared variable xcl_filename, current_vector : line;
  shared variable xcl_gatenum, xcl_oldgatenum : integer := -1;
  shared variable verbose_i, line_number : integer := 0;

  -- Used when probing:
  shared variable multiply_pass, probe_phase : integer := 0;

  -- And now, the list of functions :

  impure function Gate_Census(sinks: integer) return integer;
  impure function Back_Census return integer;
  impure function DFF_Census(sinks: integer) return integer;

  procedure update_select_gate(gate_number, bit_number, flip : integer; verb, filename : string);
  impure function RegisterLUT(L: std_logic_vector; fullpath: string;
     gate_number, lastbit : integer; exclude: std_logic_vector;
     gate_kind: enum_gate_kind := Gate_boolean) return std_logic_vector;
  impure function RegisterBack(fullpath: string; gate_number : integer) return integer;

  procedure update_gates_histograms;
  procedure display_gates_histograms;

  procedure register_gate_source(gate_number, input_number : integer; val : std_logic);
  impure function probe_return(gate_number : integer) return std_logic;

  impure function GoBackwards (A        : std_logic;                     gate_number : integer) return std_logic;
  impure function LookItUp1 (A          : std_logic; LUT2 : LookupType2; gate_number : integer) return std_logic;
  impure function LookItUp2 (A, B       : std_logic; LUT4 : LookupType4; gate_number : integer) return std_logic;
  impure function LookItUp3 (A, B, C    : std_logic; LUT8 : LookupType8; gate_number : integer) return std_logic;
  impure function LookItUp4 (A, B, C, D : std_logic; LUT16: LookupType16;gate_number : integer) return std_logic;

  procedure get_another_xcl_vector;
  procedure Init_get_xcl_vectors;

end Gates_lib;

-----------------------------------------------------------------------------------------

package body Gates_lib is

  -- just count the number of registered gates and their sinks
  impure function Gate_Census(sinks: integer) return integer is
  begin
    gate_instance_counter := gate_instance_counter + 1;
    return gate_instance_counter;
  end Gate_Census;

  impure function Back_Census return integer is
  begin
    backwards_instance_counter := backwards_instance_counter + 1;
    return Gate_Census(1);
  end Back_Census;

  impure function DFF_Census(sinks: integer) return integer is
  begin
    flipflop_instance_counter := flipflop_instance_counter + 1;
    return Gate_Census(sinks);
  end DFF_Census;


  -- Now that we know how many gates and sinks we have,
  -- allocate and initialise some shared variables with the generics,
  -- as well as the netlist/histograms.
  -- Called by the component register_generics
  procedure update_select_gate(
    gate_number, bit_number, flip : integer;
    verb, filename : string) is
  begin
    report integer'image(gate_instance_counter) & " gates found" ;
    if backwards_instance_counter > 0 then
      report " including " & integer'image(backwards_instance_counter) & " backwards gates";
    end if;
    if flipflop_instance_counter > 0 then
      report " including " & integer'image(flipflop_instance_counter) & " Flip-Flop backwards gates";
    end if;

    if gate_number < 1 then
      report " Parameters:"
           & " gate number: " & integer'image(gate_number)
           & "  bit number: " & integer'image( bit_number);
      report " -- no gate function is altered from the generics --";
    end if;
    -- register the generics and other parameters into the shared variables :
    gate_select_number := gate_number;
     bit_select_number :=  bit_number;
    if flip = 2 then  -- ok it's an ugly corner case...
       flip_LUT := 0;
       run_mode := probe;
    else
       flip_LUT := flip;
       run_mode := trace;  -- To be changed later
    end if;

    if verb'length > 0 then
      verbose_i := 1;
      if verb="full" then
        verbose_i := 2;
      end if;      
    end if;

    write(xcl_filename, filename);
    GateList := new Gate_Desc_Array(1 to gate_instance_counter);
    indexPtr := new integer_array(0 to gate_instance_counter);
    Init_get_xcl_vectors; -- load a first exclusion vector into current_vector

    if gate_number < 1 and gate_instance_counter > 0 then
      report "List of the registered gates:";
    end if;
  end update_select_gate;

  -- This function is called at the initialisation time of each gate,
  -- after the generics are evaluated and Gate_Census gives the gate_number,
  -- but before the clock starts.
  -- If fast/default mode, the arrays are not initialised and LUT is simply copied.
  -- Otherwise, this allows us to list the gate or initialise and/or tweak its LUT.
  impure function RegisterLUT(L: std_logic_vector; fullpath: string;
       gate_number, lastbit : integer; exclude: std_logic_vector;
       gate_kind: enum_gate_kind := Gate_boolean)
  return std_logic_vector is
    variable T : std_logic_vector(L'range) := L; -- the copy
    variable i : integer;

    procedure exclude_LUT (vector : string) is
      variable j : integer;
    --variable k : integer; loop counter
    begin
--      if vector'length > 0 then  => implicit
      j := 0;
      for k in vector'range loop
        exit when j > lastbit;
        if vector(k) = 'X' then
          T(j             ) := 'X';
          T(j + lastbit+ 1) := 'X';
          GateList(gate_number).histo(j             ):= -1;
          GateList(gate_number).histo(j + lastbit+ 1):= -1;
        end if;
        j := j+1;
      end loop;
      if verbose_i /= 0 then
        report " Excluding inputs of gate #" & integer'image(gate_number) & " -> " & SLV_to_bin(T) & " - " & fullpath;
      end if;
    end exclude_LUT;

  begin
    if run_mode /= fast then
      if gate_select_number < 1 then  -- just a listing
        report " gate#" & integer'image(gate_number)
             & " [bool] : " & fullpath;
      end if;

      -- backwards gates have their own procedure.
      GateList(gate_number).gate_kind := gate_kind;

      -- save the gate's name in a dynamic area:
      write(GateList(gate_number).gatename, fullpath);

      -- clear the histogram :
      GateList(gate_number).histo := (others => 0);
      GateList(gate_number).inputs := inputs_LUT(lastbit);
      assert GateList(gate_number).inputs >= 0
          report "lastbit's value is invalid (wrong gate input size)"
          severity failure;

      -- register an exclusion :
      if gate_number = xcl_gatenum then
        -- exclusion from file
	if current_vector.all'length > 0 then
          exclude_LUT(current_vector.all);
        end if;
        get_another_xcl_vector;
      else
        -- direct exclusion from generic
        if exclude'length > 0 then
          exclude_LUT(SLV_to_bin(exclude));
        end if;
      end if;

      -- if flip or trace :
      if gate_select_number >= 0 then
        -- init the histogram:
        GateList(gate_number).last := lastbit;

        -- register into the list of updates/display:
        if lastbit > 0 then
          indexPtr(registered_instance_counter) := gate_number;
          registered_instance_counter := registered_instance_counter + 1;
        end if;

        if  gate_select_number = gate_number
         and bit_select_number >= 0
         and bit_select_number <= lastbit then

          assert T(bit_select_number) /= 'X'
             report "Can't flip unreachable bit of gate#" & integer'image(gate_number) & " - " & fullpath
             severity failure;

          if flip_LUT = 1 then
            -- Alter the selected bit
            i := bit_select_number + lastbit+ 1;
            if T(bit_select_number) = '0' then
              T(i) := 'H'; -- invert the high/shadow bits
              T(bit_select_number) := '1';
            else
              T(i) := 'L';
              T(bit_select_number) := '0';
            end if;
            report " Flipping bit " & integer'image(bit_select_number)
                 & " of gate #" & integer'image(gate_number) & " -> " & SLV_to_bin(T) & " - " & fullpath;
          else
            -- transform the selected value into a meta value that will propagate
            if T(bit_select_number) = '0' then
              T(bit_select_number) := 'L';
            else
              T(bit_select_number) := 'H';
            end if;
            report " Tracing bit " & integer'image(bit_select_number)
               & " of gate #" & integer'image(gate_number) & " -> "
               & SLV_to_bin(T) & " - " & fullpath;
          end if;
        end if;
      end if;

      -- Keep a local copy of the altered LUT (for vectgen later)
      GateList(gate_number).LUT(0 to lastbit) := T(0 to lastbit);
    end if;
    return T;
  end RegisterLUT;

  -- same as RegisterLUT but without the LUT stuff.
  impure function RegisterBack(fullpath: string; gate_number : integer) return integer is
  begin
    if run_mode /= fast then
      if gate_select_number < 1 then  -- just a listing
        report " gate#" & integer'image(gate_number) & " [back] : " & fullpath;
      end if;

      -- save the gate's name in a dynamic area:
      write(GateList(gate_number).gatename, fullpath);
      GateList(gate_number).inputs := 1;

      GateList(gate_number).gate_kind := Gate_backwards;
    end if;
    return gate_number; -- nothing significant
     -- but writing something, anything, is required
  end RegisterBack;


  procedure update_gates_histograms is
    variable i, j, k : integer;
    variable l : big_sint_t;
    variable s, t : std_logic;
  begin
    assert cycle_counter < big_uint_t'high
       report "Simulation reached maximum cycle count"
       severity failure;
    cycle_counter := cycle_counter + 1;
    i := 0;

    -- scan all the gates with >=2 inputs
    while i < registered_instance_counter loop
      j := indexPtr(i);
      k := GateList(j).index;
      l := GateList(j).histo(k);
      assert l /= -1
        report "Gate#" & integer'image(j) & " reached excluded state '" & integer'image(k) & "'"  
        severity failure ;
      GateList(j).histo(k) := l + 1;

      -- count the toggles at the output:
         s := GateList(j).curOut;
      if s /= GateList(j).prevOut then
        GateList(j).changes := GateList(j).changes + 1;
        GateList(j).prevOut := s;
      end if;

      i := i+1;
    end loop;
  end update_gates_histograms;


  procedure display_gates_histograms is
  --variable k : integer; loop counter
    variable i, j, m, n : integer;
    variable c : big_uint_t;
    variable o : big_sint_t;
    variable l : line;
  begin
    report "End of simulation ! " & big_uint_t'image(cycle_counter) & " cycles.";

    c := 0;
    i := 0;
    while i < registered_instance_counter loop
      j := indexPtr(i);
      m := GateList(j).last;
      n := m+m+1;
      for k in 0 to n loop
        o := GateList(j).histo(k);
        if o = -1 then
          write(l, string'("^ "));
        else
          write(l, big_sint_t'image(o) & " ");
        end if;
        if k = m then
          write(l, string'(" |  "));
        end if;
      end loop;

      c := c + GateList(j).changes;

      report " h(" & integer'image(j) & ") = " & L.all
           & " ("  & big_sint_t'image(GateList(j).histo(special))
           & ") [" & big_uint_t'image(GateList(j).changes) & "] - " & GateList(j).gatename.all;
      Deallocate(L);
      i := i+1;
    end loop;

    report "Total state changes: " & big_uint_t'image(c) & " (not counting fanout)";
  end display_gates_histograms;

------------------------------------------------------------------------------------------

  -- the new method encodes the gate number into a string of std_logic
  -- see https://hackaday.io/project/174585-libre-gates/log/185795-the-new-netlist-scanner
  impure function probe_return(gate_number : integer) return std_logic
  is
    variable n : big_uint_t;
  begin
    -- div/mod and encoding
    n := GateList(gate_number).changes;
         GateList(gate_number).changes := n / 8;
    return std_logic'val((n mod 8)+1);
  end probe_return;
  -- so short it could be inlined...
  -- 20201209 : inlined !

  -- this new procedure version only "accumulates" symbols.
  procedure register_gate_source(gate_number, input_number: integer; val: std_logic) is
  begin
    GateList(gate_number).sinks(gate_port_number_type(input_number)) :=
    GateList(gate_number).sinks(gate_port_number_type(input_number)) + driver_number_type(
        (multiply_pass * (std_logic'pos(val) - 1)));
  end register_gate_source;
  -- should be inlined too...

-------------------------------------------------------------------------------------------
-- The functions called by each type of gate :

  impure function GoBackwards (A : std_logic;
      gate_number : integer) return std_logic is
    variable index : integer;
    variable R : std_logic := A;
  begin
    if run_mode = probe then
      if probe_phase = 0 then
        R := 'U';
      else
        if (A /= 'U') and (GateList(gate_number).prevOut = 'U') then
          register_gate_source(gate_number, 0, A);
          index := integer(GateList(gate_number).changes);
                           GateList(gate_number).changes := big_uint_t(index / 8);
          R := std_logic'val((index mod 8)+1);
          if verbose_i > 1 then
            report "back#" & integer'image(gate_number)
                 & " A=" & std_logic'image(A)
                 & ",                    " & integer'image(index)
                 & " ==> " & std_logic'image(R) ;
          end if;
        end if;
      end if;
      GateList(gate_number).prevOut := R;
    end if;

    return R;
  end GoBackwards;


  impure function LookItUp1 (A : std_logic; LUT2 : LookupType2;
          gate_number : integer) return std_logic is
    variable index : integer := 0;
    variable R : std_logic := A;
  begin

    case run_mode is
      when fast =>
        if A = '1' then
          return LUT2(1);
        else
          return LUT2(0);
        end if;

      when trace =>
        case A is
          when '0'    => index := 0;
          when '1'    => index := 1;
          when 'L'    => index := 2;
          when 'H'    => index := 3;
          when others => index := SPECIAL;
                GateList(gate_number).index := index;
                return R;
        end case;
        R := LUT2(index);
        GateList(gate_number).index := index;
        GateList(gate_number).curOut := R;

      when probe =>
        if probe_phase = 0 then
          R := 'U';
        else
          if (A /= 'U') and (GateList(gate_number).prevOut = 'U') then
            register_gate_source(gate_number, 0, A);
            index := integer(GateList(gate_number).changes);
                             GateList(gate_number).changes := big_uint_t(index / 8);
            R := std_logic'val((index mod 8)+1);
            if verbose_i > 1 then
              report "gate#" & integer'image(gate_number)
                 & " A=" & std_logic'image(A)
                 & ",                    " & integer'image(index)
                 & " ==> " & std_logic'image(R) ;
            end if;
          end if;
        end if;
        GateList(gate_number).prevOut := R;

    end case;
    return R;
  end LookItUp1;

  impure function LookItUp2 (A, B : std_logic; LUT4 : LookupType4;
          gate_number : integer) return std_logic is
    variable index, meta : integer := 0;
    variable L : std_logic;
    variable R : std_logic := A;
  begin

    case run_mode is
      when fast =>
        if A = '1' then
          index := 2;
        end if;
        if B = '1' then
          index := index+1;
        end if;
        return LUT4(index);

      when trace =>
        case A is
          when '0'    =>  NULL;
          when '1'    =>  index := 2;
          when 'L'    =>              meta := 4;
          when 'H'    =>  index := 2; meta := 4;
          when others =>  GateList(gate_number).index := SPECIAL; return A;
        end case;

        case B is
          when '0'    =>  NULL;
          when '1'    =>  index := index+1;
          when 'L'    =>                    meta := 4;       
          when 'H'    =>  index := index+1; meta := 4;
          when others =>  GateList(gate_number).index := SPECIAL; return B;
        end case;

        index := index+meta;
        R := LUT4(index);
        GateList(gate_number).index := index;
        GateList(gate_number).curOut := R;

      when probe =>
        if probe_phase = 0 then
          R := 'U';
          GateList(gate_number).histo(0) := 0;
          GateList(gate_number).histo(1) := 0;
        else
          if (A /= 'U') and (GateList(gate_number).histo(0) = 0) then
            register_gate_source(gate_number, 0, A);
            GateList(gate_number).histo(0) := 1;
          end if;

          if (B /= 'U') and (GateList(gate_number).histo(1) = 0) then
            register_gate_source(gate_number, 1, B);
            GateList(gate_number).histo(1) := 1;
          end if;

          L := GateList(gate_number).prevOut;
          index := integer(GateList(gate_number).changes);
          if (L = 'U') then
                           GateList(gate_number).changes := big_uint_t(index / 8);
            R := std_logic'val((index mod 8)+1);
          else
            R := L;
          end if;

          if verbose_i > 1 then
            report "gate#" & integer'image(gate_number)
                & " A=" & std_logic'image(A)
                & " B=" & std_logic'image(B)
                & ",              " & integer'image(index)
                & " ==> " & std_logic'image(R) ;
          end if;
        end if;
        GateList(gate_number).prevOut := R;

    end case;
    return R;
  end LookItUp2;

  impure function LookItUp3 (A, B, C : std_logic; LUT8 : LookupType8;
      gate_number : integer) return std_logic is
    variable index, meta : integer := 0;
    variable L : std_logic;
    variable R : std_logic := A;
  begin

    case run_mode is
      when fast =>
        if A = '1' then
          index := 4;
        end if;

        if B = '1' then
          index := index+2;
        end if;

        if C = '1' then
          index := index+1;
        end if;
        return LUT8(index);

      when trace =>
        case A is
          when '0'    =>  NULL;
          when '1'    =>  index := 4;
          when 'L'    =>              meta := 8;
          when 'H'    =>  index := 4; meta := 8;
          when others =>  GateList(gate_number).index := SPECIAL; return A;
        end case;

        case B is
          when '0'    =>  NULL;
          when '1'    =>  index := index+2;
          when 'L'    =>                    meta := 8;
          when 'H'    =>  index := index+2; meta := 8;
          when others =>  GateList(gate_number).index := SPECIAL; return B;
        end case;

        case C is
          when '0'    =>  NULL;
          when '1'    =>  index := index+1;
          when 'L'    =>                    meta := 8;
          when 'H'    =>  index := index+1; meta := 8;
          when others =>  GateList(gate_number).index := SPECIAL; return C;
        end case;

        index := index+meta;
        R := LUT8(index);
        GateList(gate_number).index := index;
        GateList(gate_number).curOut := R;

      when probe =>
        if probe_phase = 0 then
          R := 'U';
          GateList(gate_number).histo(0) := 0;
          GateList(gate_number).histo(1) := 0;
          GateList(gate_number).histo(2) := 0;
        else
          if (A /= 'U') and (GateList(gate_number).histo(0) = 0) then
            register_gate_source(gate_number, 0, A);
            GateList(gate_number).histo(0) := 1;
          end if;

          if (B /= 'U') and (GateList(gate_number).histo(1) = 0) then
            register_gate_source(gate_number, 1, B);
            GateList(gate_number).histo(1) := 1;
          end if;

          if (C /= 'U') and (GateList(gate_number).histo(2) = 0) then
            register_gate_source(gate_number, 2, C);
            GateList(gate_number).histo(2) := 1;
          end if;

          L := GateList(gate_number).prevOut;
          index := integer(GateList(gate_number).changes);
          if (L = 'U') then
                           GateList(gate_number).changes := big_uint_t(index / 8);
            R := std_logic'val((index mod 8)+1);
          else
            R := L;
          end if;

          if verbose_i > 1 then
            report "gate#" & integer'image(gate_number)
                & " A=" & std_logic'image(A)
                & " B=" & std_logic'image(B)
                & " C=" & std_logic'image(C)
                & ",        " & integer'image(index)
                & " ==> " & std_logic'image(R) ;
          end if;
        end if;
        GateList(gate_number).prevOut := R;

    end case;
    return R;
  end LookItUp3;


  impure function LookItUp4 (A, B, C, D : std_logic; LUT16 : LookupType16;
      gate_number : integer) return std_logic is
    variable index, meta : integer := 0;
    variable L : std_logic;
    variable R : std_logic := A;
  begin

    case run_mode is
      when fast =>
        if A = '1' then
          index := 8;
        end if;

        if B = '1' then
          index := index+4;
        end if;

        if C = '1' then
          index := index+2;
        end if;

        if D = '1' then
          index := index+1;
        end if;
        return LUT16(index);

      when trace =>
        case A is
          when '0'    =>  NULL;
          when '1'    =>  index := 8;
          when 'L'    =>              meta := 16;
          when 'H'    =>  index := 8; meta := 16;
          when others =>  GateList(gate_number).index := SPECIAL; return A;
        end case;

        case B is
          when '0'    =>  NULL;
          when '1'    =>  index := index+4;
          when 'L'    =>                    meta := 16;
          when 'H'    =>  index := index+4; meta := 16;
          when others =>  GateList(gate_number).index := SPECIAL; return B;
        end case;

        case C is
          when '0'    =>  NULL;
          when '1'    =>  index := index+2;
          when 'L'    =>                    meta := 16;
          when 'H'    =>  index := index+2; meta := 16;
          when others =>  GateList(gate_number).index := SPECIAL; return C;
        end case;

        case D is
          when '0'    =>  NULL;
          when '1'    =>  index := index+1;
          when 'L'    =>                    meta := 16;
          when 'H'    =>  index := index+1; meta := 16;
          when others =>  GateList(gate_number).index := SPECIAL; return D;
        end case;

        index := index+meta;
        R := LUT16(index);
        GateList(gate_number).index := index;
        GateList(gate_number).curOut := R;
        return R;

      when probe =>
        if probe_phase = 0 then
          R := 'U';
          GateList(gate_number).histo(0) := 0;
          GateList(gate_number).histo(1) := 0;
          GateList(gate_number).histo(2) := 0;
          GateList(gate_number).histo(3) := 0;
        else
          if (A /= 'U') and (GateList(gate_number).histo(0) = 0) then
            register_gate_source(gate_number, 0, A);
            GateList(gate_number).histo(0) := 1;
          end if;

          if (B /= 'U') and (GateList(gate_number).histo(1) = 0) then
            register_gate_source(gate_number, 1, B);
            GateList(gate_number).histo(1) := 1;
          end if;

          if (C /= 'U') and (GateList(gate_number).histo(2) = 0) then
            register_gate_source(gate_number, 2, C);
            GateList(gate_number).histo(2) := 1;
          end if;

          if (D /= 'U') and (GateList(gate_number).histo(3) = 0) then
            register_gate_source(gate_number, 3, D);
          end if;

          L := GateList(gate_number).prevOut;
          index := integer(GateList(gate_number).changes);
          if (L = 'U') then
                           GateList(gate_number).changes := big_uint_t(index / 8);
            R := std_logic'val((index mod 8)+1);
          else
            R := L;
          end if;

          if verbose_i > 1 then
            report "gate#" & integer'image(gate_number)
                & " A=" & std_logic'image(A)
                & " B=" & std_logic'image(B)
                & " C=" & std_logic'image(C)
                & " D=" & std_logic'image(D)
                & ",  " & integer'image(index)
                & " ==> " & std_logic'image(R) ;
          end if;
        end if;
        GateList(gate_number).prevOut := R;

    end case;
    return R;
  end LookItUp4;

-----------------------------------------------------------------------------------------

  -- used by RegisterLUT
  procedure get_another_xcl_vector is
    variable char : character;
  begin
    xcl_oldgatenum := xcl_gatenum;
    loop
      if endfile(exclude_file) then
        if verbose_i /= 0 then
          report "end of exclusion input file.";
        end if;
        -- prevent triggering more tests&calls
        xcl_gatenum := -1;
        file_close(exclude_file);
        exit;
      end if;

      line_number := line_number + 1;
      readline(exclude_file, current_vector);

      if current_vector.all'length > 0 then
        if current_vector.all(current_vector.all'left) = '#' then
          if verbose_i /= 0 then
            report "Comment: '" & current_vector.all & "'";
          end if;
        else
          read(current_vector, xcl_gatenum);

          assert xcl_gatenum > xcl_oldgatenum
             report  xcl_filename.all & "(" & integer'image(line_number) & ") : gate#"
             & integer'image(xcl_gatenum) & " index is lower than previous index."
             severity failure;

          assert xcl_gatenum <= gate_instance_counter
            report "The exclude file " & xcl_filename.all & " contains too many vectors"
            severity failure;

          -- skip all the space/separators
          while current_vector.all'length  > 0
           and (current_vector.all(current_vector.all'left) = ' '
             or current_vector.all(current_vector.all'left) = HT ) loop
           read(current_vector, char);
          end loop; -- should be better coded -_-

          -- Little check of the remaining string
          for i in current_vector.all'range loop
            char := current_vector.all(i);
            assert char = '-' or char = 'X'
              report "invalid characters in exclusion string."
              severity failure;
          end loop;

          exit;
        end if;
      end if;
    end loop;
  end get_another_xcl_vector;


  procedure Init_get_xcl_vectors is
  begin
    if xcl_filename.all /= "" then
      file_open(exclude_file, xcl_filename.all, read_mode);
      line_number := 1;
      get_another_xcl_vector;
    else
      -- prevent triggering the tests&calls
      if verbose_i /= 0 then
        report "no exclusion input file to read.";
      end if;
      xcl_gatenum := -1;
    end if;
  end Init_get_xcl_vectors;

end Gates_lib;
