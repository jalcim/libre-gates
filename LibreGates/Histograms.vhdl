-- file LibreGates/Histograms.vhdl
-- created ven. déc. 20 05:25:45 CET 2019 by whygee@f-cpu.org
-- version sam. août 29 22:32:30 CEST 2020 : condensed display (saves some sed filters)
-- version mer. nov. 11 14:20:25 CET 2020 : renamed some
--   variable names to remove "declaration hides variable" warnings
--
-- Released under the GNU AGPLv3 license
--
-- This file contains common definitions for collecting statistics

package Histograms is
  -- set to 61 for recent GHDL or VHDL'2018
  -- otherwise, set to 31 for standard '93 but limited capacity.
--  constant how_many_bits : integer := 61;

  -- this one uses less memory as well, OK for small or medium tests.
  constant how_many_bits : integer := 31;

  type big_uint_t is range                 0 to 2**(how_many_bits+1) - 1;
  type big_sint_t is range -2**how_many_bits to 2** how_many_bits    - 1;

  type big_uint_array is array(integer range <>) of big_uint_t;
  type big_sint_array is array(integer range <>) of big_sint_t;

  type big_uint_ax is access big_uint_array;
  type big_sint_ax is access big_sint_array;

  type Histogram_record is
    record
      total : big_uint_t;  -- total number of samples
      first : integer;     -- first index of the histogram
      last  : integer;     -- last index of the histogram
      low   : integer;     -- lowest index of non-empty bin
      high  : integer;     -- highest index of non-empty bin
      max   : big_uint_t;  -- highest value of the bins
      maxpos: integer;     -- index of the bin with highest value
      bins  : big_uint_ax; -- points to the actual data
    end record;

  shared variable condensed_display: integer := 0;

  function create_histogram(first : integer; last : integer) return Histogram_record;
  procedure update_histogram(val: integer; h: inout Histogram_record);
  procedure display_histogram(h: inout Histogram_record; width: integer);

end Histograms;

package body Histograms is

  function create_histogram(first : integer; last  : integer) return Histogram_record is
    variable h: Histogram_record;
  begin
    assert last > first
      report "trying to create a histogram with invalid bounds"
      severity failure;
    h.total := 0;
    h.first := first;
    h.last  := last;
    h.low   := last;
    h.high  := first;
    h.max   := 0;
    h.maxpos:= first;
    h.bins  := new big_uint_array(first to last);
    return h;
  end create_histogram;

  procedure update_histogram(val: integer; h: inout Histogram_record) is
    variable i : big_uint_t;

    procedure resize_histogram(newfirst: integer; newlast: integer) is
      variable new_bins : big_uint_ax := new big_uint_array(newfirst to newlast);
    begin
--      report "Resizing the histogram to " & integer'image(newfirst)
--         & "-" & integer'image(newlast) & " bins";
      new_bins(h.first to h.last) := h.bins.all;
      deallocate(h.bins);
      h.bins := new_bins;
      h.first := newfirst;
      h.last  := newlast;
    end resize_histogram;

  begin
    -- resize the damned thing
    if val > h.last then
      resize_histogram(h.first, val+10);
    end if;

    if val < h.first then
      resize_histogram(val-10, h.last);
    end if;

    i := h.bins(val) + 1;
    h.bins(val) := i;
    if i > h.max then
      h.max := i;
      h.maxpos := val;
    end if;

    if val > h.high then
      h.high := val;
    end if;

    if val < h.low then
      h.low := val;
    end if;
  end update_histogram;

  procedure display_histogram(h: inout Histogram_record; width: integer) is
    -- variable i : integer; => loop counter
    variable j, m, m1, n : integer;
    variable b : big_uint_t;
    variable s : string(1 to width);

    function align(ss : string) return string is
      variable r: string(1 to 6) := "      ";
    begin
      if ss'length >= r'length then
        return ss;
      end if;
      return r(1 to 6-ss'length) & ss;
    end align;

  begin
    m := integer(h.max);
    m1 := m-1;
    if m > 0 then
      for i in h.low to h.high loop
        b := h.bins(i);

        if b /= 0 or condensed_display = 0 then

          -- "denormal"
          n := integer(b);
          if m > width then
            n := ((n * width) + m1) /m; -- round up
          end if;

          s := (others => ' ');
          s(1 to n) := (others => '*');
          report align(integer'image(i)) & " : " & align(big_uint_t'image(b)) & " - " & s ;
        end if;
      end loop;
    end if;
  end display_histogram;

end Histograms;
