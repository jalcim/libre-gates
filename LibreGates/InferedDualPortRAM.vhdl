-- InferedDualPortRAM.vhdl
-- version 20140510
-- should work for simulation, Altera, Xilinx and Actel/microsemi.
-- A special, tuned version for Actel's A3P is also provided,
-- which shaves many dumb gates from the inferred circuit.
-- PA3's RAM4K9 serves as guidance for the other implementations.
-- * I've added a reset to comply with the tools but it's not necessary.
-- * RAM4K9 supports write-read-through (write data and it appears
--    at the outside as well) but synthesis adds more gates...
--     See WMODEA & WMODEB (high)
--         ### write-read-through is implemented here ###
-- * changed SLV to integer conversion using more standards-compliant code
-- * adapted Write Enable polarity (write='0', read='1')
-- * added some resets
-- * BLKA & BLKB don't seem to be supported by other makers
--     so it's dropped (stuck active=low)
-- * PIPEA & PIPEB tied low (not pipelined)
-- Use the A3P_Slices version for microsemi because it fails
--   to infer correctly and creates tons of FF ! I don't see
--    how to make it accept "write through" which is necessary
--    for miniYASEP.
--
-- version 20171114 : stripped-down for standalone use.
--  See also A3P_DualPortRAM.vhd
--
-- from	PA3_HB.pdf v1.5, p6-8:
-- WMODEA and WMODEB
-- These signals are used to configure the behavior of the output
-- when the RAM is in write mode. A LOW on these signals makes 
-- the output retain data from the previous read. A HIGH indicates
-- pass-through behavior, wherein the data being written will appear 
-- immediately on the output. This signal is overridden when the RAM is being read.


library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity bram is
  generic (
    SLICES  : integer := 4;
    SLICE_WIDTH : integer := 4; 
    WIDTH   : integer := 16; -- please preserve SLICES*SLICE_WIDTH;
    ADRBITS : integer := 10
    -- For Microsemi :
    -- ADRBITS       9  10  11  12 
    -- SLICE_WIDTH  8/9  4   2   1
  );
  port (
    reset : in std_logic := '1'; -- may be omitted
    -- port A :
    a_clk, a_wr_en : in std_logic;
    a_addr : in std_logic_vector(ADRBITS-1 downto 0);
    a_din  : in std_logic_vector(WIDTH-1 downto 0);
    a_dout : out std_logic_vector(WIDTH-1 downto 0);
    -- port B :
    b_clk, b_wr_en : in std_logic;
    b_addr : in std_logic_vector(ADRBITS-1 downto 0);
    b_din  : in std_logic_vector(WIDTH-1 downto 0);
    b_dout : out std_logic_vector(WIDTH-1 downto 0)
  );
end bram;

architecture rtl of bram is
  type mem_type is array ( (2**ADRBITS)-1 downto 0)
     of std_logic_vector(WIDTH-1 downto 0);
  shared variable mem: mem_type;
begin

  process(a_clk, reset)
    variable ad : integer;
  begin
    if (reset='0') then
      a_dout <= (a_dout'range=>'0');
    else
      if (a_clk'event and a_clk='1') then
        ad := to_integer(unsigned(a_addr));
        if a_wr_en='0' then
          mem(ad) := a_din;
        end if;
        a_dout <= mem(ad);
      end if;
    end if;
  end process;

  process(b_clk, reset)
    variable ad : integer;
  begin
    if (reset='0') then
      b_dout <= (b_dout'range=>'0');
    else
      if (b_clk'event and b_clk='1') then
        ad := to_integer(unsigned(b_addr));
        if b_wr_en='0' then
          mem(ad) := b_din;
        end if;
        b_dout <= mem(ad);
      end if;
    end if;
  end process;
end rtl;
