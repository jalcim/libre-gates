-- LibreGate/Netlist_lib.vhdl
-- created dim. déc.  8 08:36:48 CET 2019 by whygee@f-cpu.org
-- version dim. déc.  8 22:01:42 CET 2019 : gets the DUT's vector size.
-- version sam. déc. 21 09:52:10 CET 2019 : depth
-- version mar. déc. 24 22:25:32 CET 2019 : GateList(g).depth_max fixed
-- version mer. juil. 22 01:59:04 CEST 2020 : error " Can't find the output vector size :-( " fixed for DUT with only 1 output bit
-- version mar. sept.  1 07:10:29 CEST 2020 : LibreGates
-- version mar. sept. 15 06:30:14 CEST 2020 : forked/derived from VectGen.vhdl
-- version ven. nov. 27 23:43:15 CET 2020 : rebuilding the probing algorithm
--
-- This code extracts and analyses a netlist built with the LibreGates library.
-- This netlist provides useful information about the circuit, it might find some potential errors
-- and more importantly : it's the first step necessary for building test vectors that could
-- be used later to verify the integrity and functionality of a physical implementation.
--
-- Released under the GNU AGPLv3 license

Library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
Library SLV;
    use SLV.SLV_utils.all;
Library Histo;
    use Histo.Histograms.all;
Library work;
    use work.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

package Netlist_lib is
--  type integer_array_array is array(integer range <>) of integer_array_ptr;
--  type integer_array_array_ptr is access integer_array_array;

  shared variable
     sinks_lists_count,  -- size of sinks_lists
     pseudo_drivers_count,
     DutVectIn_length,
     DutVectOut_length : integer := 0;

  -- Inputs :  (reuses and extends declarations from Gates_lib.vhdl)
  type driver_record_array is array (integer range <>) of driver_record;
  type driver_record_array_access is access driver_record_array;
  shared variable VectIn_src : driver_record_array_access; -- array of the descriptors of the input port

  type driver_number_array     is array (integer range <>) of driver_number_type;
  type driver_number_array_ptr is access driver_number_array;
  shared variable VectOut_sink : driver_number_array_ptr;  -- array of descriptors of the output port

  type sink_number_array     is array (integer range <>) of sink_number_type;
  type sink_number_array_ptr is access sink_number_array;
  shared variable sinks_lists : sink_number_array_ptr;  -- dynamic array with sinks_lists_count entries, part of the netlist

  shared variable pseudo_drivers_list, depth_lists : integer_array_ptr;

  -- The polynomial parameters:
  shared variable Poly_factor : integer :=  499; -- 13  -- 61 -- 499 
  shared variable Poly_offset : integer :=  257; --  5  -- 23 -- 257
  -- Ideally, choose both as prime numbers, and the
  -- offset MUST be less than the factor (one half is good).
  -- The compromise is between execution speed (each
  -- factor of 8 adds another probe cycles) and
  -- error discrimination (fewer chances of coincidence).

  shared variable main_delay : time := 1 sec;

  procedure dumb_display_netlist;

  procedure Probe_netlist(
    signal DutVectIn :   out std_logic_vector;
    signal DutVectOut: in    std_logic_vector;
    signal DutVectClk: inout std_logic);

  procedure Drive_DUT(
    signal DutVectIn :   out std_logic_vector;
    signal DutVectOut: in    std_logic_vector;
    signal DutVectClk: inout std_logic;
             Interval: in    time);

  procedure Drive_DUT_dummy(
    signal DutVectIn :   out std_logic_vector;
    signal DutVectOut: in    std_logic_vector;
    signal DutVectClk: inout std_logic;
             Interval: in    time);

end Netlist_lib;

package body Netlist_lib is

-----------------------------------------------------------------
-- A first step in the whole process

  procedure Probe_netlist(
      -- the vectors MUST be 0-based
      signal DutVectIn :   out std_logic_vector;
      signal DutVectOut: in    std_logic_vector;
      signal DutVectClk: inout std_logic) is

    variable
      unconnected_inputs,
      unconnected_outputs,
      FirstGateIndex,
      LastGateIndex,
      gatecode, n : integer := 0;
    variable b : std_logic;

    procedure update_sink(d : driver_number_type; from : integer) is
      variable t : integer := integer(d);

      -- I know you love procedures so I put a procedure
      -- inside a procedure's procedure so you can ... never mind.
      procedure register_sink(s : inout driver_record) is
        variable l : integer;
      begin
        if s.fanout = 1 then
          s.sink_address := from;
        else
          l := s.sink_address - 1;
          s.sink_address := l;
          sinks_lists(l) := sink_number_type(from);
        end if;
      end register_sink;

    begin
      if t < 1 then
        register_sink(VectIn_src(-t));
      else
        register_sink(GateList(t).driver);
      end if;
    end update_sink;

    -- this procedure replaces a signature with an actual sink type,
    -- while also updating the drivers's fanout.
    procedure check_sink_source(s : inout driver_number_type; f, g: integer) is
      variable l, m, o : integer;

    begin
      -- process and check the driver's address:
      l := integer(s);
      o := l  /  Poly_factor; -- \ combined div/mod
      m := l mod Poly_factor; -- /

      if l = 0 then
        report " Invalid driver signature : not connected ?";
      else
        if l  <  Poly_offset
        or l  >  LastGateIndex
        or m /=  Poly_offset then
          if f < 1 then
            report " Invalid driver signature = " & integer'image( l)
              & " - driver conflict on outport #" & integer'image(-f) & " ?";
          else
            report " Invalid driver signature = " & integer'image(l)
                 & " - driver conflict on gate #" & integer'image(f)
                                            & "(" & integer'image(g) & ") ?";
          end if;
          unconnected_inputs :=
          unconnected_inputs + 1;
        else
          -- check the type of the driver and increment the fanout
          if l >= FirstGateIndex then
            o := o - DutVectIn'high;
            if verbose_i > 0 then
              report "driver is gate #" & integer'image(o);
            end if;
            GateList(o).driver.fanout :=
            GateList(o).driver.fanout + 1;
          else
            if verbose_i > 0 then
              report "driver is inport #" & integer'image(o);
            end if;
            VectIn_src(o).fanout :=
            VectIn_src(o).fanout + 1;
            o := -o;
          end if;

          s := driver_number_type(o);
        end if;
      end if;
    end check_sink_source;

  begin
    wait for main_delay;

    -- prepare for later with the depthlists:
    pseudo_drivers_count := backwards_instance_counter + flipflop_instance_counter;
    pseudo_drivers_list  := new integer_array(0 to pseudo_drivers_count-1);
    n:=0;

    -- 1) Scan the ports and gates to initialise their values
    multiply_pass := 1;
    gatecode := Poly_offset;

    for i in VectIn_src'range loop
      VectIn_src(i).fanout := gatecode;
      gatecode := gatecode + Poly_factor;  -- the "prefix sum" performs the sequential multiplies
    end loop;

    FirstGateIndex := gatecode;
    for i in GateList'range loop -- 1-based array /!\
      GateList(i).changes := big_uint_t(gatecode);
      GateList(i).sinks := (others=>0);
      GateList(i).driver.fanout := 0;
      GateList(i).prevOut := 'U';
--      GateList(i).histo := (others=>0);  -- already done during RegisterLUT
      LastGateIndex := gatecode;
      if GateList(i).gate_kind /= Gate_boolean then
        pseudo_drivers_list(n) := i;
        n := n + 1;
      end if;
      gatecode := gatecode + Poly_factor;
    end loop;

    VectOut_sink.all := (VectOut_sink'range => 0);

    -- the main loop
    while multiply_pass <= LastGateIndex loop

      -- Phase 0:
      -- force update and signal propagation
      DutVectIn <= (DutVectIn'range=>'U');
      probe_phase := 0;
      wait for main_delay;
      if verbose_i > 0 then
        report "--------------- Pass=" & integer'image(multiply_pass)
            & " ---------------";
      end if;

      -- Phase 1:
      -- send the driver code.
      -- The gates are updated automatically
      -- but not the ports, so inject fresh data
      for i in VectIn_src'range loop
        n := VectIn_src(i).fanout;
        b := std_logic'val((n mod 8)+1);
        if verbose_i > 1 then
          report "PortIn #" & integer'image(i)
               & " : " & integer'image(n)
               & " ==> " & std_logic'image(b);
        end if;
        DutVectIn(i) <= b;
        VectIn_src(i).fanout := n / 8;
      end loop;

      probe_phase := 1;
      wait for main_delay;

      -- Phase 3:
      -- check the outputs
      if verbose_i > 0 then
        report "Output: " & SLV_to_bin(DutVectOut);
      end if;

      for i in VectOut_sink'range loop
        b := DutVectOut(i);
        if b /= 'U' then  -- don't blast the warning in this loop
          VectOut_sink(i) :=
          VectOut_sink(i) + driver_number_type(
             (multiply_pass * (std_logic'pos(b) - 1)));
        end if;
      end loop;

      multiply_pass := multiply_pass * 8;
    end loop;

    -- The analysis of the results :
    if verbose_i > 0 then
      report " ###### Result : ######";
    end if;
    -- at this point, VectIn_src(all).fanout must be 0 by design
    --  but we must check all the others

    for i in VectOut_sink'range loop
      n := integer(VectOut_sink(i));
      if n = 0 then
        report "*** PortOut #" & integer'image(i) & " is not connected !";
        unconnected_inputs := 
        unconnected_inputs + 1;
      else
        if verbose_i > 0 then
          report "PortOut #" & integer'image(i)
               & " : "       & integer'image(n);
        end if;
        check_sink_source(VectOut_sink(i), -i, 0);
      end if;
    end loop;

    for i in GateList'range loop
      -- normally : .changes is divided down to 0 when all inputs are valid
      if GateList(i).changes /= 0 then
        report "*** Gate #" & integer'image(i) & " - " & GateList(i).gatename.all;
        for j in 0 to GateList(i).inputs-1 loop
          if GateList(i).sinks(gate_port_number_type(j)) = 0 then
            report "input #" & integer'image(j) & " not connected ?";
            unconnected_inputs := 
            unconnected_inputs + 1;
          end if;
        end loop;
      else
        for j in 0 to GateList(i).inputs-1 loop
          if verbose_i > 0 then
            report "Gate #" & integer'image(i)
                 & "(" & integer'image(j)
                 & ")=>" & driver_number_type'image(GateList(i).sinks(gate_port_number_type(j)))
                 & "   " & GateList(i).gatename.all;
          end if;
          check_sink_source(GateList(i).sinks(gate_port_number_type(j)), i, j);
        end loop;
      end if;
    end loop;

  -- Build the prefix sum to pre-allocate the lists of sinks :
  --  for each driver
  --    if  fanout > 1	then
  --      take counter, add fanout, put sum in index

    for i in VectIn_src'range loop
      n := VectIn_src(i).fanout;
      if n = 0 then
        report "PortIn #"  & integer'image(i) & " is not connected ?";
      end if;
      if n > 1 then
        sinks_lists_count :=
        sinks_lists_count + n;
        if verbose_i > 1 then
          report "PortIn #"  & integer'image(i)
            & "  fanout=" & integer'image(n)
            & "  slc=" & integer'image(sinks_lists_count);
        end if;
        VectIn_src(i).sink_address := sinks_lists_count;
      end if;
    end loop;

    for i in GateList'range loop
      n := GateList(i).driver.fanout;
      if n = 0 then
        report "Gate #"  & integer'image(i) & "'s output is not connected ? " & GateList(i).gatename.all;
      end if;
      if n > 1 then
        sinks_lists_count :=
        sinks_lists_count + n;
        if verbose_i > 1 then
          report "Gate #" & integer'image(i)
            & "  fanout=" & integer'image(n)
            & "  slc=" & integer'image(sinks_lists_count)
            & "  " & GateList(i).gatename.all;
        end if;
        GateList(i).driver.sink_address := sinks_lists_count;
      end if;
    end loop;

    assert unconnected_inputs = 0 and unconnected_outputs = 0
      report "Stopping because some inputs or outputs are left unconnected."
      severity failure;

    if verbose_i > 1 then
      report integer'image(sinks_lists_count) & " sinks in aux-list";
    end if;

    -- allocate sinks_lists when we know the precise size
    sinks_lists  := new sink_number_array(0 to sinks_lists_count-1);

    -- scan all the sinks
    --   for each sink, look at the driver
    --     if fanout > 1 then
    --       put the sink address at the index, and decrement it
    --    else put from in the index

    for i in GateList'high downto 1 loop
      for j in 0 to GateList(i).inputs-1 loop
        update_sink(GateList(i).sinks(gate_port_number_type(j)), (i*4) + j);
      end loop;
    end loop;

    for i in VectOut_sink'range loop
      update_sink(VectOut_sink(i), -i);
    end loop;

  end procedure;


-----------------------------------------------------------------

  procedure dumb_display_netlist is
    variable last : integer;

    procedure display_sink(s : integer) is
      variable d : integer;
    begin
      if s < 1 then
        report "   -> Output port #" & integer'image(-s);
      else
        d := s/4;
        report "   -> Gate #" & integer'image(d)
           & "(" & integer'image(s mod 4)
           & ") : " & GateList(d).gatename.all;
        assert d /= last
           report "Gate #" & integer'image(d) & " has identical inputs /!\"
           severity warning;
        last := d;
      end if;
    end procedure;

    procedure scan_sinks(d : driver_record) is
      variable base, f: integer;
    begin
      f := d.fanout;
      last := -1; -- bogus address that is not a gate
      if f < 1 then
        assert false
           report "Driver has no sink ! /!\"
           severity warning;
      else
        if f = 1 then
          display_sink(d.sink_address);
        else
          base := d.sink_address;
          for i in 1 to f loop
            display_sink(integer(sinks_lists(base)));
            base := base+1;
          end loop;
        end if;
      end if;
    end procedure;

  begin
    -- simply scan all the input ports and gates in arbitrary order.
    for i in VectIn_src'range loop
      report "* Input#" & integer'image(i);
      scan_sinks(VectIn_src(i));
    end loop;

    for i in GateList'range loop
      report "* Gate #" & integer'image(i) & " - " & GateList(i).gatename.all;
      scan_sinks(GateList(i).driver);
    end loop;
  end procedure;


-----------------------------------------------------------------
-- The main driver

  procedure Drive_DUT(
    signal DutVectIn :   out std_logic_vector;
    signal DutVectOut: in    std_logic_vector;
    signal DutVectClk: inout std_logic;
             Interval: in    time) is
  begin
    -- set & init the global values
    main_delay := Interval;
    DutVectIn_length  := DutVectIn'length;
    DutVectOut_length := DutVectOut'length;
    VectOut_sink := new driver_number_array(0 to DutVectOut_length-1); -- 0 based, also makes code easier
    VectIn_src := new driver_record_array(0 to DutVectIn'length-1);

    Probe_netlist( DutVectIn, DutVectOut, DutVectClk);

    dumb_display_netlist;

    wait;
  end Drive_DUT;

-----------------------------------------------------------------
-- Not very useful but it shows how to select and call the Driver.

  procedure Drive_DUT_dummy(
    signal DutVectIn :   out std_logic_vector;
    signal DutVectOut: in    std_logic_vector;
    signal DutVectClk: inout std_logic;
             Interval: in    time) is
  begin
    report   "VectIn: "  & integer'image(DutVectIn'length)
         & ", VectOut: " & integer'image(DutVectOut'length)
         & ", CLK: "     & std_logic'image(DutVectClk) ;
    wait;
  end Drive_DUT_dummy;

-----------------------------------------------------------------

end Netlist_lib;
