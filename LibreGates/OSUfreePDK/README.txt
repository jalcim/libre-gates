file LibreGates/OSUfreePDK/README.txt
Created ven. nov.  6 06:33:48 CET 2020

This directory implements a VHDL layer that emulates the standard cells
of the "OSU free PDK", which can be found at
https://vlsiarch.ecen.okstate.edu/flows/FreePDK_SRC/OSU_FreePDK.tar.gz

1 input:
"BUFX2": A
"BUFX4": A
"CLKBUF1": A
"CLKBUF2": A
"CLKBUF3": A
"INVX1": not A
"INVX2": not A
"INVX4": not A
"INVX8": not A

2 inputs :
"AND2X1":      (A and B)
"AND2X2":      (A and B)
"NAND2X1": not (A and B)
"NOR2X1" : not (A  or B)
"OR2X1":       (A  or B)
"OR2X2":       (A  or B)
"XNOR2X1": not (A xor B)
"XOR2X1":      (A xor B)

"TBUFX1": not A when EN='1' else 'Z'
"TBUFX2": not A when EN='1' else 'Z'

3 inputs :
"AOI21X1": not ((A and B) or C)
"MUX2X1":  not (A when S='1' else B)
"NAND3X1": not (A and B and C)
"NOR3X1":  not (A  or B  or C)
"OAI21X1": not ((A or B) and C)

4 inputs :
"AOI22X1": not ((C and D)  or (A and B))
"OAI22X1": not ((C  or D) and (A or B))

(Notice that MUX2 should be called MUXI.)

because there are 2 outputs,
extra Macros are used for

* HAX1:
    YC=A and B
    YS=A xor B
* FAX1:
    YC=(A and B) or (B and C) or (C and A)  "MAJ3"
    YS=A xor B xor C  "XOR3"

Sequential gates :
"LATCH"     maps to DLN1
"DFFPOSX1"  maps to DFN1
"DFFNEGX1"  maps to DFN0
"DFFSR"     DFN1P0C0 (n'existe pas?)
  https://vlsiarch.ecen.okstate.edu/flows/FreePDK_SRC/osu_freepdk_1.0/lib/html/data/DFFSR.html
No gate with "data enable" is provided though.

