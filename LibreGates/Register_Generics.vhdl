-- File LibreGates/Register_Generics.vhdl
-- version mar. sept.  1 07:04:40 CEST 2020 : split from PA3_definitions.vhdl
--
-- Released under the GNU AGPLv3 license

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

-- Note if you use the modes trace, alter, netlist :
-- /!\ You MUST write this in front/before including or instanciating the other gates !
-- It must be called first to initialise important values from the generics /!\
entity register_generics is
  generic( gate_select_number : integer :=  0;
            bit_select_number : integer := -1;
                     flip_LUT : integer :=  0;
            verbose, filename : string  := "");
  port(clock : in std_logic);
end register_generics;

architecture rtl of register_generics is
begin
  update_select_gate(gate_select_number, bit_select_number,
   flip_LUT, verbose, filename); -- called only once at start

  process(clock) begin
    case clock is
      when '1' => update_gates_histograms;
      when 'X' => display_gates_histograms;
      when others => NULL;
    end case;
  end process;
end rtl;

