-- file LibreGates/SLV_utils.vhdl
-- created lun. nov. 13 01:02:38 CET 2017 by Yann Guidon (whygee@f-cpu.org)
-- version dim. nov. 19 03:49:07 CET 2017 : added Num_to_int
-- version lun. déc.  2 23:49:03 CET 2019 : forked from YGREC8/YGREC8_utils.vhdl
-- version 20200806 : synthesis with Synplify
-- version lun. août 31 22:17:01 CEST 2020 : functions trim_name and trim_spaces moved from PA3_genlib.vhdl
-- version mer. sept.  2 20:14:42 CEST 2020 : LibreGates
--
-- Released under the GNU AGPLv3 license
--
-- These are some useful functions for using the Std_Logic_Vector,
-- some functions are shamelessly "borrowed" (forked) from yasep_utils.vhdl
-- and YGREC8_utils.vhdl (so their lineage is "interesting")
--
-- SYNTHESIS OK

Library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

package SLV_utils is
  subtype SL     is std_logic;
  subtype SLV2   is std_logic_vector( 1 downto 0);
  subtype SLV3   is std_logic_vector( 2 downto 0);
  subtype SLV4   is std_logic_vector( 3 downto 0);
  subtype SLV5   is std_logic_vector( 4 downto 0);
  subtype SLV6   is std_logic_vector( 5 downto 0);
  subtype SLV7   is std_logic_vector( 6 downto 0);
  subtype SLV8   is std_logic_vector( 7 downto 0);
  subtype SLV9   is std_logic_vector( 8 downto 0);
  subtype SLV10  is std_logic_vector( 9 downto 0);
  subtype SLV11  is std_logic_vector(10 downto 0);
  subtype SLV12  is std_logic_vector(11 downto 0);
  subtype SLV13  is std_logic_vector(12 downto 0);
  subtype SLV14  is std_logic_vector(13 downto 0);
  subtype SLV15  is std_logic_vector(14 downto 0);
  subtype SLV16  is std_logic_vector(15 downto 0);
  subtype SLV17  is std_logic_vector(16 downto 0);
  subtype SLV32  is std_logic_vector(31 downto 0);
  subtype SLV64  is std_logic_vector(63 downto 0);

  -- useful for txt to int conversion:
  type char_base_type is array (character) of integer;
  constant base_lookup : char_base_type := (
   '0'=>0,  '1'=>1,  '2'=>2,  '3'=>3,  '4'=>4,
   '5'=>5,  '6'=>6,  '7'=>7,  '8'=>8,  '9'=>9,
   'A'=>10, 'B'=>11, 'C'=>12, 'D'=>13, 'E'=>14,
   'F'=>15, others=>-1);

  -- error message for number conversion:
  type num_conv_error_type is array (0 to 5) of string(1 to 29);
  constant num_conv_error : num_conv_error_type := (
    "Conversion success           ",
    "Invalid number, empty string ",
    "Invalid digit                ",
    "Wrong base digit             ",
    "Invalid number, overflow     ",
    "Invalid number, no digit?    ");

  function safe_to_integer(u:std_logic_vector) return integer;
  function SLV_to_bin(s:std_logic_vector) return string;
  function SLV4_to_Hex(s:std_logic_vector(3 downto 0)) return character;
  function SLV_to_Hex(s:std_logic_vector) return string;
  procedure Num_to_int_p(txt: string; result, diag: out integer);
  function trim_name( orig, substring : string ) return string;
  function trim_spaces( orig : string ) return string;

end SLV_utils;

package body SLV_utils is

  function safe_to_integer(u:std_logic_vector) return integer is
  begin
   if Is_X(u) then
      return 0;
    else
      return to_integer(unsigned(u));
    end if;
  end safe_to_integer;

  function SLV_to_bin(s:std_logic_vector) return string is
    variable t : string(s'range);
    variable u : string(3 downto 1);
  begin
    for i in s'range loop
      u := std_logic'image(s(i));
      t(i) := u(2);
    end loop;
    return t;
  end SLV_to_bin;

  function SLV4_to_Hex(s: std_logic_vector(3 downto 0)) return character is
  begin
    case s is
      when "0000" => return '0';
      when "0001" => return '1';
      when "0010" => return '2';
      when "0011" => return '3';
      when "0100" => return '4';
      when "0101" => return '5';
      when "0110" => return '6';
      when "0111" => return '7';
      when "1000" => return '8';
      when "1001" => return '9';
      when "1010" => return 'A';
      when "1011" => return 'B';
      when "1100" => return 'C';
      when "1101" => return 'D';
      when "1110" => return 'E';
      when "1111" => return 'F';
      when others => return '?';
    end case;
  end SLV4_to_Hex;

  function SLV_to_Hex(s:std_logic_vector) return string is
    variable u: std_logic_vector(s'length+3 downto 0);
    variable t: string((s'length-1)/4 downto 0);
    variable i: integer:= 0;
    variable j: integer:= 0;
  begin
    u:= "0000" & s;
    while (i < s'length) loop
      t(j) := SLV4_to_Hex(u(i+3 downto i));
      i := i+4;
      j := j+1;
    end loop;
    return t;
  end SLV_to_Hex;

  procedure Num_to_int_p(txt: string; result, diag: out integer) is
    variable c : character;
    variable d, i, j, n, n2, high : integer := 0;
    variable base : integer := 10;
    variable sign : integer := 1;
  begin
    diag:=0;
    -- txt should contain only "valid characters" and no space.
    -- overflow is not well managed though. At least is must work
    -- nicely up to 2**12.

    -- but first, read the base at the trailing end of the string
    high := txt'high; -- and change the length accordingly.
    if high <1 then
      diag:=1;
      return;
    end if;

    case txt(high) is
      when 'H' => base := 16 ; high := high-1;
      when 'B' => base :=  2 ; high := high-1;
      when others =>
    end case;

    -- get the sign
    i := 1;
    if high > 1 then
      c := txt(i);
      if c='-' then
        sign := -1;
        i := i+1;
      end if;
    end if;

    convert: while (i <= high) loop
      c := txt(i);
      d := base_lookup(c);
      if d < 0 then
        diag:=2;
        return;
      end if;
      if d >= base then
        diag:=3;
        return;
      end if;
      n2 := (n*base) + d;
      if n > n2 then
        diag:=4;
        return;
      end if;
      n := n2;
      i := i+1;
      j := j+1;
    end loop convert;

    if j <= 0 then
      diag:=5;
      return;
    end if;

    result := sign*n;
    return;
  end Num_to_int_p;

  -- Trim the beginning of a full instance name, keeps only the string
  -- AFTER the first occurence of the desired substring (if found)
  function trim_name( orig, substring : string ) return string is
    variable i, high, len : integer;
    variable c : character;
  begin
    c := substring(substring'low);
    len := substring'length;
    i := orig'low;
    high := orig'high;

    -- seek the first occurence of c in orig
    loop
      if i >= high then
        return orig; -- nothing found, give up
      end if;

      if orig(i) = c then
        if (i+len) >= high then
          return orig; -- nothing found, give up
        end if;

        if substring = orig(i to (i+len)-1) then
          return orig(i+len to high);
        end if;
      end if;
      i := i+1;
    end loop;
  end;

  -- remove the spaces at the end of a string.
  function trim_spaces( orig : string ) return string is
    variable low, high : integer;
  begin
    low := orig'low;
    high := orig'high;

    loop
      if low = high     -- might not return a null string
       or orig(high) /= ' ' then
        return orig(low to high);
      end if;

      high := high - 1;
    end loop;
  end;

end SLV_utils;
