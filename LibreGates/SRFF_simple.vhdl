-- file LibreGates/SRFF_simple.vhdl
-- created mar. août 25 02:51:59 CEST 2020 by whygee@f-cpu.org
-- version mar. sept.  1 04:02:57 CEST 2020 : LibreGates !
--
-- This is the basic version and the initial definition for
-- various Set/Reset Flip-Flops, defined by the precedence
-- of the control signals and their active levels.
--
-- Precedence  Set  Reset  Macro
--            level level  name  Mapped to
--      Set     0     0    S0R0   AO1A
--      Set     0     1    S0R1   AO1C
--      Set     1     0    S1R0   AO1, AON21
--      Set     1     1    S1R1   AO1B, DLI1P1C1, AON21B
--    Reset     0     0    R0S0   OA1A
--    Reset     0     1    R1S0   OA1C
--    Reset     1     0    R0S1   OA1, OAN21
--    Reset     1     1    R1S1   OA1B, DLN1P1C1, OAN21B
--

-------------------- SET precedence --------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity S0R0 is
  port( S, R : in std_logic;
           Y : out std_logic);
end S0R0;

architecture simple of S0R0 is
begin
  Y <= '1' when S='0'
  else '0' when R='0';
end simple;


Library ieee;
    use ieee.std_logic_1164.all;

entity S0R1 is
  port( S, R : in std_logic;
           Y : out std_logic);
end S0R1;

architecture simple of S0R1 is
begin
  Y <= '1' when S='0'
  else '0' when R='1';
end simple;


Library ieee;
    use ieee.std_logic_1164.all;

entity S1R0 is
  port( S, R : in std_logic;
           Y : out std_logic);
end S1R0;

architecture simple of S1R0 is
begin
  Y <= '1' when S='1'
  else '0' when R='0';
end simple;


Library ieee;
    use ieee.std_logic_1164.all;

entity S1R1 is
  port( S, R : in std_logic;
           Y : out std_logic);
end S1R1;

architecture simple of S1R1 is
begin
  Y <= '1' when S='1'
  else '0' when R='1';
end simple;


-------------------- RESET precedence --------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity R0S0 is
  port( S, R : in std_logic;
           Y : out std_logic);
end R0S0;

architecture simple of R0S0 is
begin
  Y <= '0' when R='0'
  else '1' when S='0';
end simple;


Library ieee;
    use ieee.std_logic_1164.all;

entity R1S0 is
  port( S, R : in std_logic;
           Y : out std_logic);
end R1S0;

architecture simple of R1S0 is
begin
  Y <= '0' when R='1'
  else '1' when S='0';
end simple;


Library ieee;
    use ieee.std_logic_1164.all;

entity R0S1 is
  port( S, R : in std_logic;
           Y : out std_logic);
end R0S1;

architecture simple of R0S1 is
begin
  Y <= '0' when R='0'
  else '1' when S='1';
end simple;

Library ieee;
    use ieee.std_logic_1164.all;

entity R1S1 is
  port( S, R : in std_logic;
           Y : out std_logic);
end R1S1;

architecture simple of R1S1 is
begin
  Y <= '0' when R='1'
  else '1' when S='1';
end simple;
