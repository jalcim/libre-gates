#!/bin/bash
# LibreGates/build.sh
# version mar. sept.  1 05:53:29 CEST 2020

VER="--std=93"

# some cleanup :
rm -rf *.cf *.o &&

{
  # compile the SLV utilities
  ghdl -a $VER --work=SLV SLV_utils.vhdl &
  # compile the histogram definitions
  ghdl -a $VER --work=Histo Histograms.vhdl &
} &&
wait &&

# compile the gates structure:
ghdl -a $VER --work=LibreGates Gates_lib.vhdl \
    Register_Generics.vhdl Netlist_lib.vhdl &&

echo $'\e[32m'"LibreGates Library generation : OK"$'\e[0m' &&

### 0.33s

pushd proasic3 &&
  ./build.sh &&
popd
