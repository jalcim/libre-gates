# this script expects that the DUT is implemented in the "wrapper" executable.

./wrapper |
  sed 's/.*note): //' |
  grep ' Gate #'|
  sed 's/(trace):lut.*//' |
  sed 's/.*@//' |
  sort|
  uniq -c|
  tee census.out
