
# parallel.sh
# created 2020/11/12 by Yann Guidon
# version ven. nov. 13 11:58:04 CET 2020 : used by INC8 and ALU8
#
# Source this file to parallelise the fault insertion check.

# if NBTHREADS is not set or invalid,
#   then query the system
[[ $(( NBTHREADS + 0 )) -eq 0 ]] &&
  NBTHREADS=$( { which nproc > /dev/null 2>&1 ; } && nproc ||
     grep -c ^processor /proc/cpuinfo )

echo "using $NBTHREADS parallel threads"

err_flag=0

function ScanErrors () {
{  # $1 : testbench program name
   # $2 : optional name of a function to "fix" the DEPTH_LIST

  # get a list of all the LUT sizes
  DEPTH_LIST=$( $1 -gflip=0 -ggate_select_number=-1 |
        grep '  gate#' |sed 's/.*(trace):lut//' ) &&

  [[ "$2" ]] && {
     DEPTH_LIST=$( $2 $DEPTH_LIST )
     # echo "corrected list : " $DEPTH_LIST
  }

  # estimate the quantity of tests to run
  MAXTEST=$(( $(echo $DEPTH_LIST" 0" |sed 's/2 //g'|sed 's/ /+/g') ))

  err_flag=0
  threads=0

  k=1 && # first gate is n°1
  l=0 &&
  rm -f cycles.log &&

  # scan the list to extract the depth
  for i in $DEPTH_LIST ; do
    # for each bit in the LUT:
    if [ $i \> 2 ]; then  # don't test gates with fewer than 2 inputs
      for j in $( seq 0 $((i-1)) ) ; do
        l=$((l+1))
        threads=$(( threads + 1 ))
        echo -n $'\r'"Run #"$l"/$MAXTEST  Gate#"$k'  LUT'$i'['$j']  '

        { # The parallel workload

          #  inject an error :
          # [[ $k -eq "20" ]] && [[ $j -eq "1" ]] && { echo "here is an error for you." ; return 1; }

          RES=$( $1 -gflip=1 -ggate_select_number=$k -gbit_select_number=$j )
          [[ "$?" -eq "0" ]] && {
            echo -e '\n'$'\e[31m'"Problem at Run #"$l", Gate#"$k', LUT'$i'['$j'] : No error detected !'$'\e[0m'
            return 1
          }

          [[ "$RES" ]] || {
            echo -e '\n'$'\e[31m'"Error at Run #"$l", Gate#"$k', LUT'$i'['$j'] : Empty result ???'$'\e[0m'
            return 1
          }

          # don't count, just ignore these cases.
          [[ $RES == *"reached excluded state"*              ]] && return 0
          [[ $RES == *"Can't flip unreachable bit of gate#"* ]] && return 0

          [[ $RES == *"e): Error at cycle#"* ]] || {
            echo -e '\n'$'\e[31m'"Error at Run #"$l", Gate#"$k', LUT'$i'['$j"] : Can't find cycle count"$'\e[0m'
            echo $RES
            return 1
          }

          RES=$( echo $RES |
            sed 's/.*: Error at cycle#//' |
            sed 's/ .*//' ) # extract only the number of cycles
          echo $RES >> cycles.log # split operation
            # prevents sed from stalling and breaking atomicity of the write

          return 0 # success
        } &

        # load limiter:
        [[ $threads -ge $NBTHREADS ]] &&  {
          wait -n || { # wait until one previous thread completes
            err_flag=1
            break 2; }
          threads=$(( threads - 1 ))
        }
      done
    fi
    k=$((k+1))
  done

  [[ $err_flag -ne "0" ]] && echo "flushing..."
  until [[ "$threads" -eq "0" ]] ; do
    wait -n || err_flag=1 # don't break on this one !
    threads=$(( threads - 1 ))
  done

  } 2> /dev/null && # hide bash's job termination messages

  total=0
  while IFS= read -r line
  do
    total=$(( total + line ))
  done <"cycles.log"

  [[ $err_flag -eq "0" ]] && {
    echo -e "\nScan succeeded in $total cycles"
  }
}
