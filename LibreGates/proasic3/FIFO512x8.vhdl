-- FIFO512x8.vhd
-- created by whygee@f-cpu.org
-- More or less generic FIFO with 8 bits in, 8 bits out, 512 bytes deep.
-- A MicroSemi/Actel version is provided, derived (cleaned up) from the Libero code generator.
-- It is loosely simulated with a limited-depth, overflowing dumb array for early simulations purposes.
-- version 20150105 : merged the dumb and A3P version to ease tests.
-- version 20150106 : WEN is active-low and REN is active-high !!

library ieee;
use ieee.std_logic_1164.all;
library proasic3;
use proasic3.all;

entity FIFO512x8 is 
  port(
    DATA : in std_logic_vector(7 downto 0);
    Q : out std_logic_vector(7 downto 0);
    WE, RE, WCLOCK, RCLOCK : in std_logic;
    FULL, EMPTY : out std_logic;
    RESET : in std_logic) ;
end FIFO512x8;

architecture A3P of FIFO512x8 is
  component FIFO4K18
    port(
      AEVAL11, AEVAL10, AEVAL9, AEVAL8, AEVAL7, AEVAL6, 
      AEVAL5, AEVAL4, AEVAL3, AEVAL2, AEVAL1, AEVAL0, AFVAL11, 
      AFVAL10, AFVAL9, AFVAL8, AFVAL7, AFVAL6, AFVAL5, AFVAL4, 
      AFVAL3, AFVAL2, AFVAL1, AFVAL0, WD17, WD16, WD15, WD14, 
      WD13, WD12, WD11, WD10, WD9, WD8, WD7, WD6, WD5, WD4, WD3, 
      WD2, WD1, WD0, WW0, WW1, WW2, RW0, RW1, RW2, RPIPE, WEN, 
      REN, WBLK, RBLK, WCLK, RCLK, RESET, ESTOP, FSTOP : in std_logic := 'U';
      RD17, RD16, RD15, RD14, RD13, RD12, 
      RD11, RD10, RD9, RD8, RD7, RD6, RD5, RD4, RD3, RD2, RD1, 
      RD0, FULL, AFULL, EMPTY, AEMPTY : out std_logic) ;
    end component;
begin
  FIFOBLOCK0 : FIFO4K18
    port map(
      AEVAL11 => '0', AEVAL10 => '0', AEVAL9 => '0', AEVAL8 => '0',
      AEVAL7  => '0', AEVAL6  => '0', AEVAL5 => '0', AEVAL4 => '0',
      AEVAL3  => '0', AEVAL2  => '0', AEVAL1 => '0', AEVAL0 => '0', 
      AFVAL11 => '0', AFVAL10 => '0', AFVAL9 => '0', AFVAL8 => '0',
      AFVAL7  => '0', AFVAL6  => '0', AFVAL5 => '0', AFVAL4 => '0',
      AFVAL3  => '0', AFVAL2  => '0', AFVAL1 => '0', AFVAL0 => '0',
      WD17 => '1', WD16 => '1', WD15 => '1', WD14 => '1', WD13 => '1',
      WD12 => '1', WD11 => '1', WD10 => '1', WD9  => '1', WD8  => '1',
      WD7 => DATA(7), WD6 => DATA(6), WD5 => DATA(5), WD4 => DATA(4),
      WD3 => DATA(3), WD2 => DATA(2), WD1 => DATA(1), WD0 => DATA(0),
      WW0 => '1', WW1 => '1', WW2 => '0', -- 9 bits
      RW0 => '1', RW1 => '1', RW2 => '0', -- 9 bits
      RPIPE => '0', WEN => WE, REN => RE, WBLK => '0', RBLK => '0',
      WCLK => WCLOCK, RCLK => RCLOCK, RESET => RESET, ESTOP => '1', FSTOP => '1',

      RD17 => OPEN, RD16 => OPEN, RD15 => OPEN, RD14 => OPEN,
      RD13 => OPEN, RD12 => OPEN, RD11 => OPEN, RD10 => OPEN,
      RD9  => OPEN, RD8  => OPEN,
      RD7 => Q(7), RD6 => Q(6), RD5 => Q(5), RD4 => Q(4),
      RD3 => Q(3), RD2 => Q(2), RD1 => Q(1), RD0 => Q(0),
      FULL => FULL, AFULL => OPEN , EMPTY => EMPTY, AEMPTY => OPEN );
end A3P;

-- PA3 :  /win/data/pdf/Actel/PA3_HB.pdf
-- Table 6-6  Aspect Ratio Settings for WW[2:0]
--  WW[2:0]      RW[2:0]  DxW
--  000          000     4kx1
--  001          001     2kx2
--  010          010     1kx4
--  011          011    512x9
--  100          100   256x18
-- WBLK and RBLK : low (enable port)
-- RPIPE low : no pipeline
-- ESTOP, FSTOP : high (no wraparound)

-------------------------------------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

-- DUMB : does not wrap around, useful only for preliminary behavioural tests
architecture dumb of FIFO512x8 is
  constant MAX_DATA : integer := 10000;  -- make it a generic ?
  type mem_type is array (MAX_DATA-1 downto 0) of integer;
  shared variable mem: mem_type;
  subtype index is integer range 0 to MAX_DATA-1;
  signal RD_index, WR_index: index := 0;
begin

  EMPTY <= '1' when RD_index >= WR_index else '0';
  FULL  <= '1' when WR_index >= MAX_DATA  else '0';

  -- write process
  process(WCLOCK, RESET)
  begin
    if (RESET='0') then
      WR_index <= 0;
    else
      if (WCLOCK'event and WCLOCK='1' and WE='0') then
        assert (WR_index < MAX_DATA-1) report "FIFO OVERFLOW" severity error;
        mem(WR_index) := to_integer(unsigned(DATA));
--        report " writing " & integer'image(mem(WR_index)) & " @" & integer'image(WR_index);
        WR_index <= WR_index+1;
      end if;
    end if;
  end process;

  -- read process
  process(RCLOCK, RESET)
  begin
    if (RESET='0') then
      Q <= (Q'range=>'0');
      RD_index <= 0;
    else
      if (RCLOCK'event and RCLOCK='1' and RE='1') then
        assert (RD_index < WR_index) report "FIFO UNDERFLOW" severity error;
        Q <= std_logic_vector(to_unsigned( mem(RD_index), 8));
--        report " reading " & integer'image(mem(RD_index)) & " @" & integer'image(RD_index);
        RD_index <= RD_index+1;
      end if;
    end if;
  end process;

end dumb;
