-- LibreGates/proasic3/PA3_combi.vhdl 
-- mar. nov. 26 02:20:31 CET 2019
-- version dim. août 16 06:40:14 CEST 2020 : removed the useless inclusion of work.PA3_genlib.all
-- version mer. sept.  2 20:16:42 CEST 2020 : LibreGates !
--
-- These alternative architectures break down the gate
-- for the cases where the activity histograms show that
-- testing might be impossible due to unreachable input states.

Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library proasic3;
    use proasic3.all;

entity ao1_combi is
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao1_combi;

-- AO1 => Y := (A and B) or C;
architecture combi of ao1_combi is
  signal t : std_logic;
begin
 e_a: entity AND2 port map(A=>A, B=>B, Y=>t);
 e_o: entity OR2  port map(A=>C, B=>t, Y=>Y);
end combi;


Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library proasic3;
    use proasic3.all;

entity oa1a_combi is
  port( A, B, C : in std_logic;
              Y : out std_logic);
end oa1a_combi;

-- OA1A => Y := ((not A) or B) and C ;
architecture combi of oa1a_combi is
  signal t : std_logic;
begin
 e_o: entity OR2A port map(A=>A, B=>B, Y=>t);
 e_a: entity AND2 port map(A=>C, B=>t, Y=>Y);
end combi;
