-- File : LibreGates/proasic3/PA3_components.vhdl
-- DO NOT MODIFY ! Generated file !
-- (C) Yann Guidon 20190808 - 20200902

Library ieee;
    use ieee.std_logic_1164.all;

package proasic3 is
  component generic_gate1 generic(exclude: std_logic_vector); port(A          : in std_logic; LUT : in std_logic_vector; Y : out std_logic); end component;
  component generic_gate2 generic(exclude: std_logic_vector); port(A, B       : in std_logic; LUT : in std_logic_vector; Y : out std_logic); end component;
  component generic_gate3 generic(exclude: std_logic_vector); port(A, B, C    : in std_logic; LUT : in std_logic_vector; Y : out std_logic); end component;
  component generic_gate4 generic(exclude: std_logic_vector); port(A, B, C, D : in std_logic; LUT : in std_logic_vector; Y : out std_logic); end component;

-- 0-input gates :
  component GND generic(exclude: std_logic_vector); port(Y : out std_logic); end component;
  component VCC generic(exclude: std_logic_vector); port(Y : out std_logic); end component;

-- 1-input gates :
  component BUFD   generic(exclude: std_logic_vector); port(A : in std_logic; Y : out std_logic); end component;
  component BUFF   generic(exclude: std_logic_vector); port(A : in std_logic; Y : out std_logic); end component;
  component CLKINT generic(exclude: std_logic_vector); port(A : in std_logic; Y : out std_logic); end component;
  component INV    generic(exclude: std_logic_vector); port(A : in std_logic; Y : out std_logic); end component;
  component INVD   generic(exclude: std_logic_vector); port(A : in std_logic; Y : out std_logic); end component;

-- 2-inputs gates :
  component and2    generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- 0001
  component and2a   generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- 0100
  component and2b   generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- 1000
  component nand2   generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- 1110
  component nand2a  generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- 1011
  component nand2b  generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- 0111
  component nor2    generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- 1000
  component nor2a   generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- 0010
  component nor2b   generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- 0001
  component or2     generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- 0111
  component or2a    generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- 1101
  component or2b    generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- 1110
  component xnor2   generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- 1001
  component xor2    generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- 0110

-- 3-inputs gates :
  component and3    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00000001
  component and3a   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00010000
  component and3b   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01000000
  component and3c   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10000000
  component ao1     generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01010111
  component ao12    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10110110
  component ao13    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00101011
  component ao14    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01101011
  component ao15    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10010100
  component ao16    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01000010
  component ao17    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01100001
  component ao18    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10110010
  component ao1a    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01110101
  component ao1b    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10101011
  component ao1c    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10111010
  component ao1d    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 11010101
  component ao1e    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 11101010
  component aoi1    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10101000
  component aoi1a   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10001010
  component aoi1b   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01010100
  component aoi1c   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00101010
  component aoi1d   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00010101
  component aoi5    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 11100111
  component ax1     generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01100101
  component ax1a    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10011010
  component ax1b    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10010101
  component ax1c    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01010110
  component ax1d    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01101010
  component ax1e    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10101001
  component axo1    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01100111
  component axo2    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01110110
  component axo3    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01101110
  component axo5    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10111001
  component axo6    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10011101
  component axo7    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 11100110
  component axoi1   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10011000
  component axoi2   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10001001
  component axoi3   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10010001
  component axoi4   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01100100
  component axoi5   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01000110
  component axoi7   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00011001
  component maj3    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00010111
  component maj3x   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00010110
  component maj3xi  generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 11101001
  component min3    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 11101000
  component min3x   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01101000
  component min3xi  generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10010111
  component mx2     generic(exclude: std_logic_vector); port(A, B, S : in std_logic; Y : out std_logic); end component; -- 00011011
  component mx2a    generic(exclude: std_logic_vector); port(A, B, S : in std_logic; Y : out std_logic); end component; -- 10110001
  component mx2b    generic(exclude: std_logic_vector); port(A, B, S : in std_logic; Y : out std_logic); end component; -- 01001110
  component mx2c    generic(exclude: std_logic_vector); port(A, B, S : in std_logic; Y : out std_logic); end component; -- 11100100
  component nand3   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 11111110
  component nand3a  generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 11101111
  component nand3b  generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10111111
  component nand3c  generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01111111
  component nor3    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10000000
  component nor3a   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00001000
  component nor3b   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00000010
  component nor3c   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00000001
  component oa1     generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00010101
  component oa1a    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01010001
  component oa1b    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00101010
  component oa1c    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10100010
  component oai1    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 11101010
  component or3     generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01111111
  component or3a    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 11110111
  component or3b    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 11111101
  component or3c    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 11111110
  component xa1     generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00010100
  component xa1a    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01000001
  component xa1b    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 00101000
  component xa1c    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10000010
  component xai1    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 11101011
  component xai1a   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10111110
  component xo1     generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01111101
  component xo1a    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 11010111
  component xor3    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01101001
  component xnor3   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10010110
  component zor3    generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 10000001
  component zor3i   generic(exclude: std_logic_vector); port(A, B, C : in std_logic; Y : out std_logic); end component; -- 01111110

-- 4-inputs gates :
  component aoi211   generic(exclude: std_logic_vector); port(A, B, C, D : in std_logic; Y : out std_logic); end component; -- 1000100010000000
  component aoi22    generic(exclude: std_logic_vector); port(A, B, C, D : in std_logic; Y : out std_logic); end component; -- 1110111011100000
  component oai211   generic(exclude: std_logic_vector); port(A, B, C, D : in std_logic; Y : out std_logic); end component; -- 1111111011101110
  component oai22    generic(exclude: std_logic_vector); port(A, B, C, D : in std_logic; Y : out std_logic); end component; -- 1111100010001000

-- Sequential gates :
  component DFN0     generic(exclude: std_logic_vector); port(D,    CLK           : in std_logic;  Q : out std_logic); end component;
  component DFN1     generic(exclude: std_logic_vector); port(D,    CLK           : in std_logic;  Q : out std_logic); end component;
  component DFI0     generic(exclude: std_logic_vector); port(D,    CLK           : in std_logic; QN : out std_logic); end component;
  component DFI1     generic(exclude: std_logic_vector); port(D,    CLK           : in std_logic; QN : out std_logic); end component;
  component DFN0C0   generic(exclude: std_logic_vector); port(D,    CLK     , CLR : in std_logic;  Q : out std_logic); end component;
  component DFN1C0   generic(exclude: std_logic_vector); port(D,    CLK     , CLR : in std_logic;  Q : out std_logic); end component;
  component DFI0C0   generic(exclude: std_logic_vector); port(D,    CLK     , CLR : in std_logic; QN : out std_logic); end component;
  component DFI1C0   generic(exclude: std_logic_vector); port(D,    CLK     , CLR : in std_logic; QN : out std_logic); end component;
  component DFN0C1   generic(exclude: std_logic_vector); port(D,    CLK     , CLR : in std_logic;  Q : out std_logic); end component;
  component DFN1C1   generic(exclude: std_logic_vector); port(D,    CLK     , CLR : in std_logic;  Q : out std_logic); end component;
  component DFI0C1   generic(exclude: std_logic_vector); port(D,    CLK     , CLR : in std_logic; QN : out std_logic); end component;
  component DFI1C1   generic(exclude: std_logic_vector); port(D,    CLK     , CLR : in std_logic; QN : out std_logic); end component;
  component DFN0E0   generic(exclude: std_logic_vector); port(D, E, CLK           : in std_logic;  Q : out std_logic); end component;
  component DFN1E0   generic(exclude: std_logic_vector); port(D, E, CLK           : in std_logic;  Q : out std_logic); end component;
  component DFI0E0   generic(exclude: std_logic_vector); port(D, E, CLK           : in std_logic; QN : out std_logic); end component;
  component DFI1E0   generic(exclude: std_logic_vector); port(D, E, CLK           : in std_logic; QN : out std_logic); end component;
  component DFN0E0C0 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic;  Q : out std_logic); end component;
  component DFN1E0C0 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic;  Q : out std_logic); end component;
  component DFI0E0C0 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic; QN : out std_logic); end component;
  component DFI1E0C0 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic; QN : out std_logic); end component;
  component DFN0E0C1 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic;  Q : out std_logic); end component;
  component DFN1E0C1 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic;  Q : out std_logic); end component;
  component DFI0E0C1 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic; QN : out std_logic); end component;
  component DFI1E0C1 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic; QN : out std_logic); end component;
  component DFN0E0P0 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic;  Q : out std_logic); end component;
  component DFN1E0P0 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic;  Q : out std_logic); end component;
  component DFI0E0P0 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic; QN : out std_logic); end component;
  component DFI1E0P0 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic; QN : out std_logic); end component;
  component DFN0E0P1 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic;  Q : out std_logic); end component;
  component DFN1E0P1 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic;  Q : out std_logic); end component;
  component DFI0E0P1 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic; QN : out std_logic); end component;
  component DFI1E0P1 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic; QN : out std_logic); end component;
  component DFN0E1   generic(exclude: std_logic_vector); port(D, E, CLK           : in std_logic;  Q : out std_logic); end component;
  component DFN1E1   generic(exclude: std_logic_vector); port(D, E, CLK           : in std_logic;  Q : out std_logic); end component;
  component DFI0E1   generic(exclude: std_logic_vector); port(D, E, CLK           : in std_logic; QN : out std_logic); end component;
  component DFI1E1   generic(exclude: std_logic_vector); port(D, E, CLK           : in std_logic; QN : out std_logic); end component;
  component DFN0E1C0 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic;  Q : out std_logic); end component;
  component DFN1E1C0 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic;  Q : out std_logic); end component;
  component DFI0E1C0 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic; QN : out std_logic); end component;
  component DFI1E1C0 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic; QN : out std_logic); end component;
  component DFN0E1C1 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic;  Q : out std_logic); end component;
  component DFN1E1C1 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic;  Q : out std_logic); end component;
  component DFI0E1C1 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic; QN : out std_logic); end component;
  component DFI1E1C1 generic(exclude: std_logic_vector); port(D, E, CLK     , CLR : in std_logic; QN : out std_logic); end component;
  component DFN0E1P0 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic;  Q : out std_logic); end component;
  component DFN1E1P0 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic;  Q : out std_logic); end component;
  component DFI0E1P0 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic; QN : out std_logic); end component;
  component DFI1E1P0 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic; QN : out std_logic); end component;
  component DFN0E1P1 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic;  Q : out std_logic); end component;
  component DFN1E1P1 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic;  Q : out std_logic); end component;
  component DFI0E1P1 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic; QN : out std_logic); end component;
  component DFI1E1P1 generic(exclude: std_logic_vector); port(D, E, CLK, PRE      : in std_logic; QN : out std_logic); end component;
  component DFN0P0   generic(exclude: std_logic_vector); port(D,    CLK, PRE      : in std_logic;  Q : out std_logic); end component;
  component DFN1P0   generic(exclude: std_logic_vector); port(D,    CLK, PRE      : in std_logic;  Q : out std_logic); end component;
  component DFI0P0   generic(exclude: std_logic_vector); port(D,    CLK, PRE      : in std_logic; QN : out std_logic); end component;
  component DFI1P0   generic(exclude: std_logic_vector); port(D,    CLK, PRE      : in std_logic; QN : out std_logic); end component;
  component DFN0P1   generic(exclude: std_logic_vector); port(D,    CLK, PRE      : in std_logic;  Q : out std_logic); end component;
  component DFN1P1   generic(exclude: std_logic_vector); port(D,    CLK, PRE      : in std_logic;  Q : out std_logic); end component;
  component DFI0P1   generic(exclude: std_logic_vector); port(D,    CLK, PRE      : in std_logic; QN : out std_logic); end component;
  component DFI1P1   generic(exclude: std_logic_vector); port(D,    CLK, PRE      : in std_logic; QN : out std_logic); end component;
  component DFN0P1C1 generic(exclude: std_logic_vector); port(D,    CLK, PRE, CLR : in std_logic;  Q : out std_logic); end component;
  component DFN1P1C1 generic(exclude: std_logic_vector); port(D,    CLK, PRE, CLR : in std_logic;  Q : out std_logic); end component;
  component DFI0P1C1 generic(exclude: std_logic_vector); port(D,    CLK, PRE, CLR : in std_logic; QN : out std_logic); end component;
  component DFI1P1C1 generic(exclude: std_logic_vector); port(D,    CLK, PRE, CLR : in std_logic; QN : out std_logic); end component;
  component DLI0     generic(exclude: std_logic_vector); port(D,    G             : in std_logic; QN : out std_logic); end component;
  component DLI1     generic(exclude: std_logic_vector); port(D,    G             : in std_logic; QN : out std_logic); end component;
  component DLN0     generic(exclude: std_logic_vector); port(D,    G             : in std_logic;  Q : out std_logic); end component;
  component DLN1     generic(exclude: std_logic_vector); port(D,    G             : in std_logic;  Q : out std_logic); end component;
  component DLI0C0   generic(exclude: std_logic_vector); port(D,    G       , CLR : in std_logic; QN : out std_logic); end component;
  component DLI1C0   generic(exclude: std_logic_vector); port(D,    G       , CLR : in std_logic; QN : out std_logic); end component;
  component DLN0C0   generic(exclude: std_logic_vector); port(D,    G       , CLR : in std_logic;  Q : out std_logic); end component;
  component DLN1C0   generic(exclude: std_logic_vector); port(D,    G       , CLR : in std_logic;  Q : out std_logic); end component;
  component DLI0C1   generic(exclude: std_logic_vector); port(D,    G       , CLR : in std_logic; QN : out std_logic); end component;
  component DLI1C1   generic(exclude: std_logic_vector); port(D,    G       , CLR : in std_logic; QN : out std_logic); end component;
  component DLN0C1   generic(exclude: std_logic_vector); port(D,    G       , CLR : in std_logic;  Q : out std_logic); end component;
  component DLN1C1   generic(exclude: std_logic_vector); port(D,    G       , CLR : in std_logic;  Q : out std_logic); end component;
  component DLI0P0   generic(exclude: std_logic_vector); port(D,    G  , PRE      : in std_logic; QN : out std_logic); end component;
  component DLI1P0   generic(exclude: std_logic_vector); port(D,    G  , PRE      : in std_logic; QN : out std_logic); end component;
  component DLN0P0   generic(exclude: std_logic_vector); port(D,    G  , PRE      : in std_logic;  Q : out std_logic); end component;
  component DLN1P0   generic(exclude: std_logic_vector); port(D,    G  , PRE      : in std_logic;  Q : out std_logic); end component;
  component DLI0P1   generic(exclude: std_logic_vector); port(D,    G  , PRE      : in std_logic; QN : out std_logic); end component;
  component DLI1P1   generic(exclude: std_logic_vector); port(D,    G  , PRE      : in std_logic; QN : out std_logic); end component;
  component DLN0P1   generic(exclude: std_logic_vector); port(D,    G  , PRE      : in std_logic;  Q : out std_logic); end component;
  component DLN1P1   generic(exclude: std_logic_vector); port(D,    G  , PRE      : in std_logic;  Q : out std_logic); end component;
  component DLI0P1C1 generic(exclude: std_logic_vector); port(D,    G  , PRE, CLR : in std_logic; QN : out std_logic); end component;
  component DLI1P1C1 generic(exclude: std_logic_vector); port(D,    G  , PRE, CLR : in std_logic; QN : out std_logic); end component;
  component DLN0P1C1 generic(exclude: std_logic_vector); port(D,    G  , PRE, CLR : in std_logic;  Q : out std_logic); end component;
  component DLN1P1C1 generic(exclude: std_logic_vector); port(D,    G  , PRE, CLR : in std_logic;  Q : out std_logic); end component;


  component RAM4K9 port(
    ADDRA11, ADDRA10, ADDRA9, ADDRA8, ADDRA7, ADDRA6,
    ADDRA5,  ADDRA4,  ADDRA3, ADDRA2, ADDRA1, ADDRA0,
    ADDRB11, ADDRB10, ADDRB9, ADDRB8, ADDRB7, ADDRB6,
    ADDRB5,  ADDRB4,  ADDRB3, ADDRB2, ADDRB1, ADDRB0,
    DINA8, DINA7, DINA6, DINA5, DINA4, DINA3, DINA2, DINA1, DINA0,
    DINB8, DINB7, DINB6, DINB5, DINB4, DINB3, DINB2, DINB1, DINB0,
    WIDTHA0, WIDTHA1, WIDTHB0, WIDTHB1, PIPEA, PIPEB, WMODEA, WMODEB,
    BLKA, BLKB, WENA, WENB, CLKA, CLKB, RESET : in std_logic := 'U';
    DOUTA8, DOUTA7, DOUTA6, DOUTA5, DOUTA4, DOUTA3, DOUTA2, DOUTA1, DOUTA0,
    DOUTB8, DOUTB7, DOUTB6, DOUTB5, DOUTB4, DOUTB3, DOUTB2, DOUTB1, DOUTB0 : out std_logic);
  end component;

end proasic3;
