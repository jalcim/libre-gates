-- File : LibreGates/proasic3/PA3_definitions.vhdl
-- DO NOT MODIFY ! Generated file !
-- (C) Yann Guidon 20190808 - 20200902
--  * This version has delay set to 1 ns

-- Generic gates :

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity generic_gate1 is
  generic( Gate_Number : integer := Gate_Census(1);
           exclude: std_logic_vector := "");
  port( A : in std_logic;
        LUT : in std_logic_vector;
        Y : out std_logic);
end generic_gate1;

architecture trace of generic_gate1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT(LUT, LUT2'instance_name, Gate_Number, 1, exclude, Gate_boolean);
  Y <= LookItUp1(A, LUT2, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity generic_gate2 is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
        LUT : in std_logic_vector;
        Y : out std_logic);
end generic_gate2;

architecture trace of generic_gate2 is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT(LUT, LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity generic_gate3 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
        LUT : in std_logic_vector;
        Y : out std_logic);
end generic_gate3;

architecture trace of generic_gate3 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT(LUT, LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity generic_gate4 is
  generic( Gate_Number : integer := Gate_Census(4);
           exclude: std_logic_vector := "");
  port( A, B, C, D : in std_logic;
        LUT : in std_logic_vector;
        Y : out std_logic);
end generic_gate4;

architecture trace of generic_gate4 is
  signal LUT16 : LookupType16;
begin
  LUT16 <= RegisterLUT(LUT, LUT16'instance_name, Gate_Number, 15, exclude, Gate_boolean);
  Y <= LookItUp4(A, B, C, D, LUT16, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

-- 0-input gates :

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity GND is
  generic( Gate_Number : integer := Gate_Census(0);
           exclude: std_logic_vector := "");
  port(Y : out std_logic);
end GND;

architecture trace of GND is
  signal LUT1 : LookupType1;
begin
  LUT1 <= RegisterLUT("00", LUT1'instance_name, Gate_Number, 0, exclude, Gate_boolean);
  Y <= LUT1(0);
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity VCC is
  generic( Gate_Number : integer := Gate_Census(0);
           exclude: std_logic_vector := "");
  port(Y : out std_logic);
end VCC;

architecture trace of VCC is
  signal LUT1 : LookupType1;
begin
  LUT1 <= RegisterLUT("11", LUT1'instance_name, Gate_Number, 0, exclude, Gate_boolean);
  Y <= LUT1(0);
end trace;

--------------------------------------------------

-- 1-input gates :

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity BUFD is
  generic( Gate_Number : integer := Gate_Census(1);
           exclude: std_logic_vector := "");
  port( A : in std_logic;
        Y : out std_logic);
end BUFD;

architecture trace of BUFD is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_boolean);
  Y <= LookItUp1(A, LUT2, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity BUFF is
  generic( Gate_Number : integer := Gate_Census(1);
           exclude: std_logic_vector := "");
  port( A : in std_logic;
        Y : out std_logic);
end BUFF;

architecture trace of BUFF is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_boolean);
  Y <= LookItUp1(A, LUT2, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity CLKINT is
  generic( Gate_Number : integer := Gate_Census(1);
           exclude: std_logic_vector := "");
  port( A : in std_logic;
        Y : out std_logic);
end CLKINT;

architecture trace of CLKINT is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_boolean);
  Y <= LookItUp1(A, LUT2, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity INV is
  generic( Gate_Number : integer := Gate_Census(1);
           exclude: std_logic_vector := "");
  port( A : in std_logic;
        Y : out std_logic);
end INV;

architecture trace of INV is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_boolean);
  Y <= LookItUp1(A, LUT2, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity INVD is
  generic( Gate_Number : integer := Gate_Census(1);
           exclude: std_logic_vector := "");
  port( A : in std_logic;
        Y : out std_logic);
end INVD;

architecture trace of INVD is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_boolean);
  Y <= LookItUp1(A, LUT2, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

-- 2-inputs gates :

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity and2 is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end and2;

architecture trace of and2 is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT("0001LLLH", LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity and2a is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end and2a;

architecture trace of and2a is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT("0100LHLL", LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity and2b is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end and2b;

architecture trace of and2b is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT("1000HLLL", LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity nand2 is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end nand2;

architecture trace of nand2 is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT("1110HHHL", LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity nand2a is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end nand2a;

architecture trace of nand2a is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT("1011HLHH", LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity nand2b is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end nand2b;

architecture trace of nand2b is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT("0111LHHH", LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity nor2 is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end nor2;

architecture trace of nor2 is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT("1000HLLL", LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity nor2a is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end nor2a;

architecture trace of nor2a is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT("0010LLHL", LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity nor2b is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end nor2b;

architecture trace of nor2b is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT("0001LLLH", LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity or2 is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end or2;

architecture trace of or2 is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT("0111LHHH", LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity or2a is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end or2a;

architecture trace of or2a is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT("1101HHLH", LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity or2b is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end or2b;

architecture trace of or2b is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT("1110HHHL", LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity xnor2 is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end xnor2;

architecture trace of xnor2 is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT("1001HLLH", LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity xor2 is
  generic( Gate_Number : integer := Gate_Census(2);
           exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end xor2;

architecture trace of xor2 is
  signal LUT4 : LookupType4;
begin
  LUT4 <= RegisterLUT("0110LHHL", LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);
  Y <= LookItUp2(A, B, LUT4, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

-- 3-inputs gates :

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity and3 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end and3;

architecture trace of and3 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00000001LLLLLLLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity and3a is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end and3a;

architecture trace of and3a is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00010000LLLHLLLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity and3b is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end and3b;

architecture trace of and3b is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01000000LHLLLLLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity and3c is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end and3c;

architecture trace of and3c is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10000000HLLLLLLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ao1 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao1;

architecture trace of ao1 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01010111LHLHLHHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ao12 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao12;

architecture trace of ao12 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10110110HLHHLHHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ao13 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao13;

architecture trace of ao13 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00101011LLHLHLHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ao14 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao14;

architecture trace of ao14 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01101011LHHLHLHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ao15 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao15;

architecture trace of ao15 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10010100HLLHLHLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ao16 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao16;

architecture trace of ao16 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01000010LHLLLLHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ao17 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao17;

architecture trace of ao17 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01100001LHHLLLLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ao18 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao18;

architecture trace of ao18 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10110010HLHHLLHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ao1a is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao1a;

architecture trace of ao1a is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01110101LHHHLHLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ao1b is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao1b;

architecture trace of ao1b is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10101011HLHLHLHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ao1c is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao1c;

architecture trace of ao1c is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10111010HLHHHLHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ao1d is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao1d;

architecture trace of ao1d is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11010101HHLHLHLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ao1e is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao1e;

architecture trace of ao1e is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11101010HHHLHLHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity aoi1 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end aoi1;

architecture trace of aoi1 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10101000HLHLHLLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity aoi1a is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end aoi1a;

architecture trace of aoi1a is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10001010HLLLHLHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity aoi1b is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end aoi1b;

architecture trace of aoi1b is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01010100LHLHLHLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity aoi1c is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end aoi1c;

architecture trace of aoi1c is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00101010LLHLHLHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity aoi1d is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end aoi1d;

architecture trace of aoi1d is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00010101LLLHLHLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity aoi5 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end aoi5;

architecture trace of aoi5 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11100111HHHLLHHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ax1 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ax1;

architecture trace of ax1 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01100101LHHLLHLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ax1a is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ax1a;

architecture trace of ax1a is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10011010HLLHHLHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ax1b is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ax1b;

architecture trace of ax1b is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10010101HLLHLHLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ax1c is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ax1c;

architecture trace of ax1c is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01010110LHLHLHHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ax1d is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ax1d;

architecture trace of ax1d is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01101010LHHLHLHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity ax1e is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ax1e;

architecture trace of ax1e is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10101001HLHLHLLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity axo1 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axo1;

architecture trace of axo1 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01100111LHHLLHHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity axo2 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axo2;

architecture trace of axo2 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01110110LHHHLHHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity axo3 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axo3;

architecture trace of axo3 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01101110LHHLHHHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity axo5 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axo5;

architecture trace of axo5 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10111001HLHHHLLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity axo6 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axo6;

architecture trace of axo6 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10011101HLLHHHLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity axo7 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axo7;

architecture trace of axo7 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11100110HHHLLHHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity axoi1 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axoi1;

architecture trace of axoi1 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10011000HLLHHLLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity axoi2 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axoi2;

architecture trace of axoi2 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10001001HLLLHLLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity axoi3 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axoi3;

architecture trace of axoi3 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10010001HLLHLLLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity axoi4 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axoi4;

architecture trace of axoi4 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01100100LHHLLHLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity axoi5 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axoi5;

architecture trace of axoi5 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01000110LHLLLHHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity axoi7 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axoi7;

architecture trace of axoi7 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00011001LLLHHLLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity maj3 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end maj3;

architecture trace of maj3 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00010111LLLHLHHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity maj3x is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end maj3x;

architecture trace of maj3x is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00010110LLLHLHHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity maj3xi is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end maj3xi;

architecture trace of maj3xi is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11101001HHHLHLLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity min3 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end min3;

architecture trace of min3 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11101000HHHLHLLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity min3x is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end min3x;

architecture trace of min3x is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01101000LHHLHLLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity min3xi is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end min3xi;

architecture trace of min3xi is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10010111HLLHLHHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity mx2 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, S : in std_logic;
              Y : out std_logic);
end mx2;

architecture trace of mx2 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00011011LLLHHLHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, S, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity mx2a is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, S : in std_logic;
              Y : out std_logic);
end mx2a;

architecture trace of mx2a is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10110001HLHHLLLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, S, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity mx2b is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, S : in std_logic;
              Y : out std_logic);
end mx2b;

architecture trace of mx2b is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01001110LHLLHHHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, S, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity mx2c is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, S : in std_logic;
              Y : out std_logic);
end mx2c;

architecture trace of mx2c is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11100100HHHLLHLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, S, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity nand3 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nand3;

architecture trace of nand3 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11111110HHHHHHHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity nand3a is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nand3a;

architecture trace of nand3a is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11101111HHHLHHHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity nand3b is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nand3b;

architecture trace of nand3b is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10111111HLHHHHHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity nand3c is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nand3c;

architecture trace of nand3c is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01111111LHHHHHHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity nor3 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nor3;

architecture trace of nor3 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10000000HLLLLLLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity nor3a is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nor3a;

architecture trace of nor3a is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00001000LLLLHLLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity nor3b is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nor3b;

architecture trace of nor3b is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00000010LLLLLLHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity nor3c is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nor3c;

architecture trace of nor3c is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00000001LLLLLLLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity oa1 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end oa1;

architecture trace of oa1 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00010101LLLHLHLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity oa1a is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end oa1a;

architecture trace of oa1a is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01010001LHLHLLLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity oa1b is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end oa1b;

architecture trace of oa1b is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00101010LLHLHLHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity oa1c is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end oa1c;

architecture trace of oa1c is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10100010HLHLLLHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity oai1 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end oai1;

architecture trace of oai1 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11101010HHHLHLHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity or3 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end or3;

architecture trace of or3 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01111111LHHHHHHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity or3a is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end or3a;

architecture trace of or3a is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11110111HHHHLHHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity or3b is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end or3b;

architecture trace of or3b is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11111101HHHHHHLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity or3c is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end or3c;

architecture trace of or3c is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11111110HHHHHHHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity xa1 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xa1;

architecture trace of xa1 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00010100LLLHLHLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity xa1a is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xa1a;

architecture trace of xa1a is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01000001LHLLLLLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity xa1b is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xa1b;

architecture trace of xa1b is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("00101000LLHLHLLL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity xa1c is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xa1c;

architecture trace of xa1c is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10000010HLLLLLHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity xai1 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xai1;

architecture trace of xai1 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11101011HHHLHLHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity xai1a is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xai1a;

architecture trace of xai1a is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10111110HLHHHHHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity xo1 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xo1;

architecture trace of xo1 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01111101LHHHHHLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity xo1a is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xo1a;

architecture trace of xo1a is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("11010111HHLHLHHH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity xor3 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xor3;

architecture trace of xor3 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01101001LHHLHLLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity xnor3 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xnor3;

architecture trace of xnor3 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10010110HLLHLHHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity zor3 is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end zor3;

architecture trace of zor3 is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("10000001HLLLLLLH", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity zor3i is
  generic( Gate_Number : integer := Gate_Census(3);
           exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end zor3i;

architecture trace of zor3i is
  signal LUT8 : LookupType8;
begin
  LUT8 <= RegisterLUT("01111110LHHHHHHL", LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);
  Y <= LookItUp3(A, B, C, LUT8, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

-- 4-inputs gates :

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity aoi211 is
  generic( Gate_Number : integer := Gate_Census(4);
           exclude: std_logic_vector := "");
  port( A, B, C, D : in std_logic;
                 Y : out std_logic);
end aoi211;

architecture trace of aoi211 is
  signal LUT16 : LookupType16;
begin
  LUT16 <= RegisterLUT("1000100010000000HLLLHLLLHLLLLLLL", LUT16'instance_name, Gate_Number, 15, exclude, Gate_boolean);
  Y <= LookItUp4(A, B, C, D, LUT16, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity aoi22 is
  generic( Gate_Number : integer := Gate_Census(4);
           exclude: std_logic_vector := "");
  port( A, B, C, D : in std_logic;
                 Y : out std_logic);
end aoi22;

architecture trace of aoi22 is
  signal LUT16 : LookupType16;
begin
  LUT16 <= RegisterLUT("1110111011100000HHHLHHHLHHHLLLLL", LUT16'instance_name, Gate_Number, 15, exclude, Gate_boolean);
  Y <= LookItUp4(A, B, C, D, LUT16, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity oai211 is
  generic( Gate_Number : integer := Gate_Census(4);
           exclude: std_logic_vector := "");
  port( A, B, C, D : in std_logic;
                 Y : out std_logic);
end oai211;

architecture trace of oai211 is
  signal LUT16 : LookupType16;
begin
  LUT16 <= RegisterLUT("1111111011101110HHHHHHHLHHHLHHHL", LUT16'instance_name, Gate_Number, 15, exclude, Gate_boolean);
  Y <= LookItUp4(A, B, C, D, LUT16, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity oai22 is
  generic( Gate_Number : integer := Gate_Census(4);
           exclude: std_logic_vector := "");
  port( A, B, C, D : in std_logic;
                 Y : out std_logic);
end oai22;

architecture trace of oai22 is
  signal LUT16 : LookupType16;
begin
  LUT16 <= RegisterLUT("1111100010001000HHHHHLLLHLLLHLLL", LUT16'instance_name, Gate_Number, 15, exclude, Gate_boolean);
  Y <= LookItUp4(A, B, C, D, LUT16, Gate_Number) after 1 ns;
end trace;

--------------------------------------------------

-- Sequential gates :

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0 is
  generic( Gate_Number : integer := DFF_Census(2);
           exclude: std_logic_vector := "");
  port(D,    CLK           : in std_logic;
        Q : out std_logic);
end DFN0;

architecture trace of DFN0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK)
  begin
    if falling_edge(CLK) then
       Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1 is
  generic( Gate_Number : integer := DFF_Census(2);
           exclude: std_logic_vector := "");
  port(D,    CLK           : in std_logic;
        Q : out std_logic);
end DFN1;

architecture trace of DFN1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK)
  begin
    if rising_edge(CLK) then
       Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0 is
  generic( Gate_Number : integer := DFF_Census(2);
           exclude: std_logic_vector := "");
  port(D,    CLK           : in std_logic;
       QN : out std_logic);
end DFI0;

architecture trace of DFI0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK)
  begin
    if falling_edge(CLK) then
      QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1 is
  generic( Gate_Number : integer := DFF_Census(2);
           exclude: std_logic_vector := "");
  port(D,    CLK           : in std_logic;
       QN : out std_logic);
end DFI1;

architecture trace of DFI1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK)
  begin
    if rising_edge(CLK) then
      QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0C0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN0C0;

architecture trace of DFN0C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, CLR)
  begin
    if CLR='0' then
       Q <= '0' after 1 ns;
    else
      if falling_edge(CLK) then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1C0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN1C0;

architecture trace of DFN1C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, CLR)
  begin
    if CLR='0' then
       Q <= '0' after 1 ns;
    else
      if rising_edge(CLK) then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0C0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI0C0;

architecture trace of DFI0C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, CLR)
  begin
    if CLR='0' then
      QN <= '0' after 1 ns;
    else
      if falling_edge(CLK) then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1C0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI1C0;

architecture trace of DFI1C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, CLR)
  begin
    if CLR='0' then
      QN <= '0' after 1 ns;
    else
      if rising_edge(CLK) then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0C1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN0C1;

architecture trace of DFN0C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, CLR)
  begin
    if CLR='1' then
       Q <= '0' after 1 ns;
    else
      if falling_edge(CLK) then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1C1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN1C1;

architecture trace of DFN1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, CLR)
  begin
    if CLR='1' then
       Q <= '0' after 1 ns;
    else
      if rising_edge(CLK) then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0C1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI0C1;

architecture trace of DFI0C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, CLR)
  begin
    if CLR='1' then
      QN <= '0' after 1 ns;
    else
      if falling_edge(CLK) then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1C1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI1C1;

architecture trace of DFI1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, CLR)
  begin
    if CLR='1' then
      QN <= '0' after 1 ns;
    else
      if rising_edge(CLK) then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0E0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
        Q : out std_logic);
end DFN0E0;

architecture trace of DFN0E0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E)
  begin
    if falling_edge(CLK) and E='0' then
       Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1E0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
        Q : out std_logic);
end DFN1E0;

architecture trace of DFN1E0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E)
  begin
    if rising_edge(CLK) and E='0' then
       Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0E0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
       QN : out std_logic);
end DFI0E0;

architecture trace of DFI0E0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E)
  begin
    if falling_edge(CLK) and E='0' then
      QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1E0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
       QN : out std_logic);
end DFI1E0;

architecture trace of DFI1E0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E)
  begin
    if rising_edge(CLK) and E='0' then
      QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0E0C0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN0E0C0;

architecture trace of DFN0E0C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='0' then
       Q <= '0' after 1 ns;
    else
      if falling_edge(CLK) and E='0' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1E0C0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN1E0C0;

architecture trace of DFN1E0C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='0' then
       Q <= '0' after 1 ns;
    else
      if rising_edge(CLK) and E='0' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0E0C0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI0E0C0;

architecture trace of DFI0E0C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='0' then
      QN <= '0' after 1 ns;
    else
      if falling_edge(CLK) and E='0' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1E0C0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI1E0C0;

architecture trace of DFI1E0C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='0' then
      QN <= '0' after 1 ns;
    else
      if rising_edge(CLK) and E='0' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0E0C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN0E0C1;

architecture trace of DFN0E0C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='1' then
       Q <= '0' after 1 ns;
    else
      if falling_edge(CLK) and E='0' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1E0C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN1E0C1;

architecture trace of DFN1E0C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='1' then
       Q <= '0' after 1 ns;
    else
      if rising_edge(CLK) and E='0' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0E0C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI0E0C1;

architecture trace of DFI0E0C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='1' then
      QN <= '0' after 1 ns;
    else
      if falling_edge(CLK) and E='0' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1E0C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI1E0C1;

architecture trace of DFI1E0C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='1' then
      QN <= '0' after 1 ns;
    else
      if rising_edge(CLK) and E='0' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0E0P0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN0E0P0;

architecture trace of DFN0E0P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='0' then
       Q <= '1' after 1 ns;
    else
      if falling_edge(CLK) and E='0' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1E0P0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN1E0P0;

architecture trace of DFN1E0P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='0' then
       Q <= '1' after 1 ns;
    else
      if rising_edge(CLK) and E='0' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0E0P0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI0E0P0;

architecture trace of DFI0E0P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='0' then
      QN <= '1' after 1 ns;
    else
      if falling_edge(CLK) and E='0' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1E0P0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI1E0P0;

architecture trace of DFI1E0P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='0' then
      QN <= '1' after 1 ns;
    else
      if rising_edge(CLK) and E='0' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0E0P1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN0E0P1;

architecture trace of DFN0E0P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='1' then
       Q <= '1' after 1 ns;
    else
      if falling_edge(CLK) and E='0' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1E0P1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN1E0P1;

architecture trace of DFN1E0P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='1' then
       Q <= '1' after 1 ns;
    else
      if rising_edge(CLK) and E='0' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0E0P1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI0E0P1;

architecture trace of DFI0E0P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='1' then
      QN <= '1' after 1 ns;
    else
      if falling_edge(CLK) and E='0' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1E0P1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI1E0P1;

architecture trace of DFI1E0P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='1' then
      QN <= '1' after 1 ns;
    else
      if rising_edge(CLK) and E='0' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0E1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
        Q : out std_logic);
end DFN0E1;

architecture trace of DFN0E1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E)
  begin
    if falling_edge(CLK) and E='1' then
       Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1E1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
        Q : out std_logic);
end DFN1E1;

architecture trace of DFN1E1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E)
  begin
    if rising_edge(CLK) and E='1' then
       Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0E1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
       QN : out std_logic);
end DFI0E1;

architecture trace of DFI0E1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E)
  begin
    if falling_edge(CLK) and E='1' then
      QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1E1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
       QN : out std_logic);
end DFI1E1;

architecture trace of DFI1E1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E)
  begin
    if rising_edge(CLK) and E='1' then
      QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0E1C0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN0E1C0;

architecture trace of DFN0E1C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='0' then
       Q <= '0' after 1 ns;
    else
      if falling_edge(CLK) and E='1' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1E1C0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN1E1C0;

architecture trace of DFN1E1C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='0' then
       Q <= '0' after 1 ns;
    else
      if rising_edge(CLK) and E='1' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0E1C0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI0E1C0;

architecture trace of DFI0E1C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='0' then
      QN <= '0' after 1 ns;
    else
      if falling_edge(CLK) and E='1' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1E1C0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI1E1C0;

architecture trace of DFI1E1C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='0' then
      QN <= '0' after 1 ns;
    else
      if rising_edge(CLK) and E='1' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0E1C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN0E1C1;

architecture trace of DFN0E1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='1' then
       Q <= '0' after 1 ns;
    else
      if falling_edge(CLK) and E='1' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1E1C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN1E1C1;

architecture trace of DFN1E1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='1' then
       Q <= '0' after 1 ns;
    else
      if rising_edge(CLK) and E='1' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0E1C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI0E1C1;

architecture trace of DFI0E1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='1' then
      QN <= '0' after 1 ns;
    else
      if falling_edge(CLK) and E='1' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1E1C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI1E1C1;

architecture trace of DFI1E1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, CLR)
  begin
    if CLR='1' then
      QN <= '0' after 1 ns;
    else
      if rising_edge(CLK) and E='1' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0E1P0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN0E1P0;

architecture trace of DFN0E1P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='0' then
       Q <= '1' after 1 ns;
    else
      if falling_edge(CLK) and E='1' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1E1P0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN1E1P0;

architecture trace of DFN1E1P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='0' then
       Q <= '1' after 1 ns;
    else
      if rising_edge(CLK) and E='1' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0E1P0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI0E1P0;

architecture trace of DFI0E1P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='0' then
      QN <= '1' after 1 ns;
    else
      if falling_edge(CLK) and E='1' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1E1P0 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI1E1P0;

architecture trace of DFI1E1P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='0' then
      QN <= '1' after 1 ns;
    else
      if rising_edge(CLK) and E='1' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0E1P1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN0E1P1;

architecture trace of DFN0E1P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='1' then
       Q <= '1' after 1 ns;
    else
      if falling_edge(CLK) and E='1' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1E1P1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN1E1P1;

architecture trace of DFN1E1P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='1' then
       Q <= '1' after 1 ns;
    else
      if rising_edge(CLK) and E='1' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0E1P1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI0E1P1;

architecture trace of DFI0E1P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='1' then
      QN <= '1' after 1 ns;
    else
      if falling_edge(CLK) and E='1' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1E1P1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI1E1P1;

architecture trace of DFI1E1P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, E, PRE)
  begin
    if PRE='1' then
      QN <= '1' after 1 ns;
    else
      if rising_edge(CLK) and E='1' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0P0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN0P0;

architecture trace of DFN0P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, PRE)
  begin
    if PRE='0' then
       Q <= '1' after 1 ns;
    else
      if falling_edge(CLK) then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1P0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN1P0;

architecture trace of DFN1P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, PRE)
  begin
    if PRE='0' then
       Q <= '1' after 1 ns;
    else
      if rising_edge(CLK) then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0P0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI0P0;

architecture trace of DFI0P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, PRE)
  begin
    if PRE='0' then
      QN <= '1' after 1 ns;
    else
      if falling_edge(CLK) then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1P0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI1P0;

architecture trace of DFI1P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, PRE)
  begin
    if PRE='0' then
      QN <= '1' after 1 ns;
    else
      if rising_edge(CLK) then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0P1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN0P1;

architecture trace of DFN0P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, PRE)
  begin
    if PRE='1' then
       Q <= '1' after 1 ns;
    else
      if falling_edge(CLK) then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1P1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN1P1;

architecture trace of DFN1P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, PRE)
  begin
    if PRE='1' then
       Q <= '1' after 1 ns;
    else
      if rising_edge(CLK) then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0P1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI0P1;

architecture trace of DFI0P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, PRE)
  begin
    if PRE='1' then
      QN <= '1' after 1 ns;
    else
      if falling_edge(CLK) then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1P1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI1P1;

architecture trace of DFI1P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, PRE)
  begin
    if PRE='1' then
      QN <= '1' after 1 ns;
    else
      if rising_edge(CLK) then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN0P1C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D,    CLK, PRE, CLR : in std_logic;
        Q : out std_logic);
end DFN0P1C1;

architecture trace of DFN0P1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, PRE, CLR)
  begin
    if CLR='1' then
       Q <= '0' after 1 ns;
    else
      if PRE='1' then
         Q <= '1' after 1 ns;
      else
        if falling_edge(CLK) then
           Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
        end if;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFN1P1C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D,    CLK, PRE, CLR : in std_logic;
        Q : out std_logic);
end DFN1P1C1;

architecture trace of DFN1P1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, PRE, CLR)
  begin
    if CLR='1' then
       Q <= '0' after 1 ns;
    else
      if PRE='1' then
         Q <= '1' after 1 ns;
      else
        if rising_edge(CLK) then
           Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
        end if;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI0P1C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D,    CLK, PRE, CLR : in std_logic;
       QN : out std_logic);
end DFI0P1C1;

architecture trace of DFI0P1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, PRE, CLR)
  begin
    if CLR='1' then
      QN <= '0' after 1 ns;
    else
      if PRE='1' then
        QN <= '1' after 1 ns;
      else
        if falling_edge(CLK) then
          QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
        end if;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DFI1P1C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D,    CLK, PRE, CLR : in std_logic;
       QN : out std_logic);
end DFI1P1C1;

architecture trace of DFI1P1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(CLK, PRE, CLR)
  begin
    if CLR='1' then
      QN <= '0' after 1 ns;
    else
      if PRE='1' then
        QN <= '1' after 1 ns;
      else
        if rising_edge(CLK) then
          QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
        end if;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLI0 is
  generic( Gate_Number : integer := DFF_Census(2);
           exclude: std_logic_vector := "");
  port(D,    G             : in std_logic;
       QN : out std_logic);
end DLI0;

architecture trace of DLI0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D)
  begin
    if G='0' then
      QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLI1 is
  generic( Gate_Number : integer := DFF_Census(2);
           exclude: std_logic_vector := "");
  port(D,    G             : in std_logic;
       QN : out std_logic);
end DLI1;

architecture trace of DLI1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D)
  begin
    if G='1' then
      QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLN0 is
  generic( Gate_Number : integer := DFF_Census(2);
           exclude: std_logic_vector := "");
  port(D,    G             : in std_logic;
        Q : out std_logic);
end DLN0;

architecture trace of DLN0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D)
  begin
    if G='0' then
       Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLN1 is
  generic( Gate_Number : integer := DFF_Census(2);
           exclude: std_logic_vector := "");
  port(D,    G             : in std_logic;
        Q : out std_logic);
end DLN1;

architecture trace of DLN1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D)
  begin
    if G='1' then
       Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLI0C0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
       QN : out std_logic);
end DLI0C0;

architecture trace of DLI0C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, CLR)
  begin
    if CLR='0' then
      QN <= '0' after 1 ns;
    else
      if G='0' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLI1C0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
       QN : out std_logic);
end DLI1C0;

architecture trace of DLI1C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, CLR)
  begin
    if CLR='0' then
      QN <= '0' after 1 ns;
    else
      if G='1' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLN0C0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
        Q : out std_logic);
end DLN0C0;

architecture trace of DLN0C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, CLR)
  begin
    if CLR='0' then
       Q <= '0' after 1 ns;
    else
      if G='0' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLN1C0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
        Q : out std_logic);
end DLN1C0;

architecture trace of DLN1C0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, CLR)
  begin
    if CLR='0' then
       Q <= '0' after 1 ns;
    else
      if G='1' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLI0C1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
       QN : out std_logic);
end DLI0C1;

architecture trace of DLI0C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, CLR)
  begin
    if CLR='1' then
      QN <= '0' after 1 ns;
    else
      if G='0' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLI1C1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
       QN : out std_logic);
end DLI1C1;

architecture trace of DLI1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, CLR)
  begin
    if CLR='1' then
      QN <= '0' after 1 ns;
    else
      if G='1' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLN0C1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
        Q : out std_logic);
end DLN0C1;

architecture trace of DLN0C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, CLR)
  begin
    if CLR='1' then
       Q <= '0' after 1 ns;
    else
      if G='0' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLN1C1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
        Q : out std_logic);
end DLN1C1;

architecture trace of DLN1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, CLR)
  begin
    if CLR='1' then
       Q <= '0' after 1 ns;
    else
      if G='1' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLI0P0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
       QN : out std_logic);
end DLI0P0;

architecture trace of DLI0P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, PRE)
  begin
    if PRE='0' then
      QN <= '1' after 1 ns;
    else
      if G='0' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLI1P0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
       QN : out std_logic);
end DLI1P0;

architecture trace of DLI1P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, PRE)
  begin
    if PRE='0' then
      QN <= '1' after 1 ns;
    else
      if G='1' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLN0P0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
        Q : out std_logic);
end DLN0P0;

architecture trace of DLN0P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, PRE)
  begin
    if PRE='0' then
       Q <= '1' after 1 ns;
    else
      if G='0' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLN1P0 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
        Q : out std_logic);
end DLN1P0;

architecture trace of DLN1P0 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, PRE)
  begin
    if PRE='0' then
       Q <= '1' after 1 ns;
    else
      if G='1' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLI0P1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
       QN : out std_logic);
end DLI0P1;

architecture trace of DLI0P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, PRE)
  begin
    if PRE='1' then
      QN <= '1' after 1 ns;
    else
      if G='0' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLI1P1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
       QN : out std_logic);
end DLI1P1;

architecture trace of DLI1P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, PRE)
  begin
    if PRE='1' then
      QN <= '1' after 1 ns;
    else
      if G='1' then
        QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLN0P1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
        Q : out std_logic);
end DLN0P1;

architecture trace of DLN0P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, PRE)
  begin
    if PRE='1' then
       Q <= '1' after 1 ns;
    else
      if G='0' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLN1P1 is
  generic( Gate_Number : integer := DFF_Census(3);
           exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
        Q : out std_logic);
end DLN1P1;

architecture trace of DLN1P1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, PRE)
  begin
    if PRE='1' then
       Q <= '1' after 1 ns;
    else
      if G='1' then
         Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLI0P1C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D,    G  , PRE, CLR : in std_logic;
       QN : out std_logic);
end DLI0P1C1;

architecture trace of DLI0P1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, PRE, CLR)
  begin
    if CLR='1' then
      QN <= '0' after 1 ns;
    else
      if PRE='1' then
        QN <= '1' after 1 ns;
      else
        if G='0' then
          QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
        end if;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLI1P1C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D,    G  , PRE, CLR : in std_logic;
       QN : out std_logic);
end DLI1P1C1;

architecture trace of DLI1P1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("10LH", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, PRE, CLR)
  begin
    if CLR='1' then
      QN <= '0' after 1 ns;
    else
      if PRE='1' then
        QN <= '1' after 1 ns;
      else
        if G='1' then
          QN <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
        end if;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLN0P1C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D,    G  , PRE, CLR : in std_logic;
        Q : out std_logic);
end DLN0P1C1;

architecture trace of DLN0P1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, PRE, CLR)
  begin
    if CLR='1' then
       Q <= '0' after 1 ns;
    else
      if PRE='1' then
         Q <= '1' after 1 ns;
      else
        if G='0' then
           Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
        end if;
      end if;
    end if;
  end process;
end trace;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity DLN1P1C1 is
  generic( Gate_Number : integer := DFF_Census(4);
           exclude: std_logic_vector := "");
  port(D,    G  , PRE, CLR : in std_logic;
        Q : out std_logic);
end DLN1P1C1;

architecture trace of DLN1P1C1 is
  signal LUT2 : LookupType2;
begin
  LUT2 <= RegisterLUT("01HL", LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);
  process(G  , D, PRE, CLR)
  begin
    if CLR='1' then
       Q <= '0' after 1 ns;
    else
      if PRE='1' then
         Q <= '1' after 1 ns;
      else
        if G='1' then
           Q <= LookItUp1(D , LUT2, Gate_Number) after 1 ns;
        end if;
      end if;
    end if;
  end process;
end trace;
