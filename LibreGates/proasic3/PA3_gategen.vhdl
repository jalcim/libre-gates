-- LibreGates/proasic3/PA3_gategen.vhdl
-- created jeu. août  8 10:18:01 CEST 2019  by Yann Guidon (whygee@f-cpu.org)
-- version sam. août 10 08:06:58 CEST 2019
-- version dim. août 11 17:36:51 CEST 2019
-- version lun. août 12 18:36:04 CEST 2019
-- version mar. août 13 18:05:15 CEST 2019
-- version dim. août 25 10:25:30 CEST 2019 : DLxxxx added
-- version jeu. nov. 21 03:38:34 CET  2019 : removing "trace"vs"fast"
-- version ven. nov. 22 23:39:03 CET  2019 : generic gates
-- version ven. déc. 13 17:31:50 CET  2019 : LUT16
-- version lun. mars 23 04:52:26 CET  2020 : v2.8 adds -ggate_type="simple"
-- version mar. juil. 14 23:15:00 CEST 2020 : v2.9 adds some 4-inputs gates
-- version sam. juil. 18 06:11:26 CEST 2020 : finally spotted a ridiculous
--     and shamefully old mistake in the DFN code. TEST TEST TEST !!!
-- version lun. juil. 27 07:45:52 CEST 2020 : fixed a nasty sensitivity list omission on latches.
-- version lun. août 31 22:17:01 CEST 2020 : renamed display_histogram => display_gates_histograms
--   moved most PA3-specific definitions here and renamed this file from gategen.vhdl to PA3_gategen.vhdl
-- version mer. sept. 2 20:16:42 CEST 2020 : LibreGates !
-- version mer. nov. 11 14:20:25 CET 2020 : renamed some 
--   variable names to remove "declaration hides variable" warnings
-- version mer. déc.  9 23:26:45 CET 2020 : DFF_Census instead of Gate_Census for sequential gates
--
-- Released under the GNU AGPLv3 license
--
-- This file generates PA3_components.vhdl and PA3_definitions.vhdl
--  with various flavours.
--
-- TODO : reduce countless redundancies...

Library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
use STD.textio.all;
Library SLV;
    use	SLV.SLV_utils.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity PA3_GateGen is
  generic ( gate_delay : string := "";
            gate_type  : string := "trace"); -- default architecture name.
end PA3_GateGen;

architecture runme of PA3_GateGen is
  file gate_defs : text open write_mode is "PA3_definitions.vhdl";
  file gate_list : text open write_mode is "PA3_components.vhdl";

-- List and definitions of the A3P gates
-- (could be moved back to a library but that can wait)

  type string3x2 is array(0 to 1) of string(1 to 3);
  constant NoInputGates : string3x2 := ( "GND", "VCC" );

  type string6x6 is array(0 to 4) of string(1 to 6);
  constant BufferGates : string6x6 := (
    "BUFD  ", "BUFF  ", "CLKINT", "INV   ", "INVD  ");

  type EnumGates2 is (
    AND2,  AND2A,  AND2B,
    NAND2, NAND2A, NAND2B,
    NOR2,  NOR2A,  NOR2B,
    OR2,   OR2A,   OR2B,
    XNOR2, XOR2 );

  type DefGate2Type is array(EnumGates2) of string(1 to 24);
  constant DefGate2Array : DefGate2Type := (
    AND2   => "         A  and      B  ",
    AND2A  => "    (not A) and      B  ",
    AND2B  => "    (not A) and (not B) ",
    NAND2  => "not(     A  and      B) ",
    NAND2A => "not((not A) and      B) ",
    NAND2B => "not((not A) and (not B))",
    NOR2   => "         A  nor      B  ",
    NOR2A  => "not((not A)  or      B) ",
    NOR2B  => "not((not A)  or (not B))",
    OR2    => "         A   or      B  ",
    OR2A   => "    (not A)  or      B  ",
    OR2B   => "    (not A)  or (not B) ",
    XNOR2  => "not     (A  xor      B) ",
    XOR2   => "         A  xor      B  ");


  type EnumGates3 is (
     AND3,  AND3A, AND3B,  AND3C,  AO1,    AO12,  AO13,   AO14,  AO15,  AO16,
     AO17,  AO18,  AO1A,   AO1B,   AO1C,   AO1D,  AO1E,   AOI1,  AOI1A, AOI1B,
     AOI1C, AOI1D, AOI5,   AX1,    AX1A,   AX1B,  AX1C,   AX1D,  AX1E,  AXO1,
     AXO2,  AXO3,  AXO5,   AXO6,   AXO7,   AXOI1, AXOI2,  AXOI3, AXOI4, AXOI5,
     AXOI7, MAJ3,  MAJ3X,  MAJ3XI, MIN3,   MIN3X, MIN3XI, MX2,   MX2A,  MX2B,
     MX2C,  NAND3, NAND3A, NAND3B, NAND3C, NOR3,  NOR3A,  NOR3B, NOR3C, OA1,
     OA1A,  OA1B,  OA1C,   OAI1,   OR3,    OR3A,  OR3B,   OR3C,  XA1,   XA1A,
     XA1B,  XA1C,  XAI1,   XAI1A , XO1,    XO1A,  XOR3,   XNOR3, ZOR3,  ZOR3I);

  type DefGate3Type is array(EnumGates3) of string(1 to 94);
  constant DefGate3Array : DefGate3Type := (
    AND3   => "(     A  and      B ) and      C                                                              ",
    AND3A  => "((not A) and      B ) and      C                                                              ",
    AND3B  => "((not A) and (not B)) and      C                                                              ",
    AND3C  => "((not A) and (not B)) and (not C)                                                             ",
    AO12   => "((not A) and B) or ((not A) and (not C)) or (B and (not C)) or (A and (not B) and C)          ",
    AO13   => "(A and B) or (A and (not C)) or (B and (not C))                                               ",
    AO14   => "(A and B) or (A and (not C)) or (B and (not C)) or ((not A) and (not B) and C)                ",
    AO15   => "(A and (not B) and C) or ((not A) and B and C) or ((not A) and (not B) and (not C))           ",
    AO16   => "(A and B and (not C)) or ((not A) and (not B) and C)                                          ",
    AO17   => "(A and B and C) or ((not A) and B and (not C)) or ((not A) and (not B) and C)                 ",
    AO18   => "((not A) and B) or ((not A) and (not C)) or (B and (not C))                                   ",
    AO1    => "     (     A  and      B ) or      C                                                          ",
    AO1A   => "     ((not A) and      B ) or      C                                                          ",
    AO1B   => "     (     A  and      B ) or (not C)                                                         ",
    AO1C   => "     ((not A) and      B ) or (not C)                                                         ",
    AO1D   => "     ((not A) and (not B)) or      C                                                          ",
    AO1E   => "     ((not A) and (not B)) or (not C)                                                         ",
    AOI1   => "not ((     A  and      B ) or      C )                                                        ",
    AOI1A  => "not (((not A) and      B ) or      C )                                                        ",
    AOI1B  => "not ((     A  and      B ) or (not C))                                                        ",
    AOI1C  => "not (((not A) and (not B)) or      C )                                                        ",
    AOI1D  => "not (((not A) and (not B)) or (not C))                                                        ",
    AOI5   => "not (((not A) and B and C) or (A and (not B) and (not C)) )                                   ",
    AX1    => "    ((not A) and      B ) xor C                                                               ",
    AX1A   => "not(((not A) and      B ) xor C)                                                              ",
    AX1B   => "    ((not A) and (not B)) xor C                                                               ",
    AX1C   => "    (     A  and      B ) xor C                                                               ",
    AX1D   => "not(((not A) and (not B)) xor C)                                                              ",
    AX1E   => "not((     A  and      B ) xor C)                                                              ",
    AXO1   => "    (     A  and      B ) or (     B  xor C)                                                  ",
    AXO2   => "    ((not A) and      B ) or (     B  xor C)                                                  ",
    AXO3   => "    (     A  and (not B)) or (     B  xor C)                                                  ",
    AXO5   => "    ((not A) and      B ) or ((not B) xor C)                                                  ",
    AXO6   => "    (     A  and (not B)) or ((not B) xor C)                                                  ",
    AXO7   => "    ((not A) and (not B)) or (     B  xor C)                                                  ",
    AXOI1  => "not((     A  and      B ) or (     B  xor C))                                                 ",
    AXOI2  => "not(((not A) and      B ) or (     B  xor C))                                                 ",
    AXOI3  => "not((     A  and (not B)) or (     B  xor C))                                                 ",
    AXOI4  => "not((     A  and      B ) or ((not B) xor C))                                                 ",
    AXOI5  => "not(((not A) and      B ) or ((not B) xor C))                                                 ",
    AXOI7  => "not(((not A) and (not B)) or (     B  xor C))                                                 ",
    MAJ3   => "    (A and B            ) or (A and             C) or (            B and C)                   ",
    MAJ3X  => "    (A and B and (not C)) or (A and (not B) and C) or ((not A) and B and C)                   ",
    MAJ3XI => "not((A and B and (not C)) or (A and (not B) and C) or ((not A) and B and C))                  ",
    MIN3   => "    ((not A) and (not B)      ) or ((not A) and       (not C)) or (      (not B) and (not C)) ",
    MIN3X  => "    ((not A) and (not B) and C) or ((not A) and B and (not C)) or (A and (not B) and (not C)) ",  --- not A3P ! 54SX, 54SX-A, 54SX-S, eX
    MIN3XI => "not(((not A) and (not B) and C) or ((not A) and B and (not C)) or (A and (not B) and (not C)))",
    MX2    => "     A  when S/='1' else      B                                                               ",
    MX2A   => "(not A) when S/='1' else      B                                                               ",
    MX2B   => "     A  when S/='1' else (not B)                                                              ",
    MX2C   => "(not A) when S/='1' else (not B)                                                              ",
    NAND3  => "not ((     A  and      B ) and      C )                                                       ",
    NAND3A => "not (((not A) and      B ) and      C )                                                       ",
    NAND3B => "not (((not A) and (not B)) and      C )                                                       ",
    NAND3C => "not (((not A) and (not B)) and (not C))                                                       ",
    NOR3   => "not ((    A  or      B) or      C)                                                            ",
    NOR3A  => "not ((not A) or      B  or      C )                                                           ",
    NOR3B  => "not ((not A) or (not B) or      C )                                                           ",
    NOR3C  => "not ((not A) or (not B) or (not C))                                                           ",
    OA1    => "(     A  or B) and      C                                                                     ",
    OA1A   => "((not A) or B) and      C                                                                     ",
    OA1B   => "(     A  or B) and (not C)                                                                    ",
    OA1C   => "((not A) or B) and (not C)                                                                    ",
    OAI1   => "not ((A or B) and C)                                                                          ",
    OR3    => "(A or B) or C                                                                                 ",
    OR3A   => "(not A) or      B    or      C                                                                ",
    OR3B   => "(not A) or (not B)   or      C                                                                ",
    OR3C   => "(not A) or (not B)   or (not C)                                                               ",
    XA1    => "         (A xor B)  and      C                                                                ",
    XA1A   => "    (not (A xor B)) and      C                                                                ",
    XA1B   => "         (A xor B)  and (not C)                                                               ",
    XA1C   => "    (not (A xor B)) and (not C)                                                               ",
    XAI1   => "not(     (A xor B)  and      C)                                                               ",
    XAI1A  => "not((not (A xor B)) and      C)                                                               ",
    XNOR3  => " not ((A xor B) xor C)                                                                        ",
    XO1    => "     (A xor B)  or C                                                                          ",
    XO1A   => "(not (A xor B)) or C                                                                          ",
    XOR3   => "     (A xor B) xor C                                                                          ",
    ZOR3   => "     (A and B and C) or ((not A) and (not B) and (not C))                                     ",
    ZOR3I  => "not ((A and B and C) or ((not A) and (not B) and (not C)))                                    ");

  type EnumGates4 is (
     AOI211, AOI22, 
     OAI211, OAI22);

  type DefGate4Type is array(EnumGates4) of string(1 to 29);
  constant DefGate4Array : DefGate4Type := (
    AOI211 => "not ((A and B) or   C  or D )",
    AOI22  => "not ((A and B) or  (C and D))",
    OAI211 => "not ((A or  B) and  C and D )",
    OAI22  => "not ((A or  B) and (C  or D))");

  -- ALL THE Dxxxx gates !
  type string8x88 is array(1 to 88) of string(1 to 8);
  constant EnumDFF : string8x88 := (
    "DFN0    ", "DFN1    ", "DFI0    ", "DFI1    ",
    "DFN0C0  ", "DFN1C0  ", "DFI0C0  ", "DFI1C0  ", 
    "DFN0C1  ", "DFN1C1  ", "DFI0C1  ", "DFI1C1  ",
    "DFN0E0  ", "DFN1E0  ", "DFI0E0  ", "DFI1E0  ",
    "DFN0E0C0", "DFN1E0C0", "DFI0E0C0", "DFI1E0C0",
    "DFN0E0C1", "DFN1E0C1", "DFI0E0C1", "DFI1E0C1",
    "DFN0E0P0", "DFN1E0P0", "DFI0E0P0", "DFI1E0P0",
    "DFN0E0P1", "DFN1E0P1", "DFI0E0P1", "DFI1E0P1",
    "DFN0E1  ", "DFN1E1  ", "DFI0E1  ", "DFI1E1  ",
    "DFN0E1C0", "DFN1E1C0", "DFI0E1C0", "DFI1E1C0",
    "DFN0E1C1", "DFN1E1C1", "DFI0E1C1", "DFI1E1C1",
    "DFN0E1P0", "DFN1E1P0", "DFI0E1P0", "DFI1E1P0",
    "DFN0E1P1", "DFN1E1P1", "DFI0E1P1", "DFI1E1P1",
    "DFN0P0  ", "DFN1P0  ", "DFI0P0  ", "DFI1P0  ",
    "DFN0P1  ", "DFN1P1  ", "DFI0P1  ", "DFI1P1  ",
    "DFN0P1C1", "DFN1P1C1", "DFI0P1C1", "DFI1P1C1",
    "DLI0    ", "DLI1    ", "DLN0    ", "DLN1    ",
    "DLI0C0  ", "DLI1C0  ", "DLN0C0  ", "DLN1C0  ",
    "DLI0C1  ", "DLI1C1  ", "DLN0C1  ", "DLN1C1  ",
    "DLI0P0  ", "DLI1P0  ", "DLN0P0  ", "DLN1P0  ",
    "DLI0P1  ", "DLI1P1  ", "DLN0P1  ", "DLN1P1  ",
    "DLI0P1C1", "DLI1P1C1", "DLN0P1C1", "DLN1P1C1");

  function PA3_ComputeLUT4 (t: EnumGates2) return LookupType4 is
    variable L : LookupType4;
    variable A, B, C, Y : std_logic;
    variable i : integer;
  begin
    i := 0;

    A := '0';
    loop

      B := '0';
      loop
        case t is
          when AND2   => Y :=          A  and      B  ;
          when AND2A  => Y :=     (not A) and      B  ;
          when AND2B  => Y :=     (not A) and (not B) ;
          when NAND2  => Y := not(     A  and      B) ;
          when NAND2A => Y := not((not A) and      B) ;
          when NAND2B => Y := not((not A) and (not B));
          when NOR2   => Y :=          A  nor      B  ;
          when NOR2A  => Y := not((not A)  or      B) ;
          when NOR2B  => Y := not((not A)  or (not B));
          when OR2    => Y :=          A   or      B  ;
          when OR2A   => Y :=     (not A)  or      B  ;
          when OR2B   => Y :=     (not A)  or (not B) ;
          when XNOR2  => Y := not     (A  xor      B) ;
          when XOR2   => Y :=          A  xor      B  ;
          when others => report "Function not implemented : "
                  & EnumGates2'image(t) severity failure;
        end case;

        L(i) := Y;
        if Y = '1' then
          L(i+4) := 'H';
        else
          L(i+4) := 'L';
        end if;
        i := i+1;

        exit when B = '1'; 
        B := '1';
      end loop;

      exit when A = '1';
      A := '1';
    end loop;

    return L;
  end PA3_ComputeLUT4;


  function PA3_ComputeLUT8 (t: EnumGates3) return LookupType8 is
    variable L : LookupType8;
    variable A, B, C, Y : std_logic;
    variable i : integer;
  begin
    i := 0;

    A := '0';
    loop

      B := '0';
      loop 

        C := '0';
        loop

          case t is
            when AND3   => Y := (     A  and      B ) and      C ;
            when AND3A  => Y := ((not A) and      B ) and      C ;
            when AND3B  => Y := ((not A) and (not B)) and      C ;
            when AND3C  => Y := ((not A) and (not B)) and (not C);
            when AO12   => Y := ((not A) and B) or ((not A) and (not C)) or (B and (not C)) or (A and (not B) and C);
            when AO13   => Y := (A and B) or (A and (not C)) or (B and (not C));
            when AO14   => Y := (A and B) or (A and (not C)) or (B and (not C)) or ((not A) and (not B) and C);
            when AO15   => Y := (A and (not B) and C) or ((not A) and B and C) or ((not A) and (not B) and (not C));
            when AO16   => Y := (A and B and (not C)) or ((not A) and (not B) and C);
            when AO17   => Y := (A and B and C) or ((not A) and B and (not C)) or ((not A) and (not B) and C);
            when AO18   => Y := ((not A) and B) or ((not A) and (not C)) or (B and (not C));
            when AO1    => Y :=      (     A  and      B ) or      C;
            when AO1A   => Y :=      ((not A) and      B ) or      C;
            when AO1B   => Y :=      (     A  and      B ) or (not C);
            when AO1C   => Y :=      ((not A) and      B ) or (not C);
            when AO1D   => Y :=      ((not A) and (not B)) or      C;
            when AO1E   => Y :=      ((not A) and (not B)) or (not C);
            when AOI1   => Y := not ((     A  and      B ) or      C );
            when AOI1A  => Y := not (((not A) and      B ) or      C );
            when AOI1B  => Y := not ((     A  and      B ) or (not C));
            when AOI1C  => Y := not (((not A) and (not B)) or      C );
            when AOI1D  => Y := not (((not A) and (not B)) or (not C));
            when AOI5   => Y := not (((not A) and B and C) or (A and (not B) and (not C)) );
            when AX1    => Y :=     ((not A) and      B ) xor C;
            when AX1A   => Y := not(((not A) and      B ) xor C);
            when AX1B   => Y :=     ((not A) and (not B)) xor C;
            when AX1C   => Y :=     (     A  and      B ) xor C;
            when AX1D   => Y := not(((not A) and (not B)) xor C);
            when AX1E   => Y := not((     A  and      B ) xor C);
            when AXO1   => Y :=     (     A  and      B ) or (     B  xor C);
            when AXO2   => Y :=     ((not A) and      B ) or (     B  xor C);
            when AXO3   => Y :=     (     A  and (not B)) or (     B  xor C);
            when AXO5   => Y :=     ((not A) and      B ) or ((not B) xor C);
            when AXO6   => Y :=     (     A  and (not B)) or ((not B) xor C);
            when AXO7   => Y :=     ((not A) and (not B)) or (     B  xor C);
            when AXOI1  => Y := not((     A  and      B ) or (     B  xor C));
            when AXOI2  => Y := not(((not A) and      B ) or (     B  xor C));
            when AXOI3  => Y := not((     A  and (not B)) or (     B  xor C));
            when AXOI4  => Y := not((     A  and      B ) or ((not B) xor C));
            when AXOI5  => Y := not(((not A) and      B ) or ((not B) xor C));
            when AXOI7  => Y := not(((not A) and (not B)) or (     B  xor C));
            when MAJ3   => Y :=     (A and B            ) or (A and             C) or (            B and C);
            when MAJ3X  => Y :=     (A and B and (not C)) or (A and (not B) and C) or ((not A) and B and C);
            when MAJ3XI => Y := not((A and B and (not C)) or (A and (not B) and C) or ((not A) and B and C));
            when MIN3   => Y :=     ((not A) and (not B)      ) or ((not A) and       (not C)) or (      (not B) and (not C));
            when MIN3X  => Y :=     ((not A) and (not B) and C) or ((not A) and B and (not C)) or (A and (not B) and (not C));  --- not A3P ! 54SX, 54SX-A, 54SX-S, eX
            when MIN3XI => Y := not(((not A) and (not B) and C) or ((not A) and B and (not C)) or (A and (not B) and (not C)));
            when MX2    => if C='0' then  Y :=      A ; else Y :=      B ; end if;
            when MX2A   => if C='0' then  Y := (not A); else Y :=      B ; end if;
            when MX2B   => if C='0' then  Y :=      A ; else Y := (not B); end if;
            when MX2C   => if C='0' then  Y := (not A); else Y := (not B); end if;
            when NAND3  => Y := not ((     A  and      B ) and      C );
            when NAND3A => Y := not (((not A) and      B ) and      C );
            when NAND3B => Y := not (((not A) and (not B)) and      C );
            when NAND3C => Y := not (((not A) and (not B)) and (not C));
            when NOR3   => Y := not ((    A  or      B) or      C);
            when NOR3A  => Y := not ((not A) or      B  or      C );
            when NOR3B  => Y := not ((not A) or (not B) or      C );
            when NOR3C  => Y := not ((not A) or (not B) or (not C));
            when OA1    => Y := (     A  or B) and      C ;
            when OA1A   => Y := ((not A) or B) and      C ;
            when OA1B   => Y := (     A  or B) and (not C);
            when OA1C   => Y := ((not A) or B) and (not C);
            when OAI1   => Y := not ((A or B) and C);
            when OR3    => Y := (A or B) or C;
            when OR3A   => Y := (not A) or      B  or      C ;
            when OR3B   => Y := (not A) or (not B) or      C ;
            when OR3C   => Y := (not A) or (not B) or (not C);
            when XA1    => Y :=          (A xor B)  and      C;
            when XA1A   => Y :=     (not (A xor B)) and      C;
            when XA1B   => Y :=          (A xor B)  and (not C);
            when XA1C   => Y :=     (not (A xor B)) and (not C);
            when XAI1   => Y := not(     (A xor B)  and	     C);
            when XAI1A  => Y := not((not (A xor B)) and      C);
            when XNOR3  => Y :=  not ((A xor B) xor C);
            when XO1    => Y :=      (A xor B)  or C;
            when XO1A   => Y := (not (A xor B)) or C;
            when XOR3   => Y :=      (A xor B) xor C;
            when ZOR3   => Y :=      (A and B and C) or ((not A) and (not B) and (not C));
            when ZOR3I  => Y := not ((A and B and C) or ((not A) and (not B) and (not C)));
            when others => report "Function not implemented : "
                                & EnumGates3'image(t) severity failure;
          end case;

          L(i) := Y;
          if Y = '1' then
            L(i+8) := 'H';
          else
            L(i+8) := 'L';
          end if;
          i := i+1;

          exit when C = '1';
          C := '1';
        end loop;

        exit when B = '1'; 
        B := '1';
      end loop;

      exit when A = '1';
      A := '1';
    end loop;

    return L;
  end PA3_ComputeLUT8;


  function PA3_ComputeLUT16 (t: EnumGates4) return LookupType16 is
    variable L : LookupType16;
    variable A, B, C, D, Y : std_logic;
    variable i : integer;
  begin
    i := 0;

    A := '0';
    loop

      B := '0';
      loop 

        C := '0';
        loop

          D := '0';
          loop

            case t is
              when AOI211 => Y := not ((A and B) or   C  or D );
              when AOI22  => Y := not ((A and B) or  (C and D));
              when OAI211 => Y := not ((A or  B) and  C and D );
              when OAI22  => Y := not ((A or  B) and (C  or D));
              when others => report "Function not implemented : "
                                & EnumGates4'image(t) severity failure;
            end case;

            L(i) := Y;
            if Y = '1' then
              L(i+16) := 'H';
            else
              L(i+16) := 'L';
            end if;
            i := i+1;

            exit when D = '1';
            D := '1';
          end loop;

          exit when C = '1';
          C := '1';
        end loop;

        exit when B = '1'; 
        B := '1';
      end loop;

      exit when A = '1';
      A := '1';
    end loop;

    return L;
  end PA3_ComputeLUT16;

--------------------------------------------------------------------------

begin

  bench: process
    variable c, Epol, Cpol, Ppol, op : character;
--  variable i : integer := 0;  loop counter
    variable j, k, indent, sinx : integer := 0;
    variable spaces : string(1 to 12) := (others=>' '); 
--  variable s : EnumGates2; loop counter
--  variable t : EnumGates3; loop counter
    variable L2 : LookupType2;
    variable L4 : LookupType4;
    variable L8 : LookupType8;
    variable L16: LookupType16;
    variable row : line;
    variable QN, StrE : string(1 to 2);
    variable GCLK : string(1 to 3);
    variable StrC, StrP : string(1 to 5);
    constant letters: string(1 to 4) := "ABCD";
    variable gate_kind: enum_gate_kind := Gate_boolean;

    procedure wl(ps: string) is
    begin
      write(row, ps);
      writeline(gate_list, row);
    end wl;

    procedure wd(ps: string) is
    begin
      write(row, ps);
      writeline(gate_defs, row);
    end wd;

    procedure wdi(ps: string) is
    begin
      write(row, spaces(1 to indent) & ps);
      writeline(gate_defs, row);
    end wdi;

    procedure wdg(sinks: integer) is
    begin
      write(row, string'("  generic( "));
      if gate_type /= "simple" then
        if gate_kind = Gate_boolean then
          wd("Gate_Number : integer := Gate_Census(" & 
            integer'image(sinks) & ");");
        else
          wd("Gate_Number : integer := DFF_Census(" & 
            integer'image(sinks) & ");");
        end if;
        write(row, string'("           "));
      end if;
      wd("exclude: std_logic_vector := """");");
    end wdg;

    procedure uselib is
    begin
      wd("");
      wd("--------------------------------------------------");
      wd("");
      wd("Library ieee;");
      wd("    use ieee.std_logic_1164.all;");
      if gate_type /= "simple" then
        wd("Library LibreGates;");
        wd("    use LibreGates.all;");
        wd("    use LibreGates.Gates_lib.all;");
      end if;
      wd("");
    end uselib;

    procedure shift_indent(si : integer) is
    begin
      if si >= 4 then
        indent := si;
      end if;
    end shift_indent;

    procedure recurse_if(level: integer; sp : string; index: integer) is
    begin
      if level < 1 then
        wd(sp & c & " <= LUT(" & integer'image(k) & ");");
        k := k+1;
      else
        wd(sp & "if " & letters(index) & "/='1' then");
          recurse_if(level-1, sp & "  ", index+1);
        wd(sp & "else");
          recurse_if(level-1, sp & "  ", index+1);
        wd(sp & "end if;");
      end if;
    end recurse_if;

    procedure BCD(pi : integer) is
    begin
      if pi > 1 then write(row,string'(", B")); end if;
      if pi > 2 then write(row,string'(", C")); end if;
      if pi > 3 then write(row,string'(", D")); end if;
    end BCD;

  begin
    gate_kind := Gate_boolean;
    wl("-- File : LibreGates/proasic3/PA3_components.vhdl");
    wl("-- DO NOT MODIFY ! Generated file !");
    wl("-- (C) Yann Guidon 20190808 - 20200902");
    wl("");
    wl("Library ieee;");
    wl("    use ieee.std_logic_1164.all;");
    wl("");
    wl("package proasic3 is");

    wd("-- File : LibreGates/proasic3/PA3_definitions.vhdl");
    wd("-- DO NOT MODIFY ! Generated file !");
    wd("-- (C) Yann Guidon 20190808 - 20200902");
    if gate_delay = "" then
      wd("--  * This version has no delay.");
      report "No gate delay specified. Change this value with -ggate_delay=""1 ns"" ";
    else
      wd("--  * This version has delay set to " & gate_delay);
      report "Gate delay set to " & gate_delay;
    end if;

    if gate_type = "simple" then
      wd("--  * Gate type set to 'simple', advanced analysis is disabled.");
      report "Gate type set to 'simple', advanced analysis is disabled.";
      if gate_delay /= "" then
        wd("--  * Warning : MX2x behaviour might have incorrect timing");
        report "Warning : Gate delay not null => MX2x behaviour might be incorrect";
      end if;
    end if;


    wd("");
    wd("-- Generic gates :");

    j := 2;
    for i in 1 to 4 loop
      write(row,"  component generic_gate" & integer'image(i) & " generic(exclude: std_logic_vector); port(A");
      if i > 1 then  write(row,string'(", B")); else  write(row,string'("   ")); end if;
      if i > 2 then  write(row,string'(", C")); else  write(row,string'("   ")); end if;
      if i > 3 then  write(row,string'(", D")); else  write(row,string'("   ")); end if;
      wl(" : in std_logic; LUT : in std_logic_vector; Y : out std_logic); end component;");

      uselib;
      wd("entity generic_gate" & integer'image(i) & " is");
      wdg(i);
      write(row,string'("  port( A"));
      BCD(i);
      wd(" : in std_logic;");
      wd("        LUT : in std_logic_vector;");
      wd("        Y : out std_logic);");
      wd("end generic_gate" & integer'image(i) & ";");
      wd("");
      wd("architecture " & gate_type & " of generic_gate" & integer'image(i) & " is");
      if gate_type /= "simple" then
        wd("  signal LUT" & integer'image(j) & " : LookupType" & integer'image(j) & ";");
        wd("begin");
        wd("  LUT" & integer'image(j) & " <= RegisterLUT(LUT, LUT" & integer'image(j)
             &  "'instance_name, Gate_Number, "  & integer'image(j-1) & ", exclude, Gate_boolean);");    -- called only once at start
        write(row, string'("  Y <= LookItUp") & integer'image(i) & string'("(A"));
        BCD(i);
        write(row, string'(", LUT") & integer'image(j) & string'(", Gate_Number)"));
        if gate_delay /= "" then
          write(row, " after " & gate_delay);
        end if;
        wd(";");
      else
        c := 'Y';
        if gate_delay /= "" then
          wd("  signal S : std_logic;");
          c := 'S';
        end if;

        wd("begin");
        write(row, string'("  process(A"));
        BCD(i);
        wd(", LUT) begin");
        k := 0;
        recurse_if(i, "    ", 1);
        wd("  end process;");

        if gate_delay /= "" then
          wd("  Y <= S after " & gate_delay & ";");
        end if;
      end if;

      wd("end " & gate_type & ";");

      j := j*2;
    end loop;


    wd("");
    wd("--------------------------------------------------");
    wd("");
    wd("-- 0-input gates :");

    wl("");
    wl("-- 0-input gates :");

    for i in NoInputGates'range loop
      wl("  component "& NoInputGates(i) & " generic(exclude: std_logic_vector); port(Y : out std_logic); end component;");

      uselib;
      wd("entity " & NoInputGates(i) & " is");
      wdg(0);
      wd("  port(Y : out std_logic);");
      wd("end " & NoInputGates(i) & ";");

      wd("");
      wd("architecture " & gate_type & " of " & NoInputGates(i) & " is");
      if gate_type /= "simple" then
        wd("  signal LUT1 : LookupType1;");
        wd("begin");
        wd("  LUT1 <= RegisterLUT(""" & integer'image(i) & integer'image(i)
                 &  """, LUT1'instance_name, Gate_Number, 0, exclude, Gate_boolean);");
        wd("  Y <= LUT1(0);");
      else
        wd("begin");
        wd("  Y <= '" & integer'image(i) & "';");
      end if;
      wd("end " & gate_type & ";");
    end loop;

    wl("");
    wl("-- 1-input gates :");

    wd("");
    wd("--------------------------------------------------");
    wd("");
    wd("-- 1-input gates :");
    
    for i in BufferGates'range loop
      wl("  component "& BufferGates(i) & " generic(exclude: std_logic_vector); port(A : in std_logic; Y : out std_logic); end component;");

      if BufferGates(i)(1) = 'I' then
        L2:="10HL"; -- inverter gate
      else
        L2:="01LH"; -- note: ascending order of the bits !
      end if;
      uselib;
      wd("entity " & trim_spaces(BufferGates(i)) & " is");
      wdg(1);
      wd("  port( A : in std_logic;");
      wd("        Y : out std_logic);");
      wd("end " & trim_spaces(BufferGates(i)) & ";");

      wd("");
      wd("architecture " & gate_type & " of " & trim_spaces(BufferGates(i)) & " is");

      if gate_type /= "simple" then
        wd("  signal LUT2 : LookupType2;");
        wd("begin");
        wd("  LUT2 <= RegisterLUT(""" & SLV_to_bin(L2)
           & """, LUT2'instance_name, Gate_Number, 1, exclude, Gate_boolean);");
        write(row, string'("  Y <= LookItUp1(A, LUT2, Gate_Number)"));
      else
        wd("begin");
        write(row, string'("  Y <= "));
        if BufferGates(i)(1) = 'I' then
          write(row, string'("not "));
        end if;
        write(row, string'("A"));
      end if;
      if gate_delay /= "" then
        write(row, " after " & gate_delay);
      end if;
      wd(";");

      wd("end " & gate_type & ";");
    end loop;


    wl("");
    wl("-- 2-inputs gates :");

    wd("");
    wd("--------------------------------------------------");
    wd("");
    wd("-- 2-inputs gates :");

    for s in EnumGates2 loop
      L4 := PA3_ComputeLUT4(s);
      write(row, "  component " & EnumGates2'image(s), left, 19);
      wl(" generic(exclude: std_logic_vector); port(A, B : in std_logic; Y : out std_logic); end component; -- " & SLV_to_bin(L4(0 to 3)));

      uselib;
      wd("entity " & EnumGates2'image(s) & " is");
      wdg(2);
      wd("  port( A, B : in std_logic;");
      wd("           Y : out std_logic);");
      wd("end " & EnumGates2'image(s) & ";");

      wd("");
      wd("architecture " & gate_type & " of " & EnumGates2'image(s) & " is");
      if gate_type /= "simple" then
        wd("  signal LUT4 : LookupType4;");
        wd("begin");
        wd("  LUT4 <= RegisterLUT(""" & SLV_to_bin(L4)
           & """, LUT4'instance_name, Gate_Number, 3, exclude, Gate_boolean);");
        write(row, string'("  Y <= LookItUp2(A, B, LUT4, Gate_Number)"));
      else
        wd("begin");
	write(row, string'("  Y <= ") & trim_spaces(DefGate2Array(s)) );
      end if;

      if gate_delay /= "" then
        write(row, " after " & gate_delay);
      end if;
      wd(";");

      wd("end " & gate_type & ";");
    end loop;


    wl("");
    wl("-- 3-inputs gates :");
    wd("");
    wd("--------------------------------------------------");
    wd("");
    wd("-- 3-inputs gates :");

    for t in EnumGates3 loop
      case t is
        when MX2  |
             MX2A |
             MX2B |
             MX2C   =>  op := 'S';
        when others =>  op := 'C';
      end case;

      L8 := PA3_ComputeLUT8(t);
      write(row, "  component " & EnumGates3'image(t), left, 19);
      wl(" generic(exclude: std_logic_vector); port(A, B, " & op
          & " : in std_logic; Y : out std_logic); end component; -- " & SLV_to_bin(L8(0 to 7)));

      uselib;
      wd("entity " & EnumGates3'image(t) & " is");
      wdg(3);
      wd("  port( A, B, " & op &" : in std_logic;");
      wd("              Y : out std_logic);");
      wd("end " & EnumGates3'image(t) & ";");

      wd("");
      wd("architecture " & gate_type & " of " & EnumGates3'image(t) & " is");
      if gate_type /= "simple" then
        wd("  signal LUT8 : LookupType8;");
        wd("begin");
        wd("  LUT8 <= RegisterLUT(""" & SLV_to_bin(L8)
            & """, LUT8'instance_name, Gate_Number, 7, exclude, Gate_boolean);");    -- called only once at start
        write(row, string'("  Y <= LookItUp3(A, B, ") & op & string'(", LUT8, Gate_Number)"));
      else
	wd("begin");
        write(row, string'("  Y <= ") & trim_spaces(DefGate3Array(t)) );
      end if;
      if gate_delay /= "" then
        write(row, " after " & gate_delay);
      end if;
      wd(";");
      wd("end " & gate_type & ";");
    end loop;


    wl("");
    wl("-- 4-inputs gates :");
    wd("");
    wd("--------------------------------------------------");
    wd("");
    wd("-- 4-inputs gates :");

    for t in EnumGates4 loop

      L16 := PA3_ComputeLUT16(t);
      write(row, "  component " & EnumGates4'image(t), left, 20);
      wl(" generic(exclude: std_logic_vector); port(A, B, C, D :"
         & " in std_logic; Y : out std_logic); end component; -- " & SLV_to_bin(L16(0 to 15)));

      uselib;
      wd("entity " & EnumGates4'image(t) & " is");
      wdg(4);
      wd("  port( A, B, C, D : in std_logic;");
      wd("                 Y : out std_logic);");
      wd("end " & EnumGates4'image(t) & ";");

      wd("");
      wd("architecture " & gate_type & " of " & EnumGates4'image(t) & " is");
      if gate_type /= "simple" then
        wd("  signal LUT16 : LookupType16;");
        wd("begin");
        wd("  LUT16 <= RegisterLUT(""" & SLV_to_bin(L16)
            & """, LUT16'instance_name, Gate_Number, 15, exclude, Gate_boolean);");    -- called only once at start
        write(row, string'("  Y <= LookItUp4(A, B, C, D, LUT16, Gate_Number)"));
      else
	wd("begin");
        write(row, string'("  Y <= ") & trim_spaces(DefGate4Array(t)) );
      end if;
      if gate_delay /= "" then
        write(row, " after " & gate_delay);
      end if;
      wd(";");
      wd("end " & gate_type & ";");
    end loop;


    wl("");
    wl("-- Sequential gates :");
    wd("");
    wd("--------------------------------------------------");
    wd("");
    wd("-- Sequential gates :");
    gate_kind := Gate_register;

    for i in EnumDFF'range loop
      sinx := 2;

      if EnumDFF(i)(2)='F' then
        GCLK := "CLK";
      else
        GCLK := "G  ";
      end if;

      if EnumDFF(i)(3) = 'I' then
        QN := "QN";
        L2:="10LH"; -- inverter
      else
        QN := " Q";
        L2:="01HL";
      end if;

      StrE := "  ";
      j := 5;
      if EnumDFF(i)(5) = 'E' then
        sinx := sinx + 1;
        StrE := "E,";
        j := 7;
        Epol := EnumDFF(i)(6);
      end if;

      StrP := "     ";
      if EnumDFF(i)(j) = 'P' then
        sinx := sinx + 1;
        StrP := ", PRE";
        Ppol := EnumDFF(i)(j+1);
        j := j+2;
      end if;

      StrC := "     ";
      if j < 8 and EnumDFF(i)(j) = 'C' then
        sinx := sinx + 1;
        StrC := ", CLR";
        Cpol := EnumDFF(i)(j+1);
      end if;

      wl("  component " & EnumDFF(i) & " generic(exclude: std_logic_vector); port(D, "
         & StrE & " " & GCLK & StrP & StrC
         & " : in std_logic; " & QN &" : out std_logic); end component;");

      uselib;
      wd("entity " & trim_spaces(EnumDFF(i)) & " is");
      wdg(sinx);
      wd("  port(D, " & StrE & " " & GCLK & StrP & StrC & " : in std_logic;");
      wd("       " & QN & " : out std_logic);");
      wd("end " & trim_spaces(EnumDFF(i)) & ";");
      wd("");
      wd("architecture " & gate_type & " of " & trim_spaces(EnumDFF(i)) & " is");
      if gate_type /= "simple" then
        wd("  signal LUT2 : LookupType2;");
      end if;
      wd("begin");
      if gate_type /= "simple" then
      wd("  LUT2 <= RegisterLUT(""" & SLV_to_bin(L2)
         & """, LUT2'instance_name, Gate_Number, 1, exclude, Gate_register);");
      end if;

      write(row, string'("  process(" & GCLK));
      if EnumDFF(i)(2)='L' then
        write(row, string'(", D"));
      end if;
      if StrE(1) = 'E' then
        write(row, string'(", E"));
      end if;
      if StrP(1) = ',' then
        write(row, StrP);
      end if;
      if StrC(1) = ',' then
        write(row, StrC);
      end if;
      wd(")");
      wd("  begin");

      shift_indent(4);

      if StrC(1) = ',' then
        wdi("if CLR='" & Cpol & "' then");
        write(row, spaces(1 to indent) & "  "& QN &" <= '0'");
        if gate_delay /= "" then
          write(row, " after " & gate_delay);
        end if;
        wd(";");
        wdi("else");
        shift_indent(6);
      end if;

        -- Clear has precedence over Preset
        if StrP(1) = ',' then
          wdi("if PRE='" & Ppol & "' then");
          write(row, spaces(1 to indent) & "  "& QN &" <= '1'");
          if gate_delay /= "" then
            write(row, " after " & gate_delay);
          end if;
          wd(";");
          wdi("else");
          shift_indent(indent+2);
        end if;

          write(row, spaces(1 to indent) & "if ");

          if EnumDFF(i)(2)='F' then -- Flip Flop
            if EnumDFF(i)(4)='1' then
              write(row, string'("rising"));
            else
              write(row, string'("falling"));
            end if;
            write(row, string'("_edge(CLK)"));
            if StrE(1) = 'E' then
              write(row, " and E='" & Epol & "'");
            end if;
          else         -- Latch
            write(row, "G='" & EnumDFF(i)(4) & "'");
          end if;

          wd(" then");
          write(row, spaces(1 to indent) & "  " & QN & string'(" <= ") );

          if gate_type /= "simple" then
            write(row, string'("LookItUp1(D , LUT2, Gate_Number)"));
          else
            write(row, string'("D"));
          end if;

          if gate_delay /= "" then
            write(row, " after " & gate_delay);
          end if;
          wd(";");
          wdi("end if;");

        if StrP(1) = ',' then
          shift_indent(indent-2);
          wdi("end if;");
        end if;

      if StrC(1) = ',' then
        shift_indent(indent-2);
        wdi("end if;");
      end if;

      wd("  end process;");
      wd("end " & gate_type & ";");
    end loop;


    wl("");
    wl("");
    wl("  component RAM4K9 port(");
    wl("    ADDRA11, ADDRA10, ADDRA9, ADDRA8, ADDRA7, ADDRA6,");
    wl("    ADDRA5,  ADDRA4,  ADDRA3, ADDRA2, ADDRA1, ADDRA0,");
    wl("    ADDRB11, ADDRB10, ADDRB9, ADDRB8, ADDRB7, ADDRB6,");
    wl("    ADDRB5,  ADDRB4,  ADDRB3, ADDRB2, ADDRB1, ADDRB0,");
    wl("    DINA8, DINA7, DINA6, DINA5, DINA4, DINA3, DINA2, DINA1, DINA0,");
    wl("    DINB8, DINB7, DINB6, DINB5, DINB4, DINB3, DINB2, DINB1, DINB0,");
    wl("    WIDTHA0, WIDTHA1, WIDTHB0, WIDTHB1, PIPEA, PIPEB, WMODEA, WMODEB,");
    wl("    BLKA, BLKB, WENA, WENB, CLKA, CLKB, RESET : in std_logic := 'U';");
    wl("    DOUTA8, DOUTA7, DOUTA6, DOUTA5, DOUTA4, DOUTA3, DOUTA2, DOUTA1, DOUTA0,");
    wl("    DOUTB8, DOUTB7, DOUTB6, DOUTB5, DOUTB4, DOUTB3, DOUTB2, DOUTB1, DOUTB0 : out std_logic);");
    wl("  end component;");
    wl("");
    wl("end proasic3;");

    wait;
  end process;

end runme;
