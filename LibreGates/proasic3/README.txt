file LibreGates/proasic3/README.txt
mar. sept.  1 04:19:05 CEST 2020

This is the directory for the
ProASIC3-specific gates library
released under AGPLv3 by Yann Guidon.

You can find the latest release at
https://hackaday.io/project/162594
and http://libregates.org
