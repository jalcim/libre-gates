-- LibreGates/proasic3/SRFF_PA3.vhdl
-- created mar. août 25 02:51:59 CEST 2020 by whygee@f-cpu.org
-- version mar. sept. 1 03:55:57 CEST 2020
-- version mer. sept. 2 20:16:42 CEST 2020 : LibreGates !
--
-- Here are various Set/Reset Flip-Flops, defined by the precedence
-- of the control signals and their active levels.
--
-- Precedence  Set  Reset  Macro
--            level level  name  Mapped to
--      Set     0     0    S0R0   AO1A
--      Set     0     1    S0R1   AO1C
--      Set     1     0    S1R0   AO1, AON21
--      Set     1     1    S1R1   AO1B, DLI1P1C1, AON21B
--    Reset     0     0    R0S0   OA1A
--    Reset     0     1    R1S0   OA1C
--    Reset     1     0    R0S1   OA1, OAN21
--    Reset     1     1    R1S1   OA1B, DLN1P1C1, OAN21B
--
-------------------- SET precedence --------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library proasic3;
    use proasic3.all;

entity S0R0 is
  port( S, R : in std_logic;
           Y : out std_logic);
end S0R0;

architecture A3P of S0R0 is
  component backwards port( A : in std_logic; Y : out std_logic); end component;
  component AO1A port(A, B, C : in std_logic; Y : out std_logic); end component;
  signal i, o: std_logic := '0';
begin
  la: AO1A port map(A=>S, B=>i, C=>R, Y=>o);
  fb: backwards port map(A=>o, Y=>i);
  Y <= o;
end A3P;


Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library proasic3;
    use proasic3.all;

entity S0R1 is
  port( S, R : in std_logic;
           Y : out std_logic);
end S0R1;

architecture A3P of S0R1 is
  component backwards port( A : in std_logic; Y : out std_logic); end component;
  component AO1C port(A, B, C : in std_logic; Y : out std_logic); end component;
  signal i, o: std_logic := '0';
begin
  la: AO1C port map(A=>S, B=>i, C=>R, Y=>o);
  fb: backwards port map(A=>o, Y=>i);
  Y <= o;
end A3P;


Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library proasic3;
    use proasic3.all;

entity S1R0 is
  port( S, R : in std_logic;
           Y : out std_logic);
end S1R0;

architecture A3P of S1R0 is
  component backwards port( A : in std_logic; Y : out std_logic); end component;
  component AO1  port(A, B, C : in std_logic; Y : out std_logic); end component;
  signal i, o: std_logic := '0';
begin
  la: AO1 port map(A=>S, B=>i, C=>R, Y=>o);
  fb: backwards port map(A=>o, Y=>i);
  Y <= o;
end A3P;


Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library proasic3;
    use proasic3.all;

entity S1R1 is
  port( S, R : in std_logic;
           Y : out std_logic);
end S1R1;

architecture A3P of S1R1 is
  component backwards port( A : in std_logic; Y : out std_logic); end component;
  component AO1B port(A, B, C : in std_logic; Y : out std_logic); end component;
  signal i, o: std_logic := '0';
begin
  la: AO1B port map(A=>S, B=>i, C=>R, Y=>o);
  fb: backwards port map(A=>o, Y=>i);
  Y <= o;
end A3P;


-------------------- RESET precedence --------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library proasic3;
    use proasic3.all;

entity R0S0 is
  port( S, R : in std_logic;
           Y : out std_logic);
end R0S0;

architecture A3P of R0S0 is
  component backwards port( A : in std_logic; Y : out std_logic); end component;
  component OA1A port(A, B, C : in std_logic; Y : out std_logic); end component;
  signal i, o: std_logic := '0';
begin
  la: OA1A port map(A=>S, B=>i, C=>R, Y=>o);
  fb: backwards port map(A=>o, Y=>i);
  Y <= o;
end A3P;


Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library proasic3;
    use proasic3.all;

entity R1S0 is
  port( S, R : in std_logic;
           Y : out std_logic);
end R1S0;

architecture A3P of R1S0 is
  component backwards port( A : in std_logic; Y : out std_logic); end component;
  component OA1C port(A, B, C : in std_logic; Y : out std_logic); end component;
  signal i, o: std_logic := '0';
begin
  la: OA1C  port map(A=>S, B=>i, C=>R, Y=>o);
  fb: backwards port map(A=>o, Y=>i);
  Y <= o;
end A3P;


Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library proasic3;
    use proasic3.all;

entity R0S1 is
  port( S, R : in std_logic;
           Y : out std_logic);
end R0S1;

architecture A3P of R0S1 is
  component backwards port( A : in std_logic; Y : out std_logic); end component;
  component OA1  port(A, B, C : in std_logic; Y : out std_logic); end component;
  signal i, o: std_logic := '0';
begin
  la: OA1 port map(A=>S, B=>i, C=>R, Y=>o);
  fb: backwards port map(A=>o, Y=>i);
  Y <= o;
end A3P;


Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library proasic3;
    use proasic3.all;

entity R1S1 is
  port( S, R : in std_logic;
           Y : out std_logic);
end R1S1;

architecture A3P of R1S1 is
  component backwards port( A : in std_logic; Y : out std_logic); end component;
  component OA1B port(A, B, C : in std_logic; Y : out std_logic); end component;
  signal i, o: std_logic := '0';
begin
  la: OA1B port map(A=>S, B=>i, C=>R, Y=>o);
  fb: backwards port map(A=>o, Y=>i);
  Y <= o;
end A3P;
