-- LibreGates/proasic3/aliases.vhdl
-- jeu. juil. 16 01:31:49 CEST 2020
-- version dim. août 16 06:40:14 CEST 2020 : removed the useless inclusion of work.PA3_genlib.all
-- version mer. sept.  2 20:16:42 CEST 2020 : LibreGates !
--
-- "maps" some ASIC gates to A3P gates.

Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library proasic3;
    use proasic3.all;

entity iv1v0x4 is
  port( A : in std_logic;
        Y : out std_logic);
end iv1v0x4;

architecture aliases of iv1v0x4 is
  signal t : std_logic;
begin
 e: entity INV port map(A=>A, Y=>Y);
end aliases;


