#!/bin/bash
# LibreGates/proasic3/build.sh
# version dim. déc. 29 20:03:56 2019
# version lun. mars 23 03:04:31 2020
# version jeu. jul. 16 01:38:59 2020 : added aliases.vhdl
# version dim. août 16 06:23:19 2020 : added the "simple" version in its own directory.
# version mar. sept.  1 06:03:30 CEST 2020 : split the A3P code from the generic stuff
# version mer. sept.  2 20:16:42 CEST 2020 : moved proasic3 into LibreGates/
# version dim. nov. 22 11:31:28 CET 2020 : parallel, deduplicated

VER="--std=93"
LIBS=$(pwd)"/.."
PLIBS="-P"$LIBS

function task_simple () {
  mkdir -p simple &&
  cd simple &&
    ../pa3_gategen -ggate_type="simple" &&
    ghdl -a $VER $PLIBS -P.. --work=proasic3 PA3_definitions.vhdl PA3_components.vhdl \
       ../PA3_combi.vhdl ../aliases.vhdl $LIBS/Backwards_simple.vhdl $LIBS/SRFF_simple.vhdl &&
  rm PA3_components.vhdl # save a bit of storage...
  ## 1.85s - 0.69
}

function task_delay () {
  ./pa3_gategen -ggate_delay="1 ns" &&
  ghdl -a $VER $PLIBS --work=proasic3 PA3_definitions.vhdl PA3_components.vhdl \
    PA3_combi.vhdl aliases.vhdl $LIBS/Backwards_analysis.vhdl SRFF_PA3.vhdl
}

# some cleanup :
rm -rf *.cf *.o pa3_gategen PA3_components.vhdl PA3_definitions.vhdl simple &&

# compile the "gate generator":
ghdl -a $VER $PLIBS PA3_gategen.vhdl &&
ghdl -e $VER $PLIBS pa3_gategen &&
### 0.69s

{ # run the two tasks in parallel
  task_delay &
  task_simple &
} &&

wait -n &&
wait -n &&

ln -s ../PA3_components.vhdl simple/PA3_components.vhdl &&

echo $'\e[32m'"ProASIC3 Library generation : OK"$'\e[0m'
