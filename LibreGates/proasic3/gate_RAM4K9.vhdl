-- File LibreGates/proasic3/gate_RAM4K9.vhdl
-- created lun. mai 12 23:59:59 CEST 2014 by Yann Guidon (whygee@f-cpu.org)
-- Released under the GNU AGPLv3 license (see the license/ directory)
-- Just a little replacement for the Actel ProASIC3 gates/macros library
--  to help simulate PA3-optimised source code.

-- Note : It's just a  high-level description with quite limited acuracy,
-- aimed at not-too-slow simulation with GHDL. Do not synthesise !
-- Concurrent write to the same address is not detected, beware !
-- And the memory layout is dumb lazy, don't play with side effects,
-- use the original vendor files instead. This one is just a little convenience.

Library ieee;
    use ieee.std_logic_1164.all;

entity RAM4K9 is
  port(
    ADDRA11, ADDRA10, ADDRA9, ADDRA8, ADDRA7, ADDRA6, 
    ADDRA5,  ADDRA4,  ADDRA3, ADDRA2, ADDRA1, ADDRA0,
    ADDRB11, ADDRB10, ADDRB9, ADDRB8, ADDRB7, ADDRB6,
    ADDRB5,  ADDRB4,  ADDRB3, ADDRB2, ADDRB1, ADDRB0,
    DINA8, DINA7, DINA6, DINA5, DINA4, DINA3, DINA2, DINA1, DINA0,
    DINB8, DINB7, DINB6, DINB5, DINB4, DINB3, DINB2, DINB1, DINB0,
    WIDTHA0, WIDTHA1, WIDTHB0, WIDTHB1, PIPEA, PIPEB, WMODEA, WMODEB, 
    BLKA, BLKB, WENA, WENB, CLKA, CLKB, RESET : in std_logic := 'U';
    DOUTA8, DOUTA7, DOUTA6, DOUTA5, DOUTA4, DOUTA3, DOUTA2, DOUTA1, DOUTA0,
    DOUTB8, DOUTB7, DOUTB6, DOUTB5, DOUTB4, DOUTB3, DOUTB2, DOUTB1, DOUTB0 : out std_logic);
end RAM4K9;

architecture rtl of RAM4K9 is
  -- Maximal size and width, meaning space is wasted but the code is shorter :
  type mem_type is array (11 downto 0) of std_logic_vector(8 downto 0);
  shared variable mem: mem_type;
  shared variable DmaxA, AmaxA, DmaxB, AmaxB : integer;
begin

  -- separate because it won't toggle much:

  assert WMODEA='1'
    report "PortA : Only Pass Through write mode supported by this model."
    severity warning;
  assert WMODEB='1'
    report "PortB : Only Pass Through write mode supported by this model."
    severity warning;
  assert PIPEA='0'
    report "PortA : Pipelined read mode NOT supported by this model."
    severity warning;
  assert PIPEB='0'
    report "PortB : Pipelined read mode NOT supported by this model."
    severity warning;

  process(WIDTHA1, WIDTHA0)
  begin
    if WIDTHA1='0' then
      if WIDTHA0='0' then
        DmaxA := 1; AmaxA := 11;
      else
        DmaxA := 2; AmaxA := 10;
      end if;
    else
      if WIDTHA0='0' then
        DmaxA := 4; AmaxA :=  9;
      else
        DmaxA := 9; AmaxA :=  8;
      end if;
    end if;
    -- case-when is shorter but not working as well
  end process;

  process(WIDTHB1, WIDTHB0)
  begin
    if WIDTHB1='0' then
      if WIDTHB0='0' then
        DmaxB := 1; AmaxB := 11;
      else
        DmaxB := 2; AmaxB := 10;
      end if;
    else
      if WIDTHB0='0' then
        DmaxB := 4; AmaxB :=  9;
      else
        DmaxB := 9; AmaxB :=  8;
      end if;
    end if;
  end process;

  -- The clocked code :

  process(CLKA, RESET)
    variable dout, din : std_logic_vector(8 downto 0);
    variable ad : integer;
  begin
    din := DINA8 & DINA7 & DINA6 & DINA5 & DINA4 & DINA3 & DINA2 & DINA1 & DINA0;

    if (RESET='0') then
      dout(DmaxA downto 0) := (others=>'0');
    else
      if rising_edge(CLKA) then
        ad := 0;
        if ADDRA0='1'  then ad := ad+1; end if;
        if ADDRA1='1'  then ad := ad+2; end if;
        if ADDRA2='1'  then ad := ad+4; end if;
        if ADDRA3='1'  then ad := ad+8; end if;
        if ADDRA4='1'  then ad := ad+16; end if;
        if ADDRA5='1'  then ad := ad+32; end if;
        if ADDRA6='1'  then ad := ad+64; end if;
        if ADDRA7='1'  then ad := ad+128; end if;
        if ADDRA8='1'  then ad := ad+256; end if;
        if AmaxA >= 9 then
          if ADDRA9='1'  then ad := ad+512; end if;
          if AmaxA >= 10 then
            if ADDRA10='1' then ad := ad+1024; end if;
            if AmaxA >= 11 then
              if ADDRA11='1' then ad := ad+2048; end if;
            end if;
          end if;
        end if;

        if WENA='0' then
          mem(ad) := din;
        end if;
        dout := mem(ad);
      end if;
    end if;

    if (BLKA='0' or RESET='0') then
      dout(8 downto DmaxA) := (others=>'X');
      DOUTA0 <= dout(0);
      DOUTA1 <= dout(1);
      DOUTA2 <= dout(2);
      DOUTA3 <= dout(3);
      DOUTA4 <= dout(4);
      DOUTA5 <= dout(5);
      DOUTA6 <= dout(6);
      DOUTA7 <= dout(7);
      DOUTA8 <= dout(8);
    end if;
  end process;

  process(CLKB, RESET)
    variable dout, din : std_logic_vector(8 downto 0);
    variable ad : integer;
  begin
    din := DINB8 & DINB7 & DINB6 & DINB5 & DINB4 & DINB3 & DINB2 & DINB1 & DINB0;

    if (RESET='0') then
      dout(DmaxB downto 0) := (others=>'0');
    else
      if rising_edge(CLKB) then
        ad := 0;
        if ADDRB0='1'  then ad := ad+1; end if;
        if ADDRB1='1'  then ad := ad+2; end if;
        if ADDRB2='1'  then ad := ad+4; end if;
        if ADDRB3='1'  then ad := ad+8; end if;
        if ADDRB4='1'  then ad := ad+16; end if;
        if ADDRB5='1'  then ad := ad+32; end if;
        if ADDRB6='1'  then ad := ad+64; end if;
        if ADDRB7='1'  then ad := ad+128; end if;
        if ADDRB8='1'  then ad := ad+256; end if;
        if AmaxB >= 9 then
          if ADDRB9='1'  then ad := ad+512; end if;
          if AmaxB >= 10 then
            if ADDRB10='1' then ad := ad+1024; end if;
            if AmaxB >= 11 then
              if ADDRB11='1' then ad := ad+2048; end if;
            end if;
          end if;
        end if;

        if WENB='0' then
          mem(ad) := din;
        end if;
        dout := mem(ad);
      end if;
    end if;

    if (BLKB='0' or RESET='0') then
      dout(8 downto DmaxB) := (others=>'X');
      DOUTB0 <= dout(0);
      DOUTB1 <= dout(1);
      DOUTB2 <= dout(2);
      DOUTB3 <= dout(3);
      DOUTB4 <= dout(4);
      DOUTB5 <= dout(5);
      DOUTB6 <= dout(6);
      DOUTB7 <= dout(7);
      DOUTB8 <= dout(8);
    end if;
  end process;

end rtl;
