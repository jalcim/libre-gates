-- File : LibreGates/proasic3/PA3_definitions.vhdl
-- DO NOT MODIFY ! Generated file !
-- (C) Yann Guidon 20190808 - 20200902
--  * This version has no delay.
--  * Gate type set to 'simple', advanced analysis is disabled.

-- Generic gates :

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity generic_gate1 is
  generic( exclude: std_logic_vector := "");
  port( A : in std_logic;
        LUT : in std_logic_vector;
        Y : out std_logic);
end generic_gate1;

architecture simple of generic_gate1 is
begin
  process(A, LUT) begin
    if A/='1' then
      Y <= LUT(0);
    else
      Y <= LUT(1);
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity generic_gate2 is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
        LUT : in std_logic_vector;
        Y : out std_logic);
end generic_gate2;

architecture simple of generic_gate2 is
begin
  process(A, B, LUT) begin
    if A/='1' then
      if B/='1' then
        Y <= LUT(0);
      else
        Y <= LUT(1);
      end if;
    else
      if B/='1' then
        Y <= LUT(2);
      else
        Y <= LUT(3);
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity generic_gate3 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
        LUT : in std_logic_vector;
        Y : out std_logic);
end generic_gate3;

architecture simple of generic_gate3 is
begin
  process(A, B, C, LUT) begin
    if A/='1' then
      if B/='1' then
        if C/='1' then
          Y <= LUT(0);
        else
          Y <= LUT(1);
        end if;
      else
        if C/='1' then
          Y <= LUT(2);
        else
          Y <= LUT(3);
        end if;
      end if;
    else
      if B/='1' then
        if C/='1' then
          Y <= LUT(4);
        else
          Y <= LUT(5);
        end if;
      else
        if C/='1' then
          Y <= LUT(6);
        else
          Y <= LUT(7);
        end if;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity generic_gate4 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C, D : in std_logic;
        LUT : in std_logic_vector;
        Y : out std_logic);
end generic_gate4;

architecture simple of generic_gate4 is
begin
  process(A, B, C, D, LUT) begin
    if A/='1' then
      if B/='1' then
        if C/='1' then
          if D/='1' then
            Y <= LUT(0);
          else
            Y <= LUT(1);
          end if;
        else
          if D/='1' then
            Y <= LUT(2);
          else
            Y <= LUT(3);
          end if;
        end if;
      else
        if C/='1' then
          if D/='1' then
            Y <= LUT(4);
          else
            Y <= LUT(5);
          end if;
        else
          if D/='1' then
            Y <= LUT(6);
          else
            Y <= LUT(7);
          end if;
        end if;
      end if;
    else
      if B/='1' then
        if C/='1' then
          if D/='1' then
            Y <= LUT(8);
          else
            Y <= LUT(9);
          end if;
        else
          if D/='1' then
            Y <= LUT(10);
          else
            Y <= LUT(11);
          end if;
        end if;
      else
        if C/='1' then
          if D/='1' then
            Y <= LUT(12);
          else
            Y <= LUT(13);
          end if;
        else
          if D/='1' then
            Y <= LUT(14);
          else
            Y <= LUT(15);
          end if;
        end if;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

-- 0-input gates :

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity GND is
  generic( exclude: std_logic_vector := "");
  port(Y : out std_logic);
end GND;

architecture simple of GND is
begin
  Y <= '0';
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity VCC is
  generic( exclude: std_logic_vector := "");
  port(Y : out std_logic);
end VCC;

architecture simple of VCC is
begin
  Y <= '1';
end simple;

--------------------------------------------------

-- 1-input gates :

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity BUFD is
  generic( exclude: std_logic_vector := "");
  port( A : in std_logic;
        Y : out std_logic);
end BUFD;

architecture simple of BUFD is
begin
  Y <= A;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity BUFF is
  generic( exclude: std_logic_vector := "");
  port( A : in std_logic;
        Y : out std_logic);
end BUFF;

architecture simple of BUFF is
begin
  Y <= A;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity CLKINT is
  generic( exclude: std_logic_vector := "");
  port( A : in std_logic;
        Y : out std_logic);
end CLKINT;

architecture simple of CLKINT is
begin
  Y <= A;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity INV is
  generic( exclude: std_logic_vector := "");
  port( A : in std_logic;
        Y : out std_logic);
end INV;

architecture simple of INV is
begin
  Y <= not A;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity INVD is
  generic( exclude: std_logic_vector := "");
  port( A : in std_logic;
        Y : out std_logic);
end INVD;

architecture simple of INVD is
begin
  Y <= not A;
end simple;

--------------------------------------------------

-- 2-inputs gates :

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity and2 is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end and2;

architecture simple of and2 is
begin
  Y <=          A  and      B;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity and2a is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end and2a;

architecture simple of and2a is
begin
  Y <=     (not A) and      B;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity and2b is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end and2b;

architecture simple of and2b is
begin
  Y <=     (not A) and (not B);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity nand2 is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end nand2;

architecture simple of nand2 is
begin
  Y <= not(     A  and      B);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity nand2a is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end nand2a;

architecture simple of nand2a is
begin
  Y <= not((not A) and      B);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity nand2b is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end nand2b;

architecture simple of nand2b is
begin
  Y <= not((not A) and (not B));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity nor2 is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end nor2;

architecture simple of nor2 is
begin
  Y <=          A  nor      B;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity nor2a is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end nor2a;

architecture simple of nor2a is
begin
  Y <= not((not A)  or      B);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity nor2b is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end nor2b;

architecture simple of nor2b is
begin
  Y <= not((not A)  or (not B));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity or2 is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end or2;

architecture simple of or2 is
begin
  Y <=          A   or      B;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity or2a is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end or2a;

architecture simple of or2a is
begin
  Y <=     (not A)  or      B;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity or2b is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end or2b;

architecture simple of or2b is
begin
  Y <=     (not A)  or (not B);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity xnor2 is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end xnor2;

architecture simple of xnor2 is
begin
  Y <= not     (A  xor      B);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity xor2 is
  generic( exclude: std_logic_vector := "");
  port( A, B : in std_logic;
           Y : out std_logic);
end xor2;

architecture simple of xor2 is
begin
  Y <=          A  xor      B;
end simple;

--------------------------------------------------

-- 3-inputs gates :

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity and3 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end and3;

architecture simple of and3 is
begin
  Y <= (     A  and      B ) and      C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity and3a is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end and3a;

architecture simple of and3a is
begin
  Y <= ((not A) and      B ) and      C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity and3b is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end and3b;

architecture simple of and3b is
begin
  Y <= ((not A) and (not B)) and      C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity and3c is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end and3c;

architecture simple of and3c is
begin
  Y <= ((not A) and (not B)) and (not C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ao1 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao1;

architecture simple of ao1 is
begin
  Y <=      (     A  and      B ) or      C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ao12 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao12;

architecture simple of ao12 is
begin
  Y <= ((not A) and B) or ((not A) and (not C)) or (B and (not C)) or (A and (not B) and C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ao13 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao13;

architecture simple of ao13 is
begin
  Y <= (A and B) or (A and (not C)) or (B and (not C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ao14 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao14;

architecture simple of ao14 is
begin
  Y <= (A and B) or (A and (not C)) or (B and (not C)) or ((not A) and (not B) and C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ao15 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao15;

architecture simple of ao15 is
begin
  Y <= (A and (not B) and C) or ((not A) and B and C) or ((not A) and (not B) and (not C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ao16 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao16;

architecture simple of ao16 is
begin
  Y <= (A and B and (not C)) or ((not A) and (not B) and C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ao17 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao17;

architecture simple of ao17 is
begin
  Y <= (A and B and C) or ((not A) and B and (not C)) or ((not A) and (not B) and C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ao18 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao18;

architecture simple of ao18 is
begin
  Y <= ((not A) and B) or ((not A) and (not C)) or (B and (not C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ao1a is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao1a;

architecture simple of ao1a is
begin
  Y <=      ((not A) and      B ) or      C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ao1b is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao1b;

architecture simple of ao1b is
begin
  Y <=      (     A  and      B ) or (not C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ao1c is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao1c;

architecture simple of ao1c is
begin
  Y <=      ((not A) and      B ) or (not C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ao1d is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao1d;

architecture simple of ao1d is
begin
  Y <=      ((not A) and (not B)) or      C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ao1e is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ao1e;

architecture simple of ao1e is
begin
  Y <=      ((not A) and (not B)) or (not C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity aoi1 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end aoi1;

architecture simple of aoi1 is
begin
  Y <= not ((     A  and      B ) or      C );
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity aoi1a is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end aoi1a;

architecture simple of aoi1a is
begin
  Y <= not (((not A) and      B ) or      C );
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity aoi1b is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end aoi1b;

architecture simple of aoi1b is
begin
  Y <= not ((     A  and      B ) or (not C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity aoi1c is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end aoi1c;

architecture simple of aoi1c is
begin
  Y <= not (((not A) and (not B)) or      C );
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity aoi1d is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end aoi1d;

architecture simple of aoi1d is
begin
  Y <= not (((not A) and (not B)) or (not C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity aoi5 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end aoi5;

architecture simple of aoi5 is
begin
  Y <= not (((not A) and B and C) or (A and (not B) and (not C)) );
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ax1 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ax1;

architecture simple of ax1 is
begin
  Y <=     ((not A) and      B ) xor C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ax1a is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ax1a;

architecture simple of ax1a is
begin
  Y <= not(((not A) and      B ) xor C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ax1b is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ax1b;

architecture simple of ax1b is
begin
  Y <=     ((not A) and (not B)) xor C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ax1c is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ax1c;

architecture simple of ax1c is
begin
  Y <=     (     A  and      B ) xor C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ax1d is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ax1d;

architecture simple of ax1d is
begin
  Y <= not(((not A) and (not B)) xor C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity ax1e is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end ax1e;

architecture simple of ax1e is
begin
  Y <= not((     A  and      B ) xor C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity axo1 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axo1;

architecture simple of axo1 is
begin
  Y <=     (     A  and      B ) or (     B  xor C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity axo2 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axo2;

architecture simple of axo2 is
begin
  Y <=     ((not A) and      B ) or (     B  xor C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity axo3 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axo3;

architecture simple of axo3 is
begin
  Y <=     (     A  and (not B)) or (     B  xor C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity axo5 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axo5;

architecture simple of axo5 is
begin
  Y <=     ((not A) and      B ) or ((not B) xor C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity axo6 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axo6;

architecture simple of axo6 is
begin
  Y <=     (     A  and (not B)) or ((not B) xor C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity axo7 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axo7;

architecture simple of axo7 is
begin
  Y <=     ((not A) and (not B)) or (     B  xor C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity axoi1 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axoi1;

architecture simple of axoi1 is
begin
  Y <= not((     A  and      B ) or (     B  xor C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity axoi2 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axoi2;

architecture simple of axoi2 is
begin
  Y <= not(((not A) and      B ) or (     B  xor C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity axoi3 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axoi3;

architecture simple of axoi3 is
begin
  Y <= not((     A  and (not B)) or (     B  xor C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity axoi4 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axoi4;

architecture simple of axoi4 is
begin
  Y <= not((     A  and      B ) or ((not B) xor C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity axoi5 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axoi5;

architecture simple of axoi5 is
begin
  Y <= not(((not A) and      B ) or ((not B) xor C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity axoi7 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end axoi7;

architecture simple of axoi7 is
begin
  Y <= not(((not A) and (not B)) or (     B  xor C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity maj3 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end maj3;

architecture simple of maj3 is
begin
  Y <=     (A and B            ) or (A and             C) or (            B and C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity maj3x is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end maj3x;

architecture simple of maj3x is
begin
  Y <=     (A and B and (not C)) or (A and (not B) and C) or ((not A) and B and C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity maj3xi is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end maj3xi;

architecture simple of maj3xi is
begin
  Y <= not((A and B and (not C)) or (A and (not B) and C) or ((not A) and B and C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity min3 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end min3;

architecture simple of min3 is
begin
  Y <=     ((not A) and (not B)      ) or ((not A) and       (not C)) or (      (not B) and (not C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity min3x is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end min3x;

architecture simple of min3x is
begin
  Y <=     ((not A) and (not B) and C) or ((not A) and B and (not C)) or (A and (not B) and (not C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity min3xi is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end min3xi;

architecture simple of min3xi is
begin
  Y <= not(((not A) and (not B) and C) or ((not A) and B and (not C)) or (A and (not B) and (not C)));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity mx2 is
  generic( exclude: std_logic_vector := "");
  port( A, B, S : in std_logic;
              Y : out std_logic);
end mx2;

architecture simple of mx2 is
begin
  Y <=      A  when S/='1' else      B;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity mx2a is
  generic( exclude: std_logic_vector := "");
  port( A, B, S : in std_logic;
              Y : out std_logic);
end mx2a;

architecture simple of mx2a is
begin
  Y <= (not A) when S/='1' else      B;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity mx2b is
  generic( exclude: std_logic_vector := "");
  port( A, B, S : in std_logic;
              Y : out std_logic);
end mx2b;

architecture simple of mx2b is
begin
  Y <=      A  when S/='1' else (not B);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity mx2c is
  generic( exclude: std_logic_vector := "");
  port( A, B, S : in std_logic;
              Y : out std_logic);
end mx2c;

architecture simple of mx2c is
begin
  Y <= (not A) when S/='1' else (not B);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity nand3 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nand3;

architecture simple of nand3 is
begin
  Y <= not ((     A  and      B ) and      C );
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity nand3a is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nand3a;

architecture simple of nand3a is
begin
  Y <= not (((not A) and      B ) and      C );
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity nand3b is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nand3b;

architecture simple of nand3b is
begin
  Y <= not (((not A) and (not B)) and      C );
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity nand3c is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nand3c;

architecture simple of nand3c is
begin
  Y <= not (((not A) and (not B)) and (not C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity nor3 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nor3;

architecture simple of nor3 is
begin
  Y <= not ((    A  or      B) or      C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity nor3a is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nor3a;

architecture simple of nor3a is
begin
  Y <= not ((not A) or      B  or      C );
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity nor3b is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nor3b;

architecture simple of nor3b is
begin
  Y <= not ((not A) or (not B) or      C );
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity nor3c is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end nor3c;

architecture simple of nor3c is
begin
  Y <= not ((not A) or (not B) or (not C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity oa1 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end oa1;

architecture simple of oa1 is
begin
  Y <= (     A  or B) and      C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity oa1a is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end oa1a;

architecture simple of oa1a is
begin
  Y <= ((not A) or B) and      C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity oa1b is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end oa1b;

architecture simple of oa1b is
begin
  Y <= (     A  or B) and (not C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity oa1c is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end oa1c;

architecture simple of oa1c is
begin
  Y <= ((not A) or B) and (not C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity oai1 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end oai1;

architecture simple of oai1 is
begin
  Y <= not ((A or B) and C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity or3 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end or3;

architecture simple of or3 is
begin
  Y <= (A or B) or C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity or3a is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end or3a;

architecture simple of or3a is
begin
  Y <= (not A) or      B    or      C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity or3b is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end or3b;

architecture simple of or3b is
begin
  Y <= (not A) or (not B)   or      C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity or3c is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end or3c;

architecture simple of or3c is
begin
  Y <= (not A) or (not B)   or (not C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity xa1 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xa1;

architecture simple of xa1 is
begin
  Y <=          (A xor B)  and      C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity xa1a is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xa1a;

architecture simple of xa1a is
begin
  Y <=     (not (A xor B)) and      C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity xa1b is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xa1b;

architecture simple of xa1b is
begin
  Y <=          (A xor B)  and (not C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity xa1c is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xa1c;

architecture simple of xa1c is
begin
  Y <=     (not (A xor B)) and (not C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity xai1 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xai1;

architecture simple of xai1 is
begin
  Y <= not(     (A xor B)  and      C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity xai1a is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xai1a;

architecture simple of xai1a is
begin
  Y <= not((not (A xor B)) and      C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity xo1 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xo1;

architecture simple of xo1 is
begin
  Y <=      (A xor B)  or C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity xo1a is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xo1a;

architecture simple of xo1a is
begin
  Y <= (not (A xor B)) or C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity xor3 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xor3;

architecture simple of xor3 is
begin
  Y <=      (A xor B) xor C;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity xnor3 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end xnor3;

architecture simple of xnor3 is
begin
  Y <=  not ((A xor B) xor C);
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity zor3 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end zor3;

architecture simple of zor3 is
begin
  Y <=      (A and B and C) or ((not A) and (not B) and (not C));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity zor3i is
  generic( exclude: std_logic_vector := "");
  port( A, B, C : in std_logic;
              Y : out std_logic);
end zor3i;

architecture simple of zor3i is
begin
  Y <= not ((A and B and C) or ((not A) and (not B) and (not C)));
end simple;

--------------------------------------------------

-- 4-inputs gates :

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity aoi211 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C, D : in std_logic;
                 Y : out std_logic);
end aoi211;

architecture simple of aoi211 is
begin
  Y <= not ((A and B) or   C  or D );
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity aoi22 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C, D : in std_logic;
                 Y : out std_logic);
end aoi22;

architecture simple of aoi22 is
begin
  Y <= not ((A and B) or  (C and D));
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity oai211 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C, D : in std_logic;
                 Y : out std_logic);
end oai211;

architecture simple of oai211 is
begin
  Y <= not ((A or  B) and  C and D );
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity oai22 is
  generic( exclude: std_logic_vector := "");
  port( A, B, C, D : in std_logic;
                 Y : out std_logic);
end oai22;

architecture simple of oai22 is
begin
  Y <= not ((A or  B) and (C  or D));
end simple;

--------------------------------------------------

-- Sequential gates :

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK           : in std_logic;
        Q : out std_logic);
end DFN0;

architecture simple of DFN0 is
begin
  process(CLK)
  begin
    if falling_edge(CLK) then
       Q <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK           : in std_logic;
        Q : out std_logic);
end DFN1;

architecture simple of DFN1 is
begin
  process(CLK)
  begin
    if rising_edge(CLK) then
       Q <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK           : in std_logic;
       QN : out std_logic);
end DFI0;

architecture simple of DFI0 is
begin
  process(CLK)
  begin
    if falling_edge(CLK) then
      QN <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK           : in std_logic;
       QN : out std_logic);
end DFI1;

architecture simple of DFI1 is
begin
  process(CLK)
  begin
    if rising_edge(CLK) then
      QN <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0C0 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN0C0;

architecture simple of DFN0C0 is
begin
  process(CLK, CLR)
  begin
    if CLR='0' then
       Q <= '0';
    else
      if falling_edge(CLK) then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1C0 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN1C0;

architecture simple of DFN1C0 is
begin
  process(CLK, CLR)
  begin
    if CLR='0' then
       Q <= '0';
    else
      if rising_edge(CLK) then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0C0 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI0C0;

architecture simple of DFI0C0 is
begin
  process(CLK, CLR)
  begin
    if CLR='0' then
      QN <= '0';
    else
      if falling_edge(CLK) then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1C0 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI1C0;

architecture simple of DFI1C0 is
begin
  process(CLK, CLR)
  begin
    if CLR='0' then
      QN <= '0';
    else
      if rising_edge(CLK) then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN0C1;

architecture simple of DFN0C1 is
begin
  process(CLK, CLR)
  begin
    if CLR='1' then
       Q <= '0';
    else
      if falling_edge(CLK) then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN1C1;

architecture simple of DFN1C1 is
begin
  process(CLK, CLR)
  begin
    if CLR='1' then
       Q <= '0';
    else
      if rising_edge(CLK) then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI0C1;

architecture simple of DFI0C1 is
begin
  process(CLK, CLR)
  begin
    if CLR='1' then
      QN <= '0';
    else
      if falling_edge(CLK) then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI1C1;

architecture simple of DFI1C1 is
begin
  process(CLK, CLR)
  begin
    if CLR='1' then
      QN <= '0';
    else
      if rising_edge(CLK) then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0E0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
        Q : out std_logic);
end DFN0E0;

architecture simple of DFN0E0 is
begin
  process(CLK, E)
  begin
    if falling_edge(CLK) and E='0' then
       Q <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1E0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
        Q : out std_logic);
end DFN1E0;

architecture simple of DFN1E0 is
begin
  process(CLK, E)
  begin
    if rising_edge(CLK) and E='0' then
       Q <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0E0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
       QN : out std_logic);
end DFI0E0;

architecture simple of DFI0E0 is
begin
  process(CLK, E)
  begin
    if falling_edge(CLK) and E='0' then
      QN <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1E0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
       QN : out std_logic);
end DFI1E0;

architecture simple of DFI1E0 is
begin
  process(CLK, E)
  begin
    if rising_edge(CLK) and E='0' then
      QN <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0E0C0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN0E0C0;

architecture simple of DFN0E0C0 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='0' then
       Q <= '0';
    else
      if falling_edge(CLK) and E='0' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1E0C0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN1E0C0;

architecture simple of DFN1E0C0 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='0' then
       Q <= '0';
    else
      if rising_edge(CLK) and E='0' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0E0C0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI0E0C0;

architecture simple of DFI0E0C0 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='0' then
      QN <= '0';
    else
      if falling_edge(CLK) and E='0' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1E0C0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI1E0C0;

architecture simple of DFI1E0C0 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='0' then
      QN <= '0';
    else
      if rising_edge(CLK) and E='0' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0E0C1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN0E0C1;

architecture simple of DFN0E0C1 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='1' then
       Q <= '0';
    else
      if falling_edge(CLK) and E='0' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1E0C1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN1E0C1;

architecture simple of DFN1E0C1 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='1' then
       Q <= '0';
    else
      if rising_edge(CLK) and E='0' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0E0C1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI0E0C1;

architecture simple of DFI0E0C1 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='1' then
      QN <= '0';
    else
      if falling_edge(CLK) and E='0' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1E0C1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI1E0C1;

architecture simple of DFI1E0C1 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='1' then
      QN <= '0';
    else
      if rising_edge(CLK) and E='0' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0E0P0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN0E0P0;

architecture simple of DFN0E0P0 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='0' then
       Q <= '1';
    else
      if falling_edge(CLK) and E='0' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1E0P0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN1E0P0;

architecture simple of DFN1E0P0 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='0' then
       Q <= '1';
    else
      if rising_edge(CLK) and E='0' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0E0P0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI0E0P0;

architecture simple of DFI0E0P0 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='0' then
      QN <= '1';
    else
      if falling_edge(CLK) and E='0' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1E0P0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI1E0P0;

architecture simple of DFI1E0P0 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='0' then
      QN <= '1';
    else
      if rising_edge(CLK) and E='0' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0E0P1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN0E0P1;

architecture simple of DFN0E0P1 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='1' then
       Q <= '1';
    else
      if falling_edge(CLK) and E='0' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1E0P1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN1E0P1;

architecture simple of DFN1E0P1 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='1' then
       Q <= '1';
    else
      if rising_edge(CLK) and E='0' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0E0P1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI0E0P1;

architecture simple of DFI0E0P1 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='1' then
      QN <= '1';
    else
      if falling_edge(CLK) and E='0' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1E0P1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI1E0P1;

architecture simple of DFI1E0P1 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='1' then
      QN <= '1';
    else
      if rising_edge(CLK) and E='0' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0E1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
        Q : out std_logic);
end DFN0E1;

architecture simple of DFN0E1 is
begin
  process(CLK, E)
  begin
    if falling_edge(CLK) and E='1' then
       Q <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1E1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
        Q : out std_logic);
end DFN1E1;

architecture simple of DFN1E1 is
begin
  process(CLK, E)
  begin
    if rising_edge(CLK) and E='1' then
       Q <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0E1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
       QN : out std_logic);
end DFI0E1;

architecture simple of DFI0E1 is
begin
  process(CLK, E)
  begin
    if falling_edge(CLK) and E='1' then
      QN <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1E1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK           : in std_logic;
       QN : out std_logic);
end DFI1E1;

architecture simple of DFI1E1 is
begin
  process(CLK, E)
  begin
    if rising_edge(CLK) and E='1' then
      QN <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0E1C0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN0E1C0;

architecture simple of DFN0E1C0 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='0' then
       Q <= '0';
    else
      if falling_edge(CLK) and E='1' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1E1C0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN1E1C0;

architecture simple of DFN1E1C0 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='0' then
       Q <= '0';
    else
      if rising_edge(CLK) and E='1' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0E1C0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI0E1C0;

architecture simple of DFI0E1C0 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='0' then
      QN <= '0';
    else
      if falling_edge(CLK) and E='1' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1E1C0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI1E1C0;

architecture simple of DFI1E1C0 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='0' then
      QN <= '0';
    else
      if rising_edge(CLK) and E='1' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0E1C1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN0E1C1;

architecture simple of DFN0E1C1 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='1' then
       Q <= '0';
    else
      if falling_edge(CLK) and E='1' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1E1C1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
        Q : out std_logic);
end DFN1E1C1;

architecture simple of DFN1E1C1 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='1' then
       Q <= '0';
    else
      if rising_edge(CLK) and E='1' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0E1C1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI0E1C1;

architecture simple of DFI0E1C1 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='1' then
      QN <= '0';
    else
      if falling_edge(CLK) and E='1' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1E1C1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK     , CLR : in std_logic;
       QN : out std_logic);
end DFI1E1C1;

architecture simple of DFI1E1C1 is
begin
  process(CLK, E, CLR)
  begin
    if CLR='1' then
      QN <= '0';
    else
      if rising_edge(CLK) and E='1' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0E1P0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN0E1P0;

architecture simple of DFN0E1P0 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='0' then
       Q <= '1';
    else
      if falling_edge(CLK) and E='1' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1E1P0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN1E1P0;

architecture simple of DFN1E1P0 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='0' then
       Q <= '1';
    else
      if rising_edge(CLK) and E='1' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0E1P0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI0E1P0;

architecture simple of DFI0E1P0 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='0' then
      QN <= '1';
    else
      if falling_edge(CLK) and E='1' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1E1P0 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI1E1P0;

architecture simple of DFI1E1P0 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='0' then
      QN <= '1';
    else
      if rising_edge(CLK) and E='1' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0E1P1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN0E1P1;

architecture simple of DFN0E1P1 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='1' then
       Q <= '1';
    else
      if falling_edge(CLK) and E='1' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1E1P1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN1E1P1;

architecture simple of DFN1E1P1 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='1' then
       Q <= '1';
    else
      if rising_edge(CLK) and E='1' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0E1P1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI0E1P1;

architecture simple of DFI0E1P1 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='1' then
      QN <= '1';
    else
      if falling_edge(CLK) and E='1' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1E1P1 is
  generic( exclude: std_logic_vector := "");
  port(D, E, CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI1E1P1;

architecture simple of DFI1E1P1 is
begin
  process(CLK, E, PRE)
  begin
    if PRE='1' then
      QN <= '1';
    else
      if rising_edge(CLK) and E='1' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0P0 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN0P0;

architecture simple of DFN0P0 is
begin
  process(CLK, PRE)
  begin
    if PRE='0' then
       Q <= '1';
    else
      if falling_edge(CLK) then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1P0 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN1P0;

architecture simple of DFN1P0 is
begin
  process(CLK, PRE)
  begin
    if PRE='0' then
       Q <= '1';
    else
      if rising_edge(CLK) then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0P0 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI0P0;

architecture simple of DFI0P0 is
begin
  process(CLK, PRE)
  begin
    if PRE='0' then
      QN <= '1';
    else
      if falling_edge(CLK) then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1P0 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI1P0;

architecture simple of DFI1P0 is
begin
  process(CLK, PRE)
  begin
    if PRE='0' then
      QN <= '1';
    else
      if rising_edge(CLK) then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0P1 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN0P1;

architecture simple of DFN0P1 is
begin
  process(CLK, PRE)
  begin
    if PRE='1' then
       Q <= '1';
    else
      if falling_edge(CLK) then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1P1 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
        Q : out std_logic);
end DFN1P1;

architecture simple of DFN1P1 is
begin
  process(CLK, PRE)
  begin
    if PRE='1' then
       Q <= '1';
    else
      if rising_edge(CLK) then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0P1 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI0P1;

architecture simple of DFI0P1 is
begin
  process(CLK, PRE)
  begin
    if PRE='1' then
      QN <= '1';
    else
      if falling_edge(CLK) then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1P1 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK, PRE      : in std_logic;
       QN : out std_logic);
end DFI1P1;

architecture simple of DFI1P1 is
begin
  process(CLK, PRE)
  begin
    if PRE='1' then
      QN <= '1';
    else
      if rising_edge(CLK) then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN0P1C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK, PRE, CLR : in std_logic;
        Q : out std_logic);
end DFN0P1C1;

architecture simple of DFN0P1C1 is
begin
  process(CLK, PRE, CLR)
  begin
    if CLR='1' then
       Q <= '0';
    else
      if PRE='1' then
         Q <= '1';
      else
        if falling_edge(CLK) then
           Q <= D;
        end if;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFN1P1C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK, PRE, CLR : in std_logic;
        Q : out std_logic);
end DFN1P1C1;

architecture simple of DFN1P1C1 is
begin
  process(CLK, PRE, CLR)
  begin
    if CLR='1' then
       Q <= '0';
    else
      if PRE='1' then
         Q <= '1';
      else
        if rising_edge(CLK) then
           Q <= D;
        end if;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI0P1C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK, PRE, CLR : in std_logic;
       QN : out std_logic);
end DFI0P1C1;

architecture simple of DFI0P1C1 is
begin
  process(CLK, PRE, CLR)
  begin
    if CLR='1' then
      QN <= '0';
    else
      if PRE='1' then
        QN <= '1';
      else
        if falling_edge(CLK) then
          QN <= D;
        end if;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DFI1P1C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    CLK, PRE, CLR : in std_logic;
       QN : out std_logic);
end DFI1P1C1;

architecture simple of DFI1P1C1 is
begin
  process(CLK, PRE, CLR)
  begin
    if CLR='1' then
      QN <= '0';
    else
      if PRE='1' then
        QN <= '1';
      else
        if rising_edge(CLK) then
          QN <= D;
        end if;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLI0 is
  generic( exclude: std_logic_vector := "");
  port(D,    G             : in std_logic;
       QN : out std_logic);
end DLI0;

architecture simple of DLI0 is
begin
  process(G  , D)
  begin
    if G='0' then
      QN <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLI1 is
  generic( exclude: std_logic_vector := "");
  port(D,    G             : in std_logic;
       QN : out std_logic);
end DLI1;

architecture simple of DLI1 is
begin
  process(G  , D)
  begin
    if G='1' then
      QN <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLN0 is
  generic( exclude: std_logic_vector := "");
  port(D,    G             : in std_logic;
        Q : out std_logic);
end DLN0;

architecture simple of DLN0 is
begin
  process(G  , D)
  begin
    if G='0' then
       Q <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLN1 is
  generic( exclude: std_logic_vector := "");
  port(D,    G             : in std_logic;
        Q : out std_logic);
end DLN1;

architecture simple of DLN1 is
begin
  process(G  , D)
  begin
    if G='1' then
       Q <= D;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLI0C0 is
  generic( exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
       QN : out std_logic);
end DLI0C0;

architecture simple of DLI0C0 is
begin
  process(G  , D, CLR)
  begin
    if CLR='0' then
      QN <= '0';
    else
      if G='0' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLI1C0 is
  generic( exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
       QN : out std_logic);
end DLI1C0;

architecture simple of DLI1C0 is
begin
  process(G  , D, CLR)
  begin
    if CLR='0' then
      QN <= '0';
    else
      if G='1' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLN0C0 is
  generic( exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
        Q : out std_logic);
end DLN0C0;

architecture simple of DLN0C0 is
begin
  process(G  , D, CLR)
  begin
    if CLR='0' then
       Q <= '0';
    else
      if G='0' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLN1C0 is
  generic( exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
        Q : out std_logic);
end DLN1C0;

architecture simple of DLN1C0 is
begin
  process(G  , D, CLR)
  begin
    if CLR='0' then
       Q <= '0';
    else
      if G='1' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLI0C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
       QN : out std_logic);
end DLI0C1;

architecture simple of DLI0C1 is
begin
  process(G  , D, CLR)
  begin
    if CLR='1' then
      QN <= '0';
    else
      if G='0' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLI1C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
       QN : out std_logic);
end DLI1C1;

architecture simple of DLI1C1 is
begin
  process(G  , D, CLR)
  begin
    if CLR='1' then
      QN <= '0';
    else
      if G='1' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLN0C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
        Q : out std_logic);
end DLN0C1;

architecture simple of DLN0C1 is
begin
  process(G  , D, CLR)
  begin
    if CLR='1' then
       Q <= '0';
    else
      if G='0' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLN1C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    G       , CLR : in std_logic;
        Q : out std_logic);
end DLN1C1;

architecture simple of DLN1C1 is
begin
  process(G  , D, CLR)
  begin
    if CLR='1' then
       Q <= '0';
    else
      if G='1' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLI0P0 is
  generic( exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
       QN : out std_logic);
end DLI0P0;

architecture simple of DLI0P0 is
begin
  process(G  , D, PRE)
  begin
    if PRE='0' then
      QN <= '1';
    else
      if G='0' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLI1P0 is
  generic( exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
       QN : out std_logic);
end DLI1P0;

architecture simple of DLI1P0 is
begin
  process(G  , D, PRE)
  begin
    if PRE='0' then
      QN <= '1';
    else
      if G='1' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLN0P0 is
  generic( exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
        Q : out std_logic);
end DLN0P0;

architecture simple of DLN0P0 is
begin
  process(G  , D, PRE)
  begin
    if PRE='0' then
       Q <= '1';
    else
      if G='0' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLN1P0 is
  generic( exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
        Q : out std_logic);
end DLN1P0;

architecture simple of DLN1P0 is
begin
  process(G  , D, PRE)
  begin
    if PRE='0' then
       Q <= '1';
    else
      if G='1' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLI0P1 is
  generic( exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
       QN : out std_logic);
end DLI0P1;

architecture simple of DLI0P1 is
begin
  process(G  , D, PRE)
  begin
    if PRE='1' then
      QN <= '1';
    else
      if G='0' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLI1P1 is
  generic( exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
       QN : out std_logic);
end DLI1P1;

architecture simple of DLI1P1 is
begin
  process(G  , D, PRE)
  begin
    if PRE='1' then
      QN <= '1';
    else
      if G='1' then
        QN <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLN0P1 is
  generic( exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
        Q : out std_logic);
end DLN0P1;

architecture simple of DLN0P1 is
begin
  process(G  , D, PRE)
  begin
    if PRE='1' then
       Q <= '1';
    else
      if G='0' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLN1P1 is
  generic( exclude: std_logic_vector := "");
  port(D,    G  , PRE      : in std_logic;
        Q : out std_logic);
end DLN1P1;

architecture simple of DLN1P1 is
begin
  process(G  , D, PRE)
  begin
    if PRE='1' then
       Q <= '1';
    else
      if G='1' then
         Q <= D;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLI0P1C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    G  , PRE, CLR : in std_logic;
       QN : out std_logic);
end DLI0P1C1;

architecture simple of DLI0P1C1 is
begin
  process(G  , D, PRE, CLR)
  begin
    if CLR='1' then
      QN <= '0';
    else
      if PRE='1' then
        QN <= '1';
      else
        if G='0' then
          QN <= D;
        end if;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLI1P1C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    G  , PRE, CLR : in std_logic;
       QN : out std_logic);
end DLI1P1C1;

architecture simple of DLI1P1C1 is
begin
  process(G  , D, PRE, CLR)
  begin
    if CLR='1' then
      QN <= '0';
    else
      if PRE='1' then
        QN <= '1';
      else
        if G='1' then
          QN <= D;
        end if;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLN0P1C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    G  , PRE, CLR : in std_logic;
        Q : out std_logic);
end DLN0P1C1;

architecture simple of DLN0P1C1 is
begin
  process(G  , D, PRE, CLR)
  begin
    if CLR='1' then
       Q <= '0';
    else
      if PRE='1' then
         Q <= '1';
      else
        if G='0' then
           Q <= D;
        end if;
      end if;
    end if;
  end process;
end simple;

--------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;

entity DLN1P1C1 is
  generic( exclude: std_logic_vector := "");
  port(D,    G  , PRE, CLR : in std_logic;
        Q : out std_logic);
end DLN1P1C1;

architecture simple of DLN1P1C1 is
begin
  process(G  , D, PRE, CLR)
  begin
    if CLR='1' then
       Q <= '0';
    else
      if PRE='1' then
         Q <= '1';
      else
        if G='1' then
           Q <= D;
        end if;
      end if;
    end if;
  end process;
end simple;
