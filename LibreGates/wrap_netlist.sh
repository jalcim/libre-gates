#!/bin/bash

# LibreGates/wrap_netlist.sh
# created sam. sept. 12 09:29:47 CEST 2020
# version mar. sept. 15 05:59:38 CEST 2020 : forked from wrap.sh to become wrap_netlist.sh
# version lun. sept. 21 07:19:31 CEST 2020 : don't call update_select_gate directly.
# version ven. nov.   6 08:07:22  CET 2020 : now you can define $DRIVE_DUT to call a different test driver
# version ven. nov.  20 19:26:00  CET 2020 : tries to propagate the generics
# version sam. nov. 21 10:45:15 CET 2020 : propagates integers and strings generics
# version sam. mai 27 05:00:09 CEST 2023 : slightly more foolproof.
# version sam. 27 mai 2023 21:31:48 CEST : XML caching solved for ports (not for generics yet)
# version dim. mai 28 04:27:48 CEST 2023 : making GHDL v0.35 work again...
#
# Generates a wrapper from the XML output of GHDL

# the default function name :
[ "$DRIVE_DUT" ] || DRIVE_DUT="Drive_DUT"

################# Some generic function #################

Problem () {
  echo $'\e[91m' $* $'\e[0m' >&2
}

ENTITY=""
TAG_NAME=""
declare -A RefTable

ReadTag () {
  IFS=\> read -d \< ENTITY &&
  TAG_NAME=${ENTITY%% *}   &&
  TAG_NAME=${TAG_NAME//[$'\t\r\n ']}  &&
  ATTRIBUTES=${ENTITY#* }  &&
  ATTRIBUTES=${ATTRIBUTES//[$'\t\r\n']}
#echo "got tag $TAG_NAME = $ATTRIBUTES" >&2
}

Seek_Tag () {
  # $1 is tag to find
  # $2 is tag that marks end of range

  # TAG_NAME could have been set already
  # lack of do..while in bash
  [[ "$TAG_NAME" == "$1" ]] && return 0
  [[ "$TAG_NAME" == "$2" ]] && return 1

  while ReadTag; do
    [[ "$TAG_NAME" == "$1" ]] && return 0
    [[ "$TAG_NAME" == "$2" ]] && return 1
  done
  Problem " Tag $1 not found"
  return 1
}

GetAttribute () {      # key to be found in $1
  LIST=($ATTRIBUTES)   # split string into array
  i=0
  while [[ "${LIST[$i]}" ]]  ; do  # scan the list
    IFS='"' read -a ATTRIBUTE <<< "${LIST[$i]}"  # split the individual parameter in a sub-string
    if [[ "$ATTRIBUTE" == "$1" ]] ; then   # found the key
      echo -n "${ATTRIBUTE[1]}"           # return the value
      break;
    fi
    i=$((i+1))
  done
  return 1 # not found...
}

################# Handle the generics #################

GENERICS=""
GENERIC_MAP=()
GENERIC_DEF=()
GENERIC_NAME=""
GENERIC_TYPE=""
GENERIC_INITVAL=""
DELIMITER=''

RegisterGeneric () {
  GENERIC_MAP+=("$GENERIC_NAME => $GENERIC_NAME")

  GEN="$GENERIC_NAME : $GENERIC_TYPE"
  [ "$GENERIC_INITVAL" ] &&
    GEN=$GEN" := "$DELIMITER$GENERIC_INITVAL$DELIMITER
  GENERIC_DEF+=("$GEN;")
  GENERICS="1"
  GENERIC_NAME=""
  GENERIC_TYPE=""
  GENERIC_INITVAL=""
  DELIMITER=''
}

Process_generic () {
  DEFDEPTH=0
  while ReadTag; do
    [[ "$TAG_NAME" == "/generic_chain>" ]] && break

    # skip all sub-el
    case "$TAG_NAME" in
      "/generic_chain>")
        return 0 ;;
      "el")
        DEFDEPTH=$((DEFDEPTH+1)) ;;
      "/el>")
        [[ "$DEFDEPTH" == "1" ]] && RegisterGeneric
        DEFDEPTH=$((DEFDEPTH-1)) ;;
    esac

    # is it the root tag ?
    if [[ "$DEFDEPTH" == "1" ]] ; then
      case "$TAG_NAME" in
        "el")  GENERIC_NAME=$(GetAttribute 'identifier=') ;;
        "subtype_indication")
          GENERIC_TYPE=$(GetAttribute 'identifier=') ;;
        "default_value")
          GENERIC_KIND=$(GetAttribute 'kind=')
          # some types require more processing,
          # the contents is in another tag when value= is missing
          case "$GENERIC_KIND" in
            "integer_literal")
              GENERIC_INITVAL=$(GetAttribute "value=")
            ;;
            "string_literal8")
              Seek_Tag "string8_id" "/el>"
              GENERIC_INITVAL=$(GetAttribute "content=")
              DELIMITER='"'
            ;;
            *)
              Problem "Can't handle this type yet : $GENERIC_NAME : $GENERIC_TYPE (kind='$GENERIC_KIND')"
              return
            ;;
          esac
#          echo "got init val= $GENERIC_INITVAL"
        ;;
      esac
    fi # root tag
  done
}

################# Handle the ports #################

PORT_TYPE=""
PORTNAME=""
INOUT=""
LISTMAP=()
INDEX_IN=0
INDEX_OUT=0
CLK_USED="'X'" #this value means unused

RegisterPort () {
  if [[ "$PORT_TYPE" != "" ]] ; then
#   echo " - port $PORTNAME is $INOUT $PORT_TYPE" >&2
    if [[ "$PORT_TYPE" == "sl" ]] ||
       [[ "$PORT_TYPE" == "std_logic" ]] ; then
      SIZE=1
    else
      if [[ "$PORT_TYPE" == slv* ]] ; then
        SIZE=${PORT_TYPE#slv}
      else
        SIZE=0
        Problem "Error: $PORT_TYPE not supported"
      fi
    fi

    # apply to the appropriate vector
    case "$INOUT" in
      "in")
        if [[ "$PORTNAME" == "clk" ]] ; then
          if [[ "$PORT_TYPE" == "sl" ]] ||
             [[ "$PORT_TYPE" == "std_logic" ]] ; then
            LISTMAP+=('clk => VectClk')
            CLK_USED="'U'"
          else
            Problem "Invalid clock signal type : $PORT_TYPE"
          fi
        else
          RANGE=$INDEX_IN
          if [ "$SIZE" -gt 1 ]; then
            RANGE="$(( INDEX_IN - 1 + SIZE )) downto $INDEX_IN"
          fi;
          LISTMAP+=("$PORTNAME => VectIn($RANGE)")
          INDEX_IN=$(( INDEX_IN + SIZE ))
        fi
        ;;
      "out")
        RANGE=$INDEX_OUT
        if [ "$SIZE" -gt 1 ]; then
          RANGE="$(( INDEX_OUT - 1 + SIZE )) downto $INDEX_OUT"
        fi;
        LISTMAP+=("$PORTNAME => VectOut($RANGE)")
        INDEX_OUT=$(( INDEX_OUT + SIZE ))
        ;;
      *) Problem "Invalid port direction for signal $PORTNAME"
    esac
  fi

#reinit for later
  PORT_TYPE=""
  PORTNAME=""
  INOUT=""
}

Process_ports () {
  while ReadTag; do
    if [[ "$TAG_NAME" == "/port_chain>" ]] ; then
      break
    else
      if [[ "$TAG_NAME" == "/el>" ]] ; then
        RegisterPort
      else
        if [[ "$TAG_NAME" == "el" ]] ; then
          INOUT=$(GetAttribute 'mode=')
          PORTNAME=$(GetAttribute 'identifier=')
        else
          if [[ "$TAG_NAME" == "subtype_indication" ]] ; then
            PORT_TYPE=$(GetAttribute 'identifier=')
          else
            if [[ "$TAG_NAME" == "type" ]] ; then
              PORT_REF=$(GetAttribute 'ref=')
              if [[ "$PORT_TYPE" != "" ]] ; then # type already defined for this element,
                RefTable[$PORT_REF]=$PORT_TYPE    # register the type (may happen 2x)
              else   # lookup type
                PORT_TYPE=${RefTable[$PORT_REF]}
              fi
            fi
          fi
        fi
      fi
    fi
  done
}

################# The main processing routine #################

# Get the top level entity:
ghdl -a $* &&
TOP_ENTITY=$(ghdl --find-top $*) &&
rm -f $TOP_ENTITY.xml &&
XML_FILE="$TOP_ENTITY.xml" &&

ghdl --file-to-xml $* |
   sed 's/  */ /g'    |  # remove duplicate spaces
   sed 's/=" /="/g'   > $XML_FILE
   # why is there a leading space inside an attribute ???

if [ ! -s "$XML_FILE" ]; then
  ls -altr $XML_FILE   >&2
  Problem ' /!\ Generated XML file empty ! '
fi

grep -q 'generic_chain>' $XML_FILE && {
    Seek_Tag 'generic_chain>' 'port_chain>' &&
    Process_generic
  } < $XML_FILE  >&2

grep -q 'port_chain>' $XML_FILE && {
    Seek_Tag 'port_chain>' 'last_design_unit>' &&
    Process_ports
  } < $XML_FILE  >&2

################# Finally, display the result #################

if   [ "$INDEX_IN"  -gt 0 ]; then
  if [ "$INDEX_OUT" -gt 0 ]; then

cat <<ENDOFHEREDOC
-- Wrapper.vhdl
-- Generated on $(date +%Y%m%d-%R)
-- Do not modify

Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;
    use LibreGates.Netlist_lib.all;

entity Wrapper is
  generic (
ENDOFHEREDOC

if [[ "$GENERICS" ]] ; then
  for i  in "${!GENERIC_MAP[@]}"; do
    echo "    ${GENERIC_DEF[$i]}"
  done
fi

cat <<ENDOFHEREDOC
    WrapperWidthIn : integer := $INDEX_IN;
    WrapperWidthOut: integer := $INDEX_OUT;
    listgates: integer := 1;  -- set to 0 to display the raw list of gates
    main_delay : time := 1 sec;
    filename : string := ""; -- file of exclude vectors
    verbose : string := ""
  );
end Wrapper;

architecture Wrap of Wrapper is
  signal VectIn : std_logic_vector(WrapperWidthIn -1 downto 0);
  signal VectOut: std_logic_vector(WrapperWidthOut-1 downto 0);
  signal VectClk: std_logic := $CLK_USED;

begin

  -- force the registration of the gates.
  -- must be placed at the very beginning
  rg: entity register_generics generic map(
         gate_select_number => listgates, -- show gate listing when < 1
         bit_select_number => -1, -- no alteration
         flip_LUT => 2,           -- probe mode
         filename => filename,    -- eventual exclusions
         verbose => verbose)
  port map(clock => '0');   -- the updates are directly driven by $DRIVE_DUT
  -- note : the corresponding procedure is not called
  -- at the right time if it is called directly,
  -- the entity ensures the correct calling order.

  $DRIVE_DUT(VectIn, VectOut, VectClk, main_delay);

  -- Finally we "wire" the unit to the ports:
  tb: entity $TOP_ENTITY
ENDOFHEREDOC

if [[ "$GENERICS" ]] ; then
  echo "    generic map("
  COMA=""
  for i in "${!GENERIC_MAP[@]}"; do
    [[ "$COMA" ]] && echo $COMA; COMA=","
    echo -n "      ${GENERIC_MAP[$i]}"
  done
  echo ")"
fi

echo "    port map("
COMA=""
for i in "${!LISTMAP[@]}"; do
  [[ "$COMA" ]] && echo $COMA; COMA=","
  echo -n "      ${LISTMAP[$i]}"
done

echo ");"
echo "end Wrap;"
  else
    Problem "Error: no output found"
  fi
else
  Problem "Error: no input found"
fi

########## The end. ##########
