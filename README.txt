miroring of https://hackaday.io/project/174585-libre-gates
updated 16/09/2023

file README.txt
created sam. août 10 16:03:22 CEST 2019
version dim. août 11 17:36:51 CEST 2019
version mar. août 13 23:14:14 CEST 2019
version mar. nov. 19 07:57:42 CET 2019 : release 2.2
version jeu. nov. 21 05:44:16 CET 2019 : 2.3 (no split arch)
version dim. déc. 29 19:37:16 CET 2019 : last release of 2019 : v2.6
version lun. mars 23 03:07:37 CET 2020 : "portable" dumb version
version mar. juil. 14 21:05:54 CEST 2020 : v2.9, adding some 4-inputs gates
version mar. sept.  1 05:28:35 CEST 2020 : LibreGates it is !

Install :

Copy the source's directory to your project directory (or use a symbolic link).
All files should be already present but build.sh can regenerate them,
for example if you need a different version with different timing.
build.sh also contains instructions to compile the files into a valid GHDL package.
The other scripts in the tests/ directory contain examples of command line options
to integrate in your own build/simulation scripts.


WHAT :

This library abstracts and handles logic gates (boolean & sequential)
for the purpose of simulation, fault injection, coverage and ATPG.

Originally, this set of files implements some gates and stuff from the
standard ProASIC3 cell library (Actel/Microsemi/Microchip) in order
to simulate circuits with a standalone tool (such as GHDL).

It's used to check equivalence with files that start with these lines :

 library proasic3;
     use proasic3.all;

It implements a significant subset of the gates described in pa3_libguide_ug.pdf
(all gates with 1, 2 or 3 inputs, and all sequential gates). I avoid technology-specific
modules that can't be ported, such as PLL, I/O pins or proprietary blocks
tied to the chips.

Since the original library is made of proprietary code, I have rewritten
the gates that I use, along with a simple RTL implementation suitable
for cycle-acurate simulations. I added some delay but it is very coarse
because precise timings also depend on routing, which is impossible
to predict at this high level and before place/route.
With 1ns per gate, the estimates should still be in the +/-30% ballpark.

 * The "normal" version implements many features for simulation
 and analysis/exploration of the circuits. This is the real "meat" of
 this library, providing features only found so far in proprietary tools.

* The "simple" version is now provided (>=v2.8, 202003) in the "simple"
 subdirectory to enable other FPGA families to synthesise the source code,
 and for simulators to simulate faster (because no statistics and analysis
 is performed).

You can select the version by changing the path to the proasic3 lib,
depending on the desired depth or speed.


The new version (>= aug. 2019) of the gates library
provides much more insight into the logic's dynamic behaviour.
For example : the vectgen.vhdl program is a generic "driver" that
checks the netlist's correctness and evaluates the latencies.
(It's not able yet to generate test vectors)

Configuration relies a lot on the runtime generics option of the
recent versions of the GHDL simulator, with the -g command line parameter.


WHY :

This new tool enhances the classic ProASIC3 gates library
for design verification and "Design For Test". It will help
build test vectors and prove they can catch all the possible faults.
It also helps ensure that a complex boolean function has
no "blind spot" with unused intermediary combinations.

A non-boolean value can be injected and propagated through
the logic gates, to highlight the "logic cone" of the circuit,
or you can work with 'L' and 'H' to see how they propagate,
which can greatly help during debug/development.


HOW :


* The script /run_tests.sh generates 2 big files with all
the definitions, then runs a few sanity tests
as well as some demonstrations of the use of the
features of this library (it can take a minute or so).


* The program ./pa3_gategen (created from the above VHDL code)
generates the files PA3_components.vhdl
and PA3_definitions.vhdl, with two options :

  -ggate_delay="1 ns" => changes the unit delay of the gates
                         (default : none)

  -ggate_type="simple" => create a portable VHDL definition
                  (good for synthesis outside of A3P world
                         but introspection is not possible)

* Your VHDL source code only needs to include :

Library proasic3;
    use proasic3.all;

(just like before)


* By default, and if you don't use the "simple" gates,
the gates are simulated in "fast" mode with no logging.
You can enable logging and "meta propagation" (the "trace" mode)
by instanciating register_generics (see later). The top level simulation
file must also include these 3 generics :

  generic (
    gate_select_number: integer := 0;
    bit_select_number: integer := -1;
    flip: integer := 0
  );

The default values do nothing, but when they are changed,
the selected gate can be affected/altered.
These values can be changed during elaboration by GHDL's command line,
for example :

 $ ./program -ggate_select_number=42  -gbit_select_number=7

You can even only list all the gates (no run) with this option :

  $ ./program -ggate_select_number=-1


* When you want to collect data, a clock signal is required for sampling
the gates' states (avoiding false results due to transients). The testbench
must provide one :

  signal clock : std_logic := '0';


* The clock and the generics are fed into an entity
that does some of the dirty work behind the scene:

  rg: entity register_generics port map(
       gate_select_number => gate_select_number,
       bit_select_number => bit_select_number,
       flip_LUT => flip,
       clock => clock);

/!\ You MUST write this in front/before including
or instanciating the other gates ! It must be called
first to initialise important values from the generics /!\

When "clock" changes to value '1' then the values of all the detected gates
are sampled and the histogram is updated.

The histogram is shown at the end of the simulation,
when the "clock" signal changes to value 'X'.


* The gates propagate "non-boolean" input values, so a 'U' or 'X'
at an input port will be copied to the output.
These events are "logged" on each clock so one can :
  - know how many "non-boolean" events occured
  - trace the source and/or verify the "logic cone"
    of a boolean circuit.

You can also do the computations with 'L' and 'H',
though there is no distinction between the others ('U', 'X', '-')
on the histogram side. However they are preserved at the output
so several different logic cones can be probed at the same time.

With these informations, it is easier to design BIST and other 
test vectors, and verify their coverage !


* The histogram omits the GND/VCC "gates" though they can be altered.


* At the end of simulation, the output looks like this :

Gates_lib.vhdl:192:9:@420ns:(report note):  h(1) = 4  4  4  4   (0) [2]
Gates_lib.vhdl:192:9:@420ns:(report note):  h(2) = 4  4  4  4   (0) [3]
Gates_lib.vhdl:192:9:@420ns:(report note):  h(4) = 0  4  0  4  0  4  0  4   (0) [11]

- The first number (in h(x)) is the number of the gate. It must be matched
   with the listing that is provided when running with -ggate_select_number=-1
- Then there are 2, 4, 8 or 16 numbers, showing how many times each
    input combination of '0' and '1' is sampled. GND and VCC have no input
    so they are not included in the output.
- 'L' and 'H' have their own histogram (2, 4, 8 or 16 additional numbers).
- The number (in parens) is the number of samples that are non-boolean
   (U, X, W, Z, -, used for the logic cone)
- The last number in [square brackets] is the number of output changes/toggles.

_____________________________________________________________

DONE :
  - D Latches
  - generic gates
  - count the toggles
  - 2^31 simulation cycles maximum expanded to 2^61
  - LUT16 !
  - netlist
  - split the gates machinery from the PA3-specific definitions

TODO :
  - probe/change sequential gates
  - support "semi-sequential gates" (latches, RS FF)
  - probe nets with better algorithm
  - sed scripts to filter the outputs
  - more real-life tests and examples !
  - implement random initial values for latches and flip-flops...
      (or init as "-" and see if they propagate through the logic)

NOFIX :
  - VCC and GND are not well supported in the netlist checker,
         for example, because they have no input.
  - coarse timing (everybody has 1ns latency)
  - I/O gates ( CLKBUF, BIBUF, CLKBIBUF, INBUF, OUTBUF, TRIBUFF),
      RAM and other ProASIC3-specific peripherals (Flash ROM, PLL...)
      are not covered.
