#!/bin/bash
# version mar. sept.  1 02:42:48 CEST 2020
rm -f $(find|grep '[.]o$') $(find|grep '[.]cf$') \
   LibreGates/proasic3/pa3_gategen
pushd tests
rm -rf test*/*.log \
      test*/wrapper \
      test*/*.xml \
      test1_basic/test_gates \
      test2_INC8/inc8 test2_INC8/inc8_tb test2_INC8/version_* \
      test3_Gray/version_* \
      test5_ALU8/alu8 test5_ALU8/alu8_tb \
      test6_bigLFSR/lfsr32x
popd
