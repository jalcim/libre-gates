#!/bin/bash
# LibreGates/run_tests.sh
# created lun. nov. 13 01:55:52 CET 2017 by Yann Guidon (whygee@f-cpu.org)
# version ven. nov. 17 01:11:13 CET 2017
# version mar. nov. 19 04:24:24 CET 2019
# version mer. nov. 27 05:05:21 CET 2019
# version mar. déc.  3 07:37:49 CET 2019 : the whole script takes 72 seconds
# version dim. déc. 29 19:57:02 CET 2019 : release 2.6
# version lun. mars 23 03:19:26 CET 2020 : release 2.8
# version sam. août 29 01:04:54 CEST 2020 : v2.9
# version mer. sept.  2 20:12:12 CEST 2020 : LibreGates
#
# Released under the GNU AGPLv3 license

if ghdl -v |grep "mcode" ; then
  echo "This script does not support mcode, use GHDL with GCC or LLVM backend."
else
  echo
  echo  'This script will run exhaustive self-tests and will'
  echo  'take "a while", depending on your CPU and software.'
  echo  '(about one minute on my i7 PC)'
  echo  'You may interrupt it (CTRL+C) if needed. Please report errors.'
  echo

  DIRLIST="Y8 test1_basic test2_INC8 test3_Gray test4_cornercases test5_ALU8 test6_bigLFSR"

  # compile the PA3 gates library
  echo -n '--> going to ' ; pushd LibreGates
    ./build.sh || DIRLIST=""
  echo -n '<-- back to ' ; popd

  # don't continue if the libs have failed.
  if [ "$DIRLIST" ] ; then
    echo -n '--> going to ' ; pushd tests

    # run this to perform sanity checks with GHDL
    msg=$'\e[32m'"Library is set up and tested."$'\e[0m'
    for DIR in $DIRLIST ; do
      echo -n '--> going to ' ; pushd $DIR
        ./test.sh || { msg=$'\e[33m'" interrupted by fault"$'\e[0m'; break; }
      echo -n '<-- back to ' ; popd
    done
    echo -n '<-- back to ' ; popd
    echo $msg
  fi
fi
