-- YGREC8/YGREC8_def.vhdl
-- created lun. nov. 13 00:47:55 CET 2017 by Yann Guidon (whygee@f-cpu.org)
-- version lun. nov. 13 02:14:43 CET 2017
-- version ven. nov. 17 02:51:23 CET 2017 : polishing the conditions
-- version dim. nov. 19 08:00:34 CET 2017 : conditions corrected
-- version dim. janv. 28 03:13:47 CET 2018 : LDCx, OVL...
-- version mer. déc. 26 06:46:15 CET 2018 : reorder conditions and opcodes
-- version mer. mars 13 14:12:49 CET 2019 : more reordering and redefinitions
-- version mar. déc.  3 06:06:13 CET 2019 : moved things around, added the SLV package
--
-- Released under the GNU AGPLv3 license
--
-- These are the definitions of YGREC8,
-- shared by the core and other files that instatiate it.
-- Borrowed from the microYASEP/yasep_def.vhdl

Library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
Library SLV;
    use SLV.SLV_utils.all;

package ygrec8_def is
-- compact 4 bits version :
  constant Op_AND   : SLV4 := "0000";
  constant Op_OR    : SLV4 := "0001";
  constant Op_XOR   : SLV4 := "0010";
  constant Op_ANDN  : SLV4 := "0011";
  constant Op_CMPU  : SLV4 := "0100";
  constant Op_CMPS  : SLV4 := "0101";
  constant Op_SUB   : SLV4 := "0110";
  constant Op_ADD   : SLV4 := "0111";
  constant Op_SET   : SLV4 := "1000";
  constant Op_CALL  : SLV4 := "1001";
  constant Op_SHIFT : SLV4 := "1010"; -- SH, SA
  constant Op_ROTAT : SLV4 := "1011"; -- RO, RC
  constant Op_LDC   : SLV4 := "1100"; -- LDCL, LDCH
  constant Op_IN    : SLV4 := "1101";
  constant Op_OUT   : SLV4 := "1110";
  constant Op_INV   : SLV4 := "1111";

  type opcode_array_type  is array (0 to 15) of string(4 downto 1);
  constant Opcode_str_array : opcode_array_type := (
    "AND ", "OR  ", "XOR ", "ANDN",
    "CMPU", "CMPS", "SUB ", "ADD ",
    "SET ", "CALL", "SHFT", "ROT ",
    "LDCL", "IN  ", "OUT ", "INV ");

-- complete 5 bits version for the assembler :
  type opcode_names_array_type is array (0 to 18) of string(1 to 4);
  constant OpcodeS_str_array : opcode_names_array_type := (
    "AND ", "OR  ", "XOR ", "ANDN",
    "CMPU", "CMPS", "SUB ", "ADD ",
    "SET ", "CALL",
    "SH  ", "SA  ", "RO  ", "RC  ",
    "LDCL", "LDCH",
    "IN  ", "OUT ", "INV ");

  type opcode_val_array_type is array (0 to 18) of SLV5;
  constant OpcodeS_val_array : opcode_val_array_type := (
   "0000X",  -- AND
   "0001X",  -- OR
   "0010X",  -- XOR
   "0011X",  -- ANDN
   "0100X",  -- CMPU
   "0101X",  -- CMPS
   "0110X",  -- SUB
   "0111X",  -- ADD
   "1000X",  -- SET
   "1001X",  -- CALL
   "10100",  -- SH
   "10101",  -- SA
   "10110",  -- RO
   "10111",  -- RC
   "11000",  -- LDCL
   "11001",  -- LDCH  -- index=15 here
   "1101X",  -- IN
   "1110X",  -- OUT
   "1111X"); -- INV

  -- the same but organised for the disassembler
  type disasm_names_array_type is array (0 to 31) of string(1 to 6);
  -- yes I have run out of ideas to name those structures...
  constant Disasm_str_array : disasm_names_array_type := (
     "AND   ", "AND   ",
     "OR    ", "OR    ",
     "XOR   ", "XOR   ",
     "ANDN  ", "ANDN  ",
     "CMPU  ", "CMPU  ",
     "CMPS  ", "CMPS  ",
     "SUB   ", "SUB   ",
     "ADD   ", "ADD   ",
     "SET   ", "SET   ",
     "CALL  ", "CALL  ",
     "SH   *", "SA   *",
     "RO   *", "RC   *",
     "LDCL *", "LDCH *",
     "IN    ", "IN    ",
     "OUT   ", "OUT   ",
     "INV   ", "INV   ");

  type string_array_type_7 is array (0 to 7) of string(2 downto 1);
  constant Reg_str_array : string_array_type_7 := (
    "D1", "A1", "D2", "A2", "R1", "R2", "R3", "PC" );

  constant Reg_D1 : SLV3 := "000";
  constant Reg_A1 : SLV3 := "001";
  constant Reg_D2 : SLV3 := "010";
  constant Reg_A2 : SLV3 := "011";
  constant Reg_R1 : SLV3 := "100";
  constant Reg_R2 : SLV3 := "101";
  constant Reg_R3 : SLV3 := "110";
  constant Reg_PC : SLV3 := "111";

  type string_array_type_15 is array (0 to 15) of string(4 downto 1);
  constant Cond_str_array : string_array_type_15 := (
    "NEVR", "IFN0", "IFNC", "IFN1", "IFN ", "IFN2", "IFNZ", "IFN3",
    "ALWS", "IF0 ", "IFC ", "IF1 ", "IFP ", "IF2 ", "IFZ ", "IF3 ");

  constant Cond_N  : SLV4 := "0000";
  constant Cond_NC : SLV4 := "0010";
  constant Cond_NS : SLV4 := "0100";
  constant Cond_Z  : SLV4 := "0110";

  constant Cond_A  : SLV4 := "1000";
  constant Cond_C  : SLV4 := "1010";
  constant Cond_S  : SLV4 := "1100";
  constant Cond_NZ : SLV4 := "1110";

  -- note : the last bit is set only for the CND3 variant
  constant Cond_N0 : SLV4 := "0001";
  constant Cond_N1 : SLV4 := "0011";
  constant Cond_N2 : SLV4 := "0101";
  constant Cond_N3 : SLV4 := "0111";

  constant Cond_0  : SLV4 := "1001";
  constant Cond_1  : SLV4 := "1011";
  constant Cond_2  : SLV4 := "1101";
  constant Cond_3  : SLV4 := "1111";

end ygrec8_def;

--package body ygrec8_def is
-- empty
--end ygrec8_def;
