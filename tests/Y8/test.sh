#!/bin/bash
# LibreGates/tests/Y8/test.sh
# version lun. mars 23 03:22:16 CET 2020
# version sam. août 29 01:08:14 CEST 2020

VER="--std=93"
PLIB="-P../../LibreGates"

# some cleanup :
rm -f *.cf *.o &&

# compile the Y8 definition
ghdl -a $VER $PLIB --work=YGREC8 YGREC8_def.vhdl &&

echo $'\e[32m'"YGREC8 Library generation : OK"$'\e[0m'
