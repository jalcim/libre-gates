#!/bin/bash
# file test.sh
# created by Yann Guidon / ygdes.com
# version mar. nov. 19 04:31:45 CET 2019 : fork/split from proasic3v2/build.sh


VER="--std=93"
PA3="-P../../LibreGates -P../../LibreGates/proasic3"

[ -f ../../LibreGates/proasic3/simple/proasic3-obj93.cf ] &&
rm -f work-obj93.cf *.o test_gates &&
ghdl -a $VER $PA3 test_gates.vhdl &&
ghdl -e $VER $PA3 test_gates &&
echo "# Just listing the gates :" &&
./test_gates -ggate_select_number=-1 &&
echo "# Simple test :" &&
./test_gates &&
echo "# Logic cone test :" &&
echo "# the value '-' is propagated to the output" &&
./test_gates -gdisturb=1 &&
echo "# Alteration test :" &&
./test_gates -ggate_select_number=3 -gbit_select_number=0 -gflip=1 &&
echo "# Logic cone tracing test :" &&
./test_gates -ggate_select_number=2 -gbit_select_number=0 &&
echo $'\e[32m'"Test 1 : OK :-)"$'\e[0m'
