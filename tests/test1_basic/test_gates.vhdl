-- file test_gates.vhdl
-- created ven. août  9 03:14:28 CEST 2019
-- version sam. août 10 16:08:18 CEST 2019
-- version dim. août 11 17:36:51 CEST 2019
-- version mar. août 13 18:05:15 CEST 2019
-- version jeu. nov. 21 06:43:35 CET 2019
-- version mar. sept.  1 06:46:23 CEST 2020 : LibreGates !
--
-- Released under the GNU AGPLv3 license
--
-- This is a simple/small example that shows how to use
-- the extra features of the rewritten proasic3 library.

Library ieee;
    use ieee.std_logic_1164.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;
Library proasic3;
    use proasic3.all;

entity test_gates is
  generic (
    -- for the A3P library :
    gate_select_number: integer := 0;  -- when >= 1, select a gate to alter
    bit_select_number: integer := -1;  -- select which input bit code to alter
    flip: integer := 0;     -- set to 1 to alter the LUT and flip the given bit
    -- this testbench :
    disturb: integer := 0
  );
end test_gates;

architecture rtl of test_gates is
  signal A, B, C, D, E, F, G, H, Y : std_logic;
  signal clock : std_logic := '0';
begin
  -- this entity MUST be written first,
  -- at the very beginning of the toplevel file:
  rg: entity register_generics generic map(
       gate_select_number => gate_select_number,
       bit_select_number => bit_select_number,
       flip_LUT => flip)
      port map(clock => clock);

  -- our lousy "Design Under Test" :
  x1: entity BUFF  port map(A => A,             Y=>E );
  x2: entity xor2  port map(A => B, B=>E,       Y=>F );
  x3: entity xor2  port map(A => C, B=>F,       Y=>G );
  x4: entity VCC   port map(                    Y=>H );
  x5: entity xor3  generic map(exclude=> "X-X-X-X-")
                   port map(A => D, B=>G, C=>H, Y=>Y );

  test: process
    variable i : integer := 0;
  begin
    if gate_select_number=-1 then
      wait;
    end if;

    wait for 100 ns;

    if disturb /= 0 then
      A <= 'L';
    else
      A <= '0';
    end if;
    loop

      B <= '0';
      loop

	C <= '0';
        loop

          D <= '0';
          loop

            i := i+1;
            if i = 9 and disturb /= 0 then
              B <= '-';
            end if;

            clock <= '0';
            wait for 10 ns;
            clock <= '1';
            wait for 10 ns;
            report  " A=" & std_logic'image(A) &
                    " B=" & std_logic'image(B) &
                    " C=" & std_logic'image(C) &
                    " D=" & std_logic'image(D) &
                    " E=" & std_logic'image(E) &
                    " F=" & std_logic'image(F) &
                    " G=" & std_logic'image(G) &
                    " H=" & std_logic'image(H) &
                    " Y=" & std_logic'image(Y);

            exit when D = '1';
            D <= '1';
          end loop;

          exit when C = '1';
          C <= '1';
        end loop;

        exit when B = '1';
        B <= '1';
      end loop;

      exit when A = '1';
      A <= '1';
    end loop;

    clock<='X'; -- display the histogram
    wait;
  end process;

end rtl;
