-- LibreGates/tests/test2_INC8/INC8.decl.vhdl
-- created sam. nov. 11 18:50:10 CET 2017 by Yann Guidon (whygee@f-cpu.org)
-- version jeu. nov. 16 02:42:00 CET 2017 : uses SLV shorthands from YGREC8/YGREC8_def.vhdl
-- version dim. nov.  8 03:28:02 CET 2020 : fork from INC8.vhdl
-- Released under the GNU AGPLv3 license
--
-- Declaration for the increment unit of YGREC8.

Library ieee;
    use ieee.std_logic_1164.all;
Library SLV;
    use SLV.SLV_utils.all;

entity INC8 is
  port(
    A : in  SLV8;
    Y : out SLV8;
    V : out SL);
end INC8;
