-- LibreGates/tests/test2_INC8/INC8.vhdl
-- created sam. nov. 11 18:50:10 CET 2017 by Yann Guidon (whygee@f-cpu.org)
-- version jeu. nov. 16 02:42:00 CET 2017 : uses SLV shorthands from YGREC8/YGREC8_def.vhdl
-- version dim. nov.  8 03:38:14 CET 2020 : moved the declaration in a dedicated file.
--
-- Released under the GNU AGPLv3 license
--
-- This is the increment unit of YGREC8,
-- used to increment the PC.
-- This dumb version uses the built-in + operator

Library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
Library SLV;
    use SLV.SLV_utils.all;

architecture classic of INC8 is
  signal sumAux : unsigned(8 downto 0);
begin
  sumAux <= unsigned('0' & A) + 1;
  Y <= std_logic_vector(sumAux(7 downto 0));
  V <= std_logic(sumAux(8));
end classic;
