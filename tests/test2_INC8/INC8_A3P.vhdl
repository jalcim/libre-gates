-- LibreGates/tests/test2_INC8/INC8_A3P.vhdl.vhdl
-- created sam. nov. 11 18:50:10 CET 2017 by Yann Guidon (whygee@f-cpu.org)
-- version sam. nov. 11 19:17:40 CET 2017
-- version dim. nov. 12 00:07:51 CET 2017
-- version jeu. nov. 16 02:42:00 CET 2017 : uses SLV shorthands from YGREC8/YGREC8_def.vhdl
-- version dim. nov.  8 03:38:14 CET 2020 : moved the declaration in a dedicated file.
-- Released under the GNU AGPLv3 license
--
-- This is the increment unit of YGREC8, used to increment the PC.
-- This version uses 3-input gates from Actel/Microsemi's A3P FPGA family.

Library ieee;
    use ieee.std_logic_1164.all;
Library SLV;
    use SLV.SLV_utils.all;

architecture A3P of INC8 is
  Signal A012, A34, A345, A3456 : SL;
begin
  -- Layer 1
  Y(0) <=      not A(0);
  Y(1) <= A(1) xor A(0);
  Y(2) <= A(2) xor (A(1) and A(0));

  A012 <= A(2) and A(1) and A(0); -- FO7
  A34  <= A(3) and A(4);          -- F02
  A345 <= A(3) and A(4) and A(5); -- FO1

  -- Layer 2
  Y(3) <= A(3) xor A012;
  Y(4) <= A(4) xor (A(3) and A012);
  Y(5) <= A(5) xor (A012 and A34);
  Y(6) <= A(6) xor (A012 and A345);

  A3456 <= A34  and A(5) and A(6); -- FO2

  -- layer 3:
  Y(7) <= A(7) xor (A012 and A3456);
  V    <= A(7) and  A012 and A3456;
end A3P;
