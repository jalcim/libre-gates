-- LibreGates/tests/test2_INC8/INC8_ASIC.vhdl
-- created sam. nov. 11 18:50:10 2017 by Yann Guidon (whygee@f-cpu.org)
-- version sam. nov. 11 19:17:40 2017
-- version dim. nov. 12 00:07:51 2017
-- version jeu. nov. 16 02:42:00 2017 : uses SLV shorthands from YGREC8/YGREC8_def.vhdl
-- version mer. oct. 17 03:30:21 2018 : reorganised in a "single column"
-- version mer. oct. 17 05:06:32 2018 : PA3
-- version ven. jan.  4 05:40:03 2019 : AX1->AX1C
-- version mer. jul. 15 23:10:38 2020 : from tile to ASIC version
--     see https://hackaday.io/project/27280-ygrec8/log/180828-inc8-asicified
-- version dim. nov.  8 03:38:14 CET 2020 : moved the declaration in a dedicated file.
--
-- Released under the GNU AGPLv3 license
--
-- This is the increment unit of YGREC8, used to increment the PC.
-- This version uses 3-input gates from Actel/Microsemi's A3P FPGA family.
-- It tries to be suitable for manual placement, all the "tiles" are organised
-- in a single column. e_R5C might require a little effort but it's still
-- very close to the "tile" implementation.

Library ieee;
    use ieee.std_logic_1164.all;
Library SLV;
    use SLV.SLV_utils.all;
Library proasic3;
    use proasic3.all;

architecture ASIC of INC8 is
  Signal Ab : SLV8;
  Signal A012, A012a, A012b, A34, A345, A3456 : SL;
begin

  -- a first column of inverters / x4 buffers
  buffs: for i in A'range generate
    x4: entity iv1v0x4 port map(A=> A(i), Y=>Ab(i));
  end generate buffs;

  -- Row 0 :
  Y(0) <= Ab(0);
  -- Row 1
  e_R1B: entity XOR2    port map(A=>Ab(0), B=>Ab(1),           Y=> Y(1));
  -- Row 2
  e_R2A: entity NOR3    port map(A=>Ab(0), B=>Ab(1), C=>Ab(2), Y=> A012); -- x2 or x4
  e_R2B: entity AX1D    port map(A=>Ab(0), B=>Ab(1), C=>Ab(2), Y=> Y(2));
  -- Row 3
  e_R3A: entity iv1v0x4 port map(A=> A012,                     Y=>A012a); -- x4
  e_R3B: entity XOR2    port map(A=>Ab(3), B=>A012a,           Y=> Y(3));
  -- Row 4
  e_R4A: entity OR2     port map(A=>Ab(3), B=>Ab(4),           Y=>  A34); -- x2
  e_R4B: entity AX1D    port map(A=>A012a, B=>Ab(3), C=>Ab(4), Y=> Y(4));
  -- Row 5
  e_R5A: entity OR3     port map(A=>Ab(3), B=>Ab(4), C=>Ab(5), Y=> A345); -- x1
  e_R5B: entity AX1D    port map(A=>A012a, B=>  A34, C=>Ab(5), Y=> Y(5));
  e_R5C: entity iv1v0x4 port map(A=> A012,                     Y=>A012b); -- x4
  -- Row 6
  e_R6A: entity OR3     port map(A=>  A34, B=>Ab(5), C=>Ab(6), Y=>A3456); -- x2 or x4
  e_R6B: entity AX1D    port map(A=>A012b, B=> A345, C=>Ab(6), Y=>Y(6));
  -- Row 7
  e_R7A: entity NOR3    port map(A=>A012b, B=>A3456, C=>Ab(7), Y=>   V);
  e_R7B: entity AX1D    port map(A=>A012b, B=>A3456, C=>Ab(7), Y=>Y(7));
end ASIC;
