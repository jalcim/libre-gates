-- LibreGates/tests/test2_INC8/INC8_gates.vhdl
-- created sam. nov. 11 18:50:10 CET 2017 by Yann Guidon (whygee@f-cpu.org)
-- version sam. nov. 11 19:17:40 CET 2017
-- version dim. nov. 12 00:07:51 CET 2017
-- version jeu. nov. 16 02:42:00 CET 2017 : uses SLV shorthands from YGREC8/YGREC8_def.vhdl
-- version mer. oct. 17 03:30:21 CET 2018 : reorganised in a "single column"
-- version dim. nov.  8 03:38:14 CET 2020 : moved the declaration in a dedicated file.
-- Released under the GNU AGPLv3 license
--
-- This is the increment unit of YGREC8, used to increment the PC.
-- This version uses 3-input gates from Actel/Microsemi's A3P FPGA family.
-- This version is derived from INC8_A3P.vhdl but is rearranged to be better
-- suitable for manual placement, all the "tiles" are organised in a single column.
-- See the picture INC8_schematic.png

Library ieee;
    use ieee.std_logic_1164.all;
Library SLV;
    use SLV.SLV_utils.all;

architecture tiles of INC8 is
  Signal A012, A34, A345, A3456 : SL;
begin
  -- Row 0
  Y(0) <=      not A(0);

  -- Row 1
  Y(1) <= A(1) xor A(0);

  -- Row 2
  Y(2) <= A(2) xor (A(1) and A(0));

  -- Row 3
  A012 <= A(2) and  A(1) and A(0); -- FO7
  Y(3) <= A(3) xor A012;

  -- Row 4
  A34  <= A(3) and  A(4);          -- F02
  Y(4) <= A(4) xor (A(3) and A012);

  -- Row 5
  A345 <= A(3) and  A(4) and A(5); -- FO1
  Y(5) <= A(5) xor (A012 and A34);

  -- Row 6
  A3456 <= A34 and  A(5) and A(6); -- FO2
  Y(6) <= A(6) xor (A012 and A345);

  -- Row 7
  Y(7) <= A(7) xor (A012 and A3456);
  V    <= A(7) and  A012 and A3456;
end tiles;
