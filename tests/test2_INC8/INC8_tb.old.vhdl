-- LibreGates/tests/test2_INC8/INC8_tb.vhdl
-- version lun. nov. 13 01:42:14 CET 2017 by Yann Guidon (whygee@f-cpu.org)
-- version mar. oct. 30 07:20:24 CET 2018 : more suitable for individual gate delays
-- Released under the GNU AGPLv3 license

Library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
Library SLV;
    use SLV.SLV_utils.all;

entity INC8_tb is
end INC8_tb;

architecture plip of INC8_tb is
  signal V : SL := '0';
  signal A, Y : SLV8 := "00000000";
begin

  tb: entity INC8 port map (
     A => A,
     Y => Y,
     V => V);

  bench: process
    variable i: integer := 0;
    variable expected : SLV9;
  begin

    for i in 0 to 255 loop
      A <= std_logic_vector(to_unsigned(i, 8));
      wait for 10 ns;

      expected := std_logic_vector(to_unsigned(i+1, 9));
      assert expected = (V & Y)
        report "Error at " & integer'image(i)
          & " " & SLV_to_bin(A)
          & " " & std_logic'image(V)
          & " " & SLV_to_bin(Y)
        severity failure;
    end loop;

    wait;
  end process;

end plip;
