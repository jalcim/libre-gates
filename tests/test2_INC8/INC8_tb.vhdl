-- file LibreGates/tests/test2_INC8/INC8_tb.vhdl
-- version lun. nov. 13 01:42:14 CET 2017 by Yann Guidon (whygee@f-cpu.org)
-- version mar. oct. 30 07:20:24 CET 2018 : more suitable for individual gate delays
-- version mar. nov. 19 06:24:33 CET 2019 : forked from YGREC8/INC8_tb.vhdl
-- version jeu. nov. 21 07:01:36 CET 2019 : trace->flip generics change
-- version sam. nov. 23 21:33:19 CET 2019 : more generics changes to simplify/reduce code duplication,
--         moved INC8_tb2.vhdl back to INC8_tb.vhdl
-- version lun. déc.  2 22:59:38 CET 2019 : changed register_generics,
--        getting more data from each run to optimise the test time,
--         shaved 56% of test time with new scanning algorithms !
-- version mer. nov. 11 14:20:25 CET 2020 : renamed some
--	variable names to remove "declaration hides variable" warnings
-- 
--
-- Released under the GNU AGPLv3 license

Library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
Library SLV;
    use SLV.SLV_utils.all;
Library work;
    use work.all;
-- added :
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;

entity INC8_tb is
-- added :
  generic (
    -- for the A3P library :
    gate_select_number: integer := 0;  -- when >= 1, select a gate to alter
    bit_select_number: integer := -1;  -- select which input bit code to alter
    flip: integer := -1
       -- default : do nothing, don't instanciate register_generics
       -- clear to 0 to alter 0/1 to meta L/H code
       -- set to 1 to bit flip one bit of the LUT
  );
end INC8_tb;

architecture plip of INC8_tb is
  signal V : SL := '0';
  signal A, Y : SLV8 := "00000000";
-- added :
  signal clock : std_logic := '0';
begin

-- added :
  extra: if flip >= 0 generate
    -- this entity MUST be written first,
    -- at the very beginning of the toplevel file:
    rg: entity register_generics generic map(
         gate_select_number => gate_select_number,
         bit_select_number => bit_select_number,
         flip_LUT => flip) 
        port map(clock => clock);
  end generate;

  tb: entity INC8 port map (
     A => A,
     Y => Y,
     V => V);

  bench: process
    variable i : integer := 0;

    procedure test_cycle(n : integer) is
      variable expected : SLV9;
    begin
      A <= std_logic_vector(to_unsigned(n, 8));
-- added :
      clock <= '0';
      wait for 10 ns;
      clock <= '1';
      wait for 10 ns;

      i := i+1;
--   report " i=" & integer'image(i) &  "  n: " & integer'image(n) & "   Y=" & SLV_to_bin(Y);

      expected := std_logic_vector(to_unsigned(n+1, 9));
      assert expected = (V & Y)
        report "Error at cycle#" & integer'image(i)
          & " n:" & integer'image(n)
          & " A:" & SLV_to_bin(A)
          & " V:" & std_logic'image(V)
          & " Y:" & SLV_to_bin(Y)
        severity failure;
    end test_cycle;

    -- 3995 cycles 
    procedure scan_forward is
      variable j : integer := 0;
    begin
      loop
        test_cycle(j);
        j := j+1;
        exit when j >= 256;
      end loop;
    end scan_forward;

    -- 2742 cycles 
    -- (dumb and some redundancies but good enough)
    procedure dumb_pow2 is
      variable j : integer := 1;
    begin
      -- first, try powers of 2
      loop
        test_cycle(j-1);
        exit when j > 255;
        test_cycle(j);
        j := j+j;
      end loop;

      -- then a dumb loop covers the rest
      for lj in 5 to 254 loop
        test_cycle(lj);
      end loop;
    end dumb_pow2;

    --  2704 cycles 
    procedure teste_reverse is
      variable j : integer := 1;
      variable k : integer := 0;
      variable l : integer := 0;
    begin
      loop
        l := j;
        loop
          if l < 256 then
            test_cycle(l);
          end if;
          l := l-1;
          exit when l < k;
        end loop;
        k := j+1;
        j := j+j;
        exit when j > 256;
      end loop;
    end teste_reverse;

    -- 2527 cycles ...
    procedure scan_folding is
      variable j, k : integer := 0;
    begin
      loop
        test_cycle(j);
        test_cycle(255-j);
        j := j+1;
	exit when j >= 128;
      end loop;
    end scan_folding;

    -- 1743 cycles !
    procedure reverse_folding is
      variable j : integer := 1;  -- the current power of 2
      variable k : integer := 0;  -- the inferior limit for the reverse scan
      variable l : integer := 0;  -- the sub-loop counter for reverse scan
    begin
      loop
	l := j;
        loop
          if (l < 128) then -- 128 appears 2x
            test_cycle(    l);
            test_cycle(255-l);
          end if;
          l := l-1;
          exit when l < k;
        end loop;
        k := j+1;
        j := j+j;
        exit when j > 128;
      end loop;
    end reverse_folding;


  begin
-- added :
    if gate_select_number=-1 then
      wait;
    end if;

    -- Choose your favorite scanning algorithm :
--    scan_forward;
--    scan_folding;
--    dumb_pow2;
--    teste_reverse;
    reverse_folding;

-- added :
    clock<='X'; -- display the histogram
    wait;
  end process;

end plip;
