-- LibreGates/tests/test2_INC8/INC8_tiles.vhdl
-- created sam. nov. 11 18:50:10 2017 by Yann Guidon (whygee@f-cpu.org)
-- version sam. nov. 11 19:17:40 2017
-- version dim. nov. 12 00:07:51 2017
-- version jeu. nov. 16 02:42:00 2017 : uses SLV shorthands from YGREC8/YGREC8_def.vhdl
-- version mer. oct. 17 03:30:21 2018 : reorganised in a "single column"
-- version mer. oct. 17 05:06:32 2018 : PA3
-- version ven. jan.  4 05:40:03 CET 2019 : AX1->AX1C
-- version dim. nov.  8 03:38:14 CET 2020 : moved the declaration in a dedicated file.
--
-- Released under the GNU AGPLv3 license
--
-- This is the increment unit of YGREC8, used to increment the PC.
-- This version uses 3-input gates from Actel/Microsemi's A3P FPGA family.
-- It tries to be suitable for manual placement, all the "tiles" are organised
-- in a single column.

Library ieee;
    use ieee.std_logic_1164.all;
Library SLV;
    use SLV.SLV_utils.all;
Library proasic3;
    use proasic3.all;

architecture tiles of INC8 is
  Signal A012, A34, A345, A3456 : SL;
begin
  -- Row 0
  e_R0B: entity INV    port map(A=> A(0),                    Y=>Y(0)); --1 Y(0) <=                not A(0) ;
  -- Row 1
  e_R1B: entity XOR2   port map(A=> A(0), B=>A(1),           Y=>Y(1)); --1 Y(1)           <= A(1) xor A(0) ;
  -- Row 2
  e_R2B: entity AX1C   port map(A=> A(0), B=>A(1), C=>A(2),  Y=>Y(2)); --1 Y(2) <= A(2) xor (A(1) and A(0));
  -- Row 3
  e_R3A: entity AND3   port map(A=> A(0), B=>A(1), C=>A(2),  Y=>A012); --1 A012 <= A(2) and  A(1) and A(0) ; -- FO7
  e_R3B: entity XOR2   port map(A=> A(3), B=>A012,           Y=>Y(3)); --2 Y(3) <= A(3) xor A012;
  -- Row 4
  e_R4A: entity AND2   port map(A=> A(3), B=>A(4),           Y=> A34); --1 A34  <= A(3) and A(4);          -- F02
  e_R4B: entity AX1C   port map(A=> A012, B=>A(3), C=>A(4),  Y=>Y(4)); --2 Y(4) <= A(4) xor (A(3) and A012);
  -- Row 5
  e_R5A: entity AND3   port map(A=> A(3), B=>A(4), C=>A(5),  Y=>A345); --1 A345 <= A(3) and A(4) and A(5); -- FO1
  e_R5B: entity AX1C   port map(A=> A012, B=>A34,  C=>A(5),  Y=>Y(5)); --2   Y(5) <= A(5) xor (A012 and A34);
  -- Row 6
  e_R6A: entity AND3   port map(A=>  A34, B=>A(5), C=>A(6), Y=>A3456); --2  A3456 <= A34  and A(5) and A(6); -- FO2
  e_R6B: entity AX1C   port map(A=> A012, B=>A345, C=>A(6),  Y=>Y(6)); --2    Y(6) <= A(6) xor (A012 and A345);
  -- Row 7
  e_R7A: entity AND3   port map(A=>A012, B=>A3456, C=>A(7),  Y=>   V); --3    V    <= A(7) and  A012 and A3456;
  e_R7B: entity AX1C   port map(A=>A012, B=>A3456, C=>A(7),  Y=>Y(7)); --3   Y(7) <= A(7) xor (A012 and A3456);
end tiles;
