file test2_INC8/README.txt
created dim. nov. 24 17:35:39 CET 2019 by whygee@f-cpu.org
version lun. déc. 30 15:55:59 CET 2019
version jeu. juil. 16 02:50:04 CEST 2020


This directory implements a full test of a 8-bits incrementer,
which is available in several versions :
INC8.vhdl       : This dumb version uses the built-in + operator
INC8_A3P.vhdl   : This version is mapped to 3-input gates from Actel/Microsemi's A3P FPGA family
                     but uses generic operations.
INC8_gates.vhdl : Same as INC8_A3P.vhdl but with gates rearranged for placement.
INC8_tiles.vhdl : same as INC8_gates.vhdl with gates mapped to library gates architecture.
INC8_ASIC.vhdl : derived from INC8_tiles.vhdl but mapped to an ASIC using standard cells
 (INC8_ASIC.png details all the gates and INC8_ASIC_test.cjs is useful to run the circuit
   through www.falstad.com/circuit/circuitjs.html )

Preserving all the steps of the design helps pinpoint errors in the process
and provides a fallback in case of regression. Anyway, the unit works just as
expected and is exhaustively tested by INC8_tb.old.vhdl : this minimal test covers
all the cases and should be enough.

A new test is derived : INC8_tb.vhdl
Changes are marked and provide new features, such as generics on the command line
and control the alteration of the design. A non-linear scanning algorithm also
speeds up some fault coverage tests.

Command line parameters :

* ./inc8_tb : runs the full test as usual.

* ./inc8_tb -gflip=0 : enables listing the gates and run the test

* ./inc8_tb -gflip=0 -ggate_select_number=-1 : list the gates but don't run the test

* ./inc8_tb -gflip=1 -ggate_select_number=X -gbit_select_number=Y : inject a fault,
     run the test with the Yth bit of the Xth gate's LUT flipped 

* ./inc8_tb -gflip=0 -ggate_select_number=X -gbit_select_number=Y : trace the propagation
    of the input state of a gate : run the test, while the Yth bit of the Xth gate's LUT
    generates a meta-value ('L' or 'H' instead of '0' or '1').

You should look at the diff of the two files INC8_tb.old.vhdl and INC8_tb.vhdl
to see the modifications, which are also described in the README.txt at the root
of the project.

The script test.sh will test all the sanity checks, then extract a rough listing
of all the tested gates, then all the bits of all the gate LUTs will be flipped
to check that all error cases are covered. Of course, all the injected errors must
be caught :-) Finally, the whole netlist is extracted and displayed in order of depth.
