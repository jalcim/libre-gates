#!/bin/bash
# file test.sh
# created by Yann Guidon / ygdes.com
# version sam. nov. 11 19:26:36 CET 2017
# version lun. nov. 13 02:34:03 CET 2017
# version jeu. nov. 16 03:07:00 CET 2017
# version mer. oct. 17 03:38:23 CEST 2018 : INC8_tiles.vhdl & INC8_gates.vhdl
# version mar. nov. 19 06:02:18 CET 2019 : integrated in the proasic3v2 testbench
# version jeu. nov. 21 06:57:25 CET 2019 : trace changes
# version mar. nov. 19 04:31:45 CET 2019 : fork/split from proasic3v2/build.sh
# version jeu. jul. 16 02:36:53 CEST 2020 : added ASIC version
# version ven. nov. 13 11:58:04 CET 2020 : use parallel.sh

PA3="-P$PWD/../../LibreGates -P$PWD/../../LibreGates/proasic3"

function run_test() {
  # arg.1 : INC8 version/file
  # arg.2 : message
  rm -rf version_$2 &&
  mkdir version_$2 &&
  cd version_$2 &&
  ghdl -a $PA3 ../INC8.decl.vhdl ../$1 ../INC8_tb.vhdl &&
  ghdl -e $PA3 INC8_tb &&
  ./inc8_tb &&
  cd .. &&
  rm -rf version_$2 &&
  echo "INC8 $2 : OK"
}

[ -f ../../LibreGates/proasic3/simple/proasic3-obj93.cf ] &&
echo -e "\nSanity check of the reference designs :" &&
{
run_test INC8.vhdl        "simple" &
run_test INC8_A3P.vhdl    "A3P"    &
run_test INC8_gates.vhdl  "gates"  &
run_test INC8_tiles.vhdl  "tiles"  &
run_test INC8_ASIC.vhdl   "ASIC"   &
} &&
wait &&
## 1.9s when serial
## 0.86s in parallel

echo -e "\nOK now let's list the gates" &&
rm -f work-obj93.cf *.o &&
ghdl -a $PA3 INC8.decl.vhdl INC8_ASIC.vhdl INC8_tb.vhdl &&
ghdl -e $PA3 INC8_tb &&
./inc8_tb -gflip=0  |sed 's/.*note): //' &&
# -gflip=0 enables additional features

echo -n -e "\nThen we check that all the errors can be found " &&
. ../../LibreGates/parallel.sh &&
{ ScanErrors ./inc8_tb || {
    echo $'\e[31m'"Something wrong happened !"$'\e[0m'
rm -f work-obj93.cf *.o &&
ghdl -a $PA3 INC8.decl.vhdl INC8_ASIC.vhdl INC8_tb.vhdl &&
ghdl -e $PA3 INC8_tb &&
./inc8_tb -gflip=0  |sed 's/.*note): //'    false
  }
} &&

echo -e "\nFinally let's extract and check the netlist :" &&
rm -f inc8 work-obj93.cf *.o &&
# scan the ports
../../LibreGates/wrap_netlist.sh $PA3 INC8.decl.vhdl > INC8_Wrapped.vhdl &&
ghdl -a $PA3 INC8_ASIC.vhdl INC8_Wrapped.vhdl &&
ghdl -e $PA3 Wrapper &&
./wrapper | sed 's/.*te): //' &&

echo $'\e[32m'"INC8 : OK"$'\e[0m'
