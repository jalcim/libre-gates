-- YGREC8/VHDL/TAP/Gray/Gray6_RTL.vhdl
-- created ven. juil. 17 19:02:35 CEST 2020 by whygee@f-cpu.org
-- version sam. juil. 18 00:32:19 CEST 2020 : Gray6_behavioral.vhdl => Gray6_RTL.vhdl
--
-- Released under the GNU AGPLv3 license
--
-- The less trivial Gray code counter.
-- DFFs are tied directly to the outputs
-- and no glitch can occur.
-- DFF with enable are emulated with IFs
-- in the process.

Library ieee;
    use ieee.std_logic_1164.all;
Library SLV;
    use SLV.SLV_utils.all;

entity Gray6 is
  port(
    CLK, RESET : in SL;
    G6 : out SLV6);
end Gray6;

architecture RTL of Gray6 is
  signal FF: SLV6;
begin
  G6 <= FF; -- G6 is not inout

  process (CLK, RESET) is
    variable dir01, dir23,
      x0, x1, x2, x3,
      sat01, sat23 : SL;
  begin
    if RESET/='1' then
      FF <= "000000";
    else
      if rising_edge(CLK) then

        -- downwards chain :
        dir23 :=  FF(4) xor FF(5);
        x3 := not FF(3) xor dir23;
        x2 :=     FF(2) xor dir23;

        -- middle bits :
        dir01 :=  FF(3) xor x2;
        x1 := not FF(1) xor dir01;
        x0 :=     FF(0) xor dir01;
        
        -- upwards now, from the LSB :
        sat01 :=  FF(0) nor x1;
        FF(0) <= x1;
        FF(1) <= x0 xor sat01;

        -- then the middle bits
        if sat01 = '1' then
          sat23 := FF(2) nor x3;
          FF(2) <= x3;
          FF(3) <= x2 xor sat23;

          -- the MSB :
          if sat23 = '1' then
            FF(4) <= not FF(5);
            FF(5) <=     FF(4);
          end if;
        end if;
      end if;
    end if;
  end process;
end RTL;
