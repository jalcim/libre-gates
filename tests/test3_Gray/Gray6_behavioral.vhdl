-- YGREC8/VHDL/TAP/Gray/Gray6_behavioral.vhdl
-- created ven. juil. 17 19:02:35 CEST 2020 by whygee@f-cpu.org
--
-- Released under the GNU AGPLv3 license
--
-- This is a trivial Gray code counter. This is only used for
-- behavioral simulations because the output is potentially
-- glitchy and requires another 2 levels of DFF.

Library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
Library SLV;
    use SLV.SLV_utils.all;

entity Gray6 is
  port(
    CLK, RESET : in  SL;
    G6 : out SLV6);
end Gray6;

architecture behavioral of Gray6 is
  signal bin : unsigned(5 downto 0);
begin

  process (CLK, RESET) is
    variable t : unsigned(5 downto 0);
  begin
    if RESET='0' then
      G6  <= "000000";
      bin <= "000000";
    else
      if rising_edge(CLK) then
        t := unsigned(bin) + 1;
        bin <= t;
        G6 <=  std_logic_vector( t(5) &
                  ( t(5 downto 1) xor t(4 downto 0)) );
      end if;
    end if;
  end process;
end behavioral;
