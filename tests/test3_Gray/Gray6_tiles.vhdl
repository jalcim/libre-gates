-- YGREC8/VHDL/TAP/Gray/Gray6_tiles.vhdl
-- created ven. juil. 17 19:02:35 CEST 2020 by whygee@f-cpu.org
-- version sam. juil. 18 00:32:19 CEST 2020 : Gray6_behavioral.vhdl => Gray6_RTL.vhdl
-- version sam. juil. 18 01:35:46 CEST 2020 : => Gray6_tiles.vhdl
-- Released under the GNU AGPLv3 license
--
-- The less trivial Gray code counter.
-- DFFs are tied directly to the outputs
-- and no glitch can occur.
-- DFF with enable are emulated with IFs
-- in the process.

Library ieee;
    use ieee.std_logic_1164.all;
Library SLV;
    use SLV.SLV_utils.all;
Library proasic3;
    use proasic3.all;

entity Gray6 is
  port(
    CLK, RESET : in SL;
    G6 : out SLV6);
end Gray6;

architecture tiles of Gray6 is
  signal FF: SLV6;
  signal x0, x1, x2, x3, n1, n3, ff5n,
    dir01, dir23, sat01, sat23, sat45 : SL;
    -- note : there are now 3 "sat" signals,
    -- one for each pair of bits.
begin
  G6 <= FF; -- G6 is not inout

-- MSB :
--   dir23 :=  FF(4) xor FF(5);
--   if sat45 = '1' then
--     FF(5) <=     FF(4);
--     FF(4) <= not FF(5); -> bodge iv5 / ff5n
  x23:  entity XOR2     port map(A=>FF(4), B=>FF(5), Y=> dir23);
  Gr5:  entity DFN1E1C0 port map(D=>FF(4), E=>sat45, CLK=>CLK, CLR=>RESET, Q=>FF(5));
  iv5:  entity INV      port map(A=>FF(5), Y=>ff5n); -- Bodge inverter
  Gr4:  entity DFN1E1C0 port map(D=> ff5n, E=>sat45, CLK=>CLK, CLR=>RESET, Q=>FF(4));

-- Middle bits :
--   x3 := not FF(3) xor dir23;
--   x2 :=     FF(2) xor dir23;
--   dir01 :=  FF(3) xor x2;
--   if sat01 = '1' then
--     sat23 := FF(2) nor x3;
--     FF(2) <= x3;
--     FF(3) <= x2 xor sat23; (n3)
--     sat45 <= sat23 and sat01;
  s23:  entity NOR2     port map(A=>FF(2), B=>   x3, Y=> sat23);
  f3x:  entity XOR2     port map(A=>sat23, B=>   x2, Y=>    n3);
  Gr3:  entity DFN1E1C0 port map(D=>   n3, E=>sat01, CLK=>CLK, CLR=>RESET, Q=>FF(3));
  xx3:  entity XNOR2    port map(A=>dir23, B=>FF(3), Y=>    x3);
  Gr2:  entity DFN1E1C0 port map(D=>   x3, E=>sat01, CLK=>CLK, CLR=>RESET, Q=>FF(2));
  xx2:  entity XOR2     port map(A=>dir23, B=>FF(2), Y=>    x2);
-- chaining outputs:
  s45:  entity AND2     port map(A=>sat23, B=>sat01, Y=> sat45);
  d01:  entity XOR2     port map(A=>   x2, B=>FF(3), Y=> dir01);

-- LSB :
-- sat01 :=  FF(0) nor x1;
-- FF(1) <= x0 xor sat01;  (+n1)
-- x1 := not FF(1) xor dir01;
-- FF(0) <= x1;
-- x0 := FF(0) xor dir01;
  s01:  entity NOR2     port map(A=>FF(0), B=>   x1, Y=> sat01);
  f1x:  entity XOR2     port map(A=>sat01, B=>   x0, Y=>    n1);
  Gr1:  entity DFN1C0   port map(D=>   n1, CLK=>CLK, CLR=>RESET, Q=>FF(1));
  xx1:  entity XNOR2    port map(A=>dir01, B=>FF(1), Y=>    x1);
  Gr0:  entity DFN1C0   port map(D=>   x1, CLK=>CLK, CLR=>RESET, Q=>FF(0));
  xx0:  entity XOR2     port map(A=>dir01, B=>FF(0), Y=>    x0);

end tiles;
