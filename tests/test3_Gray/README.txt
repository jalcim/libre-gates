/data/had/YGREC8/VHDL/TAP/Gray/README.txt
created dim. juil. 19 20:39:24 CEST 2020 by whygee@f-cpu.org
version dim. nov. 15 14:47:03 CET 2020

This directory implements a 6-bits Gray code counter,
including a gate-level design based on Johnson counters,
which is glitchless and favours compactness over speed.

The design is explained in detail at
https://hackaday.io/project/27280-ygrec8/log/180888-gray-counter
That page shows how to change the size of the counter,
which is modular :

* The LSB are implemented with a modified, saturating
   Johnson counter directly clocked from the input.

* The middle bits is the more complex code, that might
   use clock gating and/or "latch enable".

* The top bit(s) might be a single enabled-DFF or two.

Except for the first and last couple of bits,
the modules communicate with 2 bits on each side :
 - saturation : the current level has reached the end
     of the sequence, the signal is used to enable
     changes of the upper bits.
 - direction : tell the lower counters to count up or down.

Using this system, the counter's length can be adapted
to other applications. For now, 6 bits are enought
to probe a 8-bits system.
