# LibreGates/tests/test3_Gray/test.sh
# version sam. juil. 18 06:37:32 CEST 2020
# version dim. nov. 15 13:47:19 CET 2020 : parallelised

echo "Now let's test some DFF..."

PA3="-P$PWD/../../LibreGates -P$PWD/../../LibreGates/proasic3"

function run_test() {
  # $1 : file/version
  # $2 : message
  # $3 : directory name
  rm -rf $3 &&
  mkdir $3 &&
  cd $3 &&
  ghdl -a $PA3 ../$1 ../test_Gray6.vhdl &&
  ghdl -e $PA3 test_Gray6 &&
  ./test_gray6 &&
  echo " - Gray6 $2 : OK"
}

[ -f ../../LibreGates/proasic3/simple/proasic3-obj93.cf ] &&
{
run_test Gray6_behavioral.vhdl "behavioral" "version_behav" &
run_test Gray6_RTL.vhdl        "       RTL" "version_rtl"   &
run_test Gray6_tiles.vhdl      "     Tiles" "version_tiles" &
} &&
wait -n &&
wait -n &&
wait -n &&
rm -r version_* &&
echo $'\e[32m'"=> Gray counter : pass"$'\e[0m'
