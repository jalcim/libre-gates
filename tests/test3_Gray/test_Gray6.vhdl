-- file YGREC8/VHDL/TAP/Gray/test_Gray6.vhdl
-- created ven. juil. 17 19:48:28 CEST 2020
--
-- Released under the GNU AGPLv3 license

Library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
Library SLV;
    use SLV.SLV_utils.all;
Library work;
    use work.all;

entity test_Gray6 is
  generic (  maxcount: integer := 200  );
  -- top level -> nothing
end test_Gray6;

architecture behavioural of test_Gray6 is
  signal Clk, Reset : SL := '0';
  signal G6 : SLV6;
begin

  dut: entity Gray6 port map(
      Clk => Clk,
    Reset => Reset,
       G6 => G6 );

  process is
    variable count : integer;

    procedure cycle is
    begin
      wait for 500 us;
      Clk <= '1' ;
      wait for 500 us;
      Clk <= '0' ;
    end cycle;

    procedure show is
      variable t : SLV6;
      variable j : integer;
    begin
      t := G6 xor ("0000" & G6(5 downto 4)); --  num ^= num >>  4;
      t :=  t xor (  "00" &  t(5 downto 2)); --  num ^= num >>  2;
      t :=  t xor (   '0' &  t(5 downto 1)); --  num ^= num >>  1;
      j :=  to_integer(unsigned(t));

      if maxcount /= 200 then
        report  integer'image(count)
            & ":  " & SLV_to_bin(G6)
            & " = " & SLV_to_bin(t)
            & " = " & integer'image(j) ;
      else
        assert j = count mod 64
          report "mismatch at value "
                & integer'image(count)
                & ":  " & SLV_to_bin(G6)
                & " = " & integer'image(j)
            severity warning ; -- failure;
      end if;
    end show;

  begin

    count := 0;
    -- report "* Asserting RESET";
    Reset <= '0';
    Clk <= '0' ;
    wait for 1000 us;
    show;

    -- report "* Releasing RESET";
    Reset <= '1';
    wait for 1000 us;
    show;

    -- report "* Let's go !";
    while count <= maxcount loop
      count := count + 1;
      cycle;
      show;
    end loop;

    wait;
  end process;

end behavioural;
