-- LibreGates/tests/test4_cornercases/backwards_loops.decl.vhdl
-- created mer. déc. 25 22:13:38 CET 2019 by whygee@f-cpu.org
-- version mer. sept.  2 23:11:10 CEST 2020
-- version dim. nov.  8 05:51:45 CET 2020  with split files
--
-- Released under the GNU AGPLv3 license

Library ieee;
    use ieee.std_logic_1164.all;
Library SLV;
    use SLV.SLV_utils.all;

entity BackwardsLoops is
  port(
    VIn : in  SLV5;
    VOut: out SLV3
  );
end BackwardsLoops;
