-- LibreGates/tests/test4_cornercases/backwards_loops.vhdl
-- created mer. déc. 25 22:13:38 CET 2019 by whygee@f-cpu.org
-- version mer. sept.  2 23:11:10 CEST 2020
-- version dim. nov.  8 05:51:45 CET 2020  with split files
--
-- This unit shows how to solve problems caused by loops
-- as demonstrated in weird_unit.vhdl. The "Backwards" component
-- explicity breaks the logic loop and enables custom R/S or
-- transparent latches.
--
-- Released under the GNU AGPLv3 license

Library ieee;
    use ieee.std_logic_1164.all;
Library SLV;
    use SLV.SLV_utils.all;
Library work;
    use work.all;
Library proasic3;
    use proasic3.all;

architecture loops of BackwardsLoops is
  signal sig1, sig1b, sig2,
     sig3, sig3b, sig4, sig5 : std_logic;
begin
  -- Just a set/reset flip-flop :
  ff1: entity NAND2 port map(A=>VIn(0), B=>sig2,  Y=>sig1);
  fb1: entity backwards port map(A=>sig1,            Y=>sig1b);
  ff2: entity NAND2 port map(A=>VIn(1), B=>sig1b, Y=>sig2);
  VOut(0) <= sig1;
  VOut(1) <= sig2;

  -- Just a transparent latch:
  mx0: entity MX2 port map(A=>VIn(2), B=>sig3b, S=>VIn(3), Y=>sig3);
  dum: entity AND2 port map(A=>sig3,  B=>VIn(4),           Y=>sig4);
  fb2: entity backwards port map(A=>sig3,                  Y=>sig3b);
  VOut(2) <= sig4;
end loops;
