-- Wrapper.vhdl
-- Generated on 20230528-05:31
-- Do not modify

Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;
    use LibreGates.Netlist_lib.all;

entity Wrapper is
  generic (
    WrapperWidthIn : integer := 5;
    WrapperWidthOut: integer := 3;
    listgates: integer := 1;  -- set to 0 to display the raw list of gates
    main_delay : time := 1 sec;
    filename : string := ""; -- file of exclude vectors
    verbose : string := ""
  );
end Wrapper;

architecture Wrap of Wrapper is
  signal VectIn : std_logic_vector(WrapperWidthIn -1 downto 0);
  signal VectOut: std_logic_vector(WrapperWidthOut-1 downto 0);
  signal VectClk: std_logic := 'X';

begin

  -- force the registration of the gates.
  -- must be placed at the very beginning
  rg: entity register_generics generic map(
         gate_select_number => listgates, -- show gate listing when < 1
         bit_select_number => -1, -- no alteration
         flip_LUT => 2,           -- probe mode
         filename => filename,    -- eventual exclusions
         verbose => verbose)
  port map(clock => '0');   -- the updates are directly driven by Drive_DUT
  -- note : the corresponding procedure is not called
  -- at the right time if it is called directly,
  -- the entity ensures the correct calling order.

  Drive_DUT(VectIn, VectOut, VectClk, main_delay);

  -- Finally we "wire" the unit to the ports:
  tb: entity backwardsloops
    port map(
      vin => VectIn(4 downto 0),
      vout => VectOut(2 downto 0));
end Wrap;
