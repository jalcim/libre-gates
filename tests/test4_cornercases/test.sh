#!/bin/bash
# file LibreGates/tests/test4_cornercases/test.sh
# created by Yann Guidon / whygee@f-cpu.org
# version mer. déc.  9 05:37:04 CET 2020 : netlist probe works again

echo
echo  "This script tests corner cases and possible problems, don't worry."
echo

PA3="-P../../LibreGates -P../../LibreGates/proasic3"

[ -f ../../LibreGates/proasic3/simple/proasic3-obj93.cf ] &&

rm -f *.o *.cf wrapper &&
../../LibreGates/wrap_netlist.sh $PA3 weird_unit.decl.vhdl > weird_unit_Wrapped.vhdl &&
ghdl -a $PA3 weird_unit.vhdl weird_unit_Wrapped.vhdl &&
ghdl -e $PA3 wrapper &&
./wrapper | sed 's/.*note): //' &&
echo $'\e[32m'"The above command should have failed due to unconnected inputs."$'\e[0m' &&

rm -f *.o *.cf wrapper &&
../../LibreGates/wrap_netlist.sh $PA3 backwards_loops.decl.vhdl > backwards_loops_Wrapped.vhdl &&
ghdl -a $PA3 backwards_loops.vhdl backwards_loops_Wrapped.vhdl &&
ghdl -e $PA3 wrapper &&
./wrapper | sed 's/.*note): //' &&

echo $'\e[32m'"Test 4 : OK"$'\e[0m'
