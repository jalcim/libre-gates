-- LibreGates/tests/test4_cornercases/weird_unit.decl.vhdl
-- created mer. déc. 25 22:13:38 CET 2019 by whygee@f-cpu.org
-- version sam. nov.  7 10:30:59 CET 2020 : split and uses the new wrapper generator.
-- version mer. déc. 16 03:49:14 CET 2020 : added driver conflict
--
-- Released under the GNU AGPLv3 license

Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library proasic3;
    use proasic3.all;
Library SLV;
    use SLV.SLV_utils.all;

entity Wrapped is
  port(
    VIn : in  SLV14;
    VOut: out SLV8
  );
end Wrapped;
