-- LibreGates/tests/test4_cornercases/weird_unit.vhdl
-- created mer. déc. 25 22:13:38 CET 2019 by whygee@f-cpu.org
-- version sam. nov.  7 10:30:59 CET 2020 : split and uses the new wrapper generator.
-- version mer. déc. 16 03:49:14 CET 2020 : added driver conflict
--
-- This is not a functional or useful unit but it contains cases
-- that should be caught by the tools.
--
-- Released under the GNU AGPLv3 license

Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library proasic3;
    use proasic3.all;
Library SLV;
    use SLV.SLV_utils.all;

architecture Weird of Wrapped is
  signal sig1, sig2, sig3, sig4, sig5, sig6 : std_logic;
begin

  -- a set/reset flip-flop :
  ff1: entity NAND2 port map(A=>VIn(0), B=>sig2, Y=>sig1);
  ff2: entity NAND2 port map(A=>VIn(1), B=>sig1, Y=>sig2);
  VOut(0) <= sig1;
  VOut(1) <= sig2;

  -- a transparent latch:
  mx0: entity MX2 port map(A=>VIn(2), B=>sig3,      S=>VIn(3), Y=>sig3);
  dummy: entity AND2 port map(A=>sig3,   B=>VIn(4), Y=>sig4);
  VOut(2) <= sig4;

  -- output #3 is not connected
  -- VOut(3) <= 'U';

  -- input #5 is not connected

  -- unconnected output : unobservable
  open1: entity AND2 port map(A=>VIn(6), B=>VIn(7), Y=>sig5);
  -- > to test if the general depth is reduced when finding a "long" unconnected network:
  open2: entity AND2 port map(A=>sig5,      B=>VIn(8), Y=>OPEN);

  -- unconnected inputs : uncontrollable
  open3: entity AND2 port map(A=>'U', B=>'U', Y=>VOut(4));

  -- driver conflict : 2 gates try to drive a single output port
  clf1:  entity AND2 port map(A=>VIn(9), B=>VIn(10), Y=>VOut(5));
  clf2:  entity OR2  port map(A=>VIn(9), B=>VIn(10), Y=>VOut(5));

  -- another driver conflict where 2 signals drive a single gate input
  sig6 <= VIn(11);
  sig6 <= VIn(12);
  cfl3: entity BUFF port map(A=>sig6, Y=>VOut(6));

  -- detect when a gate receives the same signal on different inputs:
  com: entity AND2 port map(A=>VIn(13), B=>VIn(13), Y=>VOut(7));

end Weird;
