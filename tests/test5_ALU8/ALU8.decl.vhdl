-- LibreGates/tests/test5_ALU8/ALU8.decl.vhdl
-- forked sam. nov.  7 16:52:53 CET 2020
--
-- just the "header" so the ports can be extracted.
-- and also to shave some identical lines from the various implementations.

Library ieee;
    use ieee.std_logic_1164.all;
Library SLV;
    use SLV.SLV_utils.all;

entity ALU8 is
  port(
    Cin, NEG, PassMask, OrXor, Rop2Mx, CMPS : in SL;
    SRI, SND : in SLV8;
    ROP2, SUM : out SLV8;
    Cout : out SL);
end ALU8;
