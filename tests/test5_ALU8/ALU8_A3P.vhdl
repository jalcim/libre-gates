-- LibreGates/tests/test5_ALU8/ALU8_A3P.vhdl
-- created mer. nov.  8 07:21:19 CET 2017 by Yann Guidon (whygee@f-cpu.org)
-- version ven. nov. 10 06:44:34 CET 2017
-- version jeu. nov. 16 02:21:35 CET 2017 : uses SLV shorthands from YGREC8/YGREC8_def.vhdl
-- version mer. mars 13 15:45:52 CET 2019 : swapped XOR and OR
-- version mer. mars 20 07:29:05 CET 2019 : SET/Pass re-implemented
-- version jeu. mars 21 08:51:50 CET 2019 : renamed SRC & DST
-- version lun. mars 25 01:00:02 CET 2019 : new ROP2 structure,	all opcodes OK
-- version mer. mars 27 21:17:03 CET 2019 : A3P init
-- version lun. avril 1 03:56:12 CEST 2019 : separate decoder
-- version dim. nov. 24 20:05:31 CET 2019 : shorter code : half the size with for..generate
-- version mer. nov. 27 05:36:03 CET 2019 : now using "exclude" generics
-- version dim. nov.  8 03:38:14 CET 2020 : moved the declaration in a dedicated file.
-- Released under the GNU AGPLv3 license
--
-- This is the arithmetic and logic unit of YGREC8,
-- performing the add/sub and boolean operations on 8 bits
-- with separate outputs for ROP2 and ADD.
-- This version is broken down into 3-inputs "tiles"
-- that are mapped to A3P gates.
--
-- OPCODES:
-- F3 F2 F1 F0      tested
-- 0  0  0  0  XOR    *
-- 0  0  0  1  OR     *
-- 0  0  1  0  AND    *
-- 0  0  1  1  ANDN   *
-- 0  1  0  0  CMPU   *
-- 0  1  0  1  CMPS   *
-- 0  1  1  0  SUB    *
-- 0  1  1  1  ADD    *
-- 1  0  0  0  SET    *
--
---------------------------------------------------------

Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library SLV;
    use SLV.SLV_utils.all;
Library	YGREC8;
    use YGREC8.ygrec8_def.all;
Library proasic3;
    use proasic3.all;

architecture A3P of ALU8 is
  Signal ALU_XOR, P, G, S, C, X, AndPass : SLV8;
  Signal P2, G2 : SLV2;
  Signal Carry, Carry2, Carry3, C2, SignedOverflow : SL;
begin

  -- The ROP2 part:
  GenROP2: for I in 0 to 7 generate
    --1
    rop2_g: entity XA1 port map(A=>NEG, B=>SND(I), C=>SRI(I), Y=>G(I)); --  G <= SRI and (SND xor NEG);
    rop2_p: entity XO1 port map(A=>NEG, B=>SND(I), C=>SRI(I), Y=>P(I)); --  P <= SRI or  (SND xor NEG);
    --2
    rop2_x: entity OA1A generic map(exclude=>"----X-X-") port map(A=>G(I), B=>OrXor, C=>P(I),  Y=>X(I)); --  X <= P and (OrXor or not G);
    rop2_a: entity AO1  generic map(exclude=>"-X---X--") port map(A=>PassMask, B=>SRI(I), C=>G(I), Y=> AndPass(I)); -- AndPass <= G or (PassMask and SRI);
    -- 3
    rop2_mx0: entity MX2 port map(A=>AndPass(I), B=>X(I), S=>Rop2Mx, Y=>ROP2(I)); -- ROP2 <= X when Rop2Mx='1' else AndPass;
  end generate GenROP2;

  sof: entity AND2A port map(A=>X(7), B=>CMPS, Y=>SignedOverflow); -- SignedOverflow <= not X(7) and CMPS;


-- ADD/SUB, Carry-Lookahead adder with 2-levels of CLA3 blocks
  C(0) <= Cin;

  -- First CLA3 block:
  --inputs: Cin, G0, G1, G2, P0, P1, P2

  cla1a: entity AO1 generic map(exclude=>"-X---X--") port map(A=>Cin,  B=>P(0),  C=>G(0),  Y=>C(1));  --   C(1)  <= G(0)  OR (Cin AND P(0));
  cla1b: entity AND3       port map(A=>P(0), B=>P(1),  C=>P(2),  Y=>P2(0)); --  P2(0) <=  P(0) AND P(1) AND P(2);
  cla1c: entity CLA3(ASIC) port map(A=>P(1), B=>Cin,   C=>P(0),  D=>G(0),  E=>G(1), Y=>C(2));  --  C(2)  <= G(1)  OR (Cin AND P(0) AND P(1))   OR (G(0) AND P(1));
  cla1d: entity CLA3(ASIC) port map(A=>P(2), B=>G(0),  C=>P(1),  D=>G(1),  E=>G(2), Y=>G2(0)); --  G2(0) <= G(2)  OR (G(0) AND P(1) and P(2))  OR (G(1) AND P(2));
  cla1e: entity AO1        port map(A=>Cin,  B=>P2(0), C=>G2(0), Y=>C(3)); --  C(3)  <= G2(0) OR (Cin AND P2(0));

  -- Second CLA3 block:
  --inputs: C3, G3, G4, G5, P3, P4, P5
  cla2a: entity AO1 generic map(exclude=>"-X---X--") port map(A=>C(3), B=>P(3), C=>G(3), Y=>C(4));  --  C(4)  <= G(3)  OR (C(3) AND P(3));
  cla2b: entity AND3       port map(A=>P(3), B=>P(4), C=>P(5), Y=>P2(1)); --  P2(1) <= P(3)  AND P(4) AND P(5);

  cla2c: entity CLA3(ASIC) port map(A=>P(4), B=>P(3), C=>C(3), D=>G(3), E=>G(4), Y=>C(5));  -- C(5)  <= G(4)  OR (C(3) AND P(3) AND P(4))  OR (G(3) AND P(4));
  cla2d: entity CLA3(ASIC) port map(A=>P(5), B=>P(4), C=>G(3), D=>G(4), E=>G(5), Y=>G2(1)); -- G2(1) <= G(5)  OR (G(3) AND P(4) and P(5))  OR (G(4) AND P(5));

  cla2e: entity AO1  port map(A=>C(3), B=>P2(1), C=>G2(1), Y=>C(6)); --  C(6)  <= G2(1) OR (C(3) AND P2(1));

  -- 2nd level of CLA
  cla3a: entity CLA3(ASIC) port map(A=>P2(1), B=>Cin, C=>P2(0), D=>G2(0), E=>G2(1), Y=>C2); -- C2 <= G2(1) OR (Cin AND P2(0) AND P2(1)) OR (G2(0) AND P2(1));

  --inputs: C2, G6, G7, P6, P7
  cla3b: entity AO1 generic map(exclude=>"-X---X--") port map(A=>C2, B=>P(6),  C=> G(6), Y=>C(7)); -- C(7) <= G(6) OR (C2 AND P(6));

-- Carry Out :
--  Carry <=  G(7) OR (C2 AND P(6) AND P(7)) OR (G(6) AND P(7));
-- broken down:
--  Carry2 <= G(6) AND P(7);
--  Carry3 <= C2 AND P(6) AND P(7);
--  Carry <= G(7) OR Carry2 OR Carry3;
-- Lower latency :
  cry_a: entity AND2 port map(A=>P(6), B=> P(7),             Y=>Carry2); --  Carry2 <= P(6) AND P(7);
  cry_b: entity AO1 generic map(exclude=>"-X---X--")
                     port map(A=>G(6), B=> P(7),  C=>G(7),   Y=>Carry3); --  Carry3 <= (G(6) AND P(7)) OR G(7);
  cry_c: entity AO1  port map(A=>C2,   B=>Carry2, C=>Carry3, Y=>Carry);  --  Carry  <= (C2 AND Carry2) OR Carry3;

-- Final:
  GenSum: for I in 0 to 7 generate
    sumx_0:  entity XOR2 port map(A=>X(I), B=>C(I), Y=>SUM(I)); --   SUM <= X XOR C;
  end generate GenSum;
  sumx_8:  entity XOR2 port map(A=>SignedOverflow, B=>Carry , Y=>Cout);

end A3P;
