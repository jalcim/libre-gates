-- LibreGates/tests/test5_ALU8/ALU8_CLA.vhdl
-- created mer. nov.  8 07:21:19 CET 2017 by Yann Guidon (whygee@f-cpu.org)
-- version ven. nov. 10 06:44:34 CET 2017
-- version jeu. nov. 16 02:21:35 CET 2017 : uses SLV shorthands from YGREC8/YGREC8_def.vhdl
-- version mer. mars 13 15:45:52 CET 2019 : swapped XOR and OR
-- version mer. mars 20 07:29:05 CET 2019 : SET/Pass re-implemented
-- version jeu. mars 21 08:51:50 CET 2019 : renamed SRC & DST
-- version lun. mars 25 01:00:02 CET 2019 : new ROP2 structure,	all opcodes OK
-- version lun. avril 1 03:56:12 CEST 2019 : separate decoder
-- version dim. nov.  8 03:38:14 CET 2020 : moved the declaration in a dedicated file.
-- Released under the GNU AGPLv3 license
--
-- This is the arithmetic and logic unit of YGREC8,
-- performing the add/sub and boolean operations on 8 bits
-- with separate outputs for ROP2 and ADD.
-- This version is is broken down into 3-inputs "tiles"
-- but not yet mapped to A3P gates (see ALU8_A3P.vhdl)
--
-- OPCODES:
-- F3 F2 F1 F0      tested
-- 0  0  0  0  XOR    *
-- 0  0  0  1  OR     *
-- 0  0  1  0  AND    *
-- 0  0  1  1  ANDN   *
-- 0  1  0  0  CMPU   *
-- 0  1  0  1  CMPS   *
-- 0  1  1  0  SUB    *
-- 0  1  1  1  ADD    *
-- 1  0  0  0  SET    *

Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library YGREC8;
    use YGREC8.ygrec8_def.all;
Library SLV;
    use SLV.SLV_utils.all;

architecture CLA3 of ALU8 is
  Signal  ALU_XOR, P, G, S, C, X,
     NEG8, PassMask8, OrXor8, AndPass : SLV8;
  Signal P2, G2 : SLV2;
  Signal C2, SignedOverflow : SL;

begin
  -- fanout:
  NEG8      <= (others=>      NEG);
  OrXor8    <= (others=>    OrXor);
  PassMask8 <= (others=> PassMask);

  -- The ROP2 part:
  G       <= SRI and (SND xor NEG8);
  P       <= SRI  or (SND xor NEG8);
  
  X       <= P   and (OrXor8    or not G);
  AndPass <= G    or (PassMask8 and  SRI);

  ROP2    <= X when Rop2Mx='1'
               else AndPass;

  SignedOverflow <= not X(7) and CMPS;

  -- ADD/SUB, Carry-Lookahead adder with 2-levels of CLA3 blocks
  C(0) <= Cin;

  --inputs: Cin, G0, G1, G2, P0, P1, P2
  C(1)  <= G(0)  OR (Cin AND P(0));
  C(2)  <= G(1)  OR (Cin AND P(0) AND P(1))           OR (G(0) AND P(1));
  P2(0) <=                   P(0) AND P(1) AND P(2);
  G2(0) <= G(2)                                       OR (G(0) AND P(1) and P(2))  OR (G(1) AND P(2));

  C(3)  <= G2(0) OR (Cin AND P2(0));

  --inputs: C3, G3, G4, G5, P3, P4, P5
  C(4)  <= G(3)  OR (C(3) AND P(3));
  C(5)  <= G(4)  OR (C(3) AND P(3) AND P(4))           OR (G(3) AND P(4));
  P2(1) <=                    P(3) AND P(4) AND P(5);
  G2(1) <= G(5)                                        OR (G(3) AND P(4) and P(5))  OR (G(4) AND P(5));

  C(6)  <= G2(1) OR (C(3) AND P2(1));

  -- 2nd level of CLA
  C2    <= G2(1) OR (Cin AND P2(0) AND P2(1)) OR (G2(0) AND P2(1));

  --inputs: C2, G6, G7, P6, P7
  C(7) <= G(6) OR (C2 AND P(6));
  Cout <=  SignedOverflow xor
           ( G(7) OR (C2 AND P(6) AND P(7)) OR (G(6) AND P(7)) );
  SUM <= X XOR C;

end CLA3;
