-- Wrapper.vhdl
-- Generated on 20230528-05:47
-- Do not modify

Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;
    use LibreGates.Netlist_lib.all;

entity Wrapper is
  generic (
    WrapperWidthIn : integer := 22;
    WrapperWidthOut: integer := 17;
    listgates: integer := 1;  -- set to 0 to display the raw list of gates
    main_delay : time := 1 sec;
    filename : string := ""; -- file of exclude vectors
    verbose : string := ""
  );
end Wrapper;

architecture Wrap of Wrapper is
  signal VectIn : std_logic_vector(WrapperWidthIn -1 downto 0);
  signal VectOut: std_logic_vector(WrapperWidthOut-1 downto 0);
  signal VectClk: std_logic := 'X';

begin

  -- force the registration of the gates.
  -- must be placed at the very beginning
  rg: entity register_generics generic map(
         gate_select_number => listgates, -- show gate listing when < 1
         bit_select_number => -1, -- no alteration
         flip_LUT => 2,           -- probe mode
         filename => filename,    -- eventual exclusions
         verbose => verbose)
  port map(clock => '0');   -- the updates are directly driven by Drive_DUT
  -- note : the corresponding procedure is not called
  -- at the right time if it is called directly,
  -- the entity ensures the correct calling order.

  Drive_DUT(VectIn, VectOut, VectClk, main_delay);

  -- Finally we "wire" the unit to the ports:
  tb: entity alu8
    port map(
      cin => VectIn(0),
      neg => VectIn(1),
      passmask => VectIn(2),
      orxor => VectIn(3),
      rop2mx => VectIn(4),
      cmps => VectIn(5),
      sri => VectIn(13 downto 6),
      snd => VectIn(21 downto 14),
      rop2 => VectOut(7 downto 0),
      sum => VectOut(15 downto 8),
      cout => VectOut(16));
end Wrap;
