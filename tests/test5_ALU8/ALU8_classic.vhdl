-- LibreGates/tests/test5_ALU8/ALU8_classic.vhdl
-- created mer. nov.  8 07:21:19 CET 2017 by Yann Guidon (whygee@f-cpu.org)
-- version ven. nov. 10 07:36:19 CET 2017 : built-in + operator used.
-- version jeu. nov. 16 02:21:35 CET 2017 : uses SLV shorthands from YGREC8/YGREC8_def.vhdl
-- version mer. mars 13 15:45:52 CET 2019 : swapped XOR and OR
-- version mer. mars 20 07:29:05 CET 2019 : SET/Pass re-implemented
-- version jeu. mars 21 08:51:50 CET 2019 : renamed SRC & DST
-- version lun. mars 25 01:00:02 CET 2019 : new ROP2 structure, all opcodes OK
-- version lun. avril 1 03:56:12 CEST 2019 : separate decoder
-- version dim. nov.  8 03:38:14 CET 2020 : moved the declaration in a dedicated file.
-- Released under the GNU AGPLv3 license
--
-- This is the arithmetic and logic unit of YGREC8,
-- performing the add/sub and boolean operations on 8 bits
-- with separate outputs for ROP2 and ADD.
-- This version is functionally accurate and will synthesize
-- but it is not optimal.
--
-- OPCODES:
-- F3 F2 F1 F0      tested
-- 0  0  0  0  XOR    *
-- 0  0  0  1  OR     *
-- 0  0  1  0  AND    *
-- 0  0  1  1  ANDN   *
-- 0  1  0  0  CMPU   *
-- 0  1  0  1  CMPS   *
-- 0  1  1  0  SUB    *
-- 0  1  1  1  ADD    *
-- 1  0  0  0  SET    *

Library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
Library work;
    use work.all;
Library YGREC8;
    use YGREC8.ygrec8_def.all;
Library SLV;
    use SLV.SLV_utils.all;

architecture classic of ALU8 is
  Signal ALU_XOR, G, NEG8, SNDX : SLV8;
  Signal sumAux : unsigned(9 downto 0);
  Signal SignedOverflow : SL;

begin
  -- Prepare the control signals:
  NEG8 <= (others=> NEG);
  SignedOverflow <= '1' when (SRI(7) /= SND(7)) and CMPS='1'
     else '0';

  -- Initial XOR of the operands
  SNDX <= SND XOR NEG8;

  -- ROP2
  ALU_XOR <= SRI  OR SNDX when OrXor='1'
        else SRI XOR SNDX;
  G       <= SRI when PassMask='1'
        else SRI AND SNDX;
  ROP2    <= ALU_XOR when ROP2mx='1'
        else G;

  -- ADD/SUB, the classic way:
  sumAux <= unsigned('0' & SRI & '1') + unsigned('0' & SNDX & Cin);
  SUM <= std_logic_vector(sumAux(8 downto 1));
  Cout <= std_logic(sumAux(9)) xor SignedOverflow;

end classic;
