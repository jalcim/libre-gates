-- LibreGates/tests/test5_ALU8/ALU8_tb.vhdl
-- version lun. nov. 13 01:43:33 CET 2017 by Yann Guidon (whygee@f-cpu.org)
-- version jeu. nov. 16 02:21:35 CET 2017 : uses SLV shorthands from YGREC8/YGREC8_def.vhdl
-- version jeu. mars 21 08:51:50 CET 2019 : renamed SRC & DST, test SET/pass
-- version lun. mars 25 06:10:20 CET 2019 : all opcodes pass
-- version dim. nov. 24 18:21:18 CET 2019 : adding fault injection
-- version mar. déc.  3 05:34:34 CET 2019 : some interface changes, folded-reverse scan, external list of excluded gates
-- version mer. nov. 11 14:20:25 CET 2020 : renamed some variable names to remove "declaration hides variable" warnings
--
--     13248331 simulation cycles in 383s to check all the faults with linear scanning
--      1121723 cycles in 36s with reverse-folding !
-- version jeu. déc.  5 04:56:17 CET 2019 :
--       931316 cycles only in 34s simply by swapping operands...

-- Released under the GNU AGPLv3 license

Library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
Library SLV;
    use SLV.SLV_utils.all;
Library	Histo;
    use	Histo.Histograms.all;
Library YGREC8;
    use YGREC8.ygrec8_def.all;
Library work;
    use work.all;
Library LibreGates;
    use LibreGates.all;
    use LibreGates.Gates_lib.all;
Library proasic3;
    use proasic3.all;

entity ALU8_tb is
-- added :
  generic (
    -- for the A3P library :
    gate_select_number: integer := 0;  -- when >= 1, select a gate to alter
    bit_select_number: integer := -1;  -- select which input bit code to alter
    flip: integer := -1;
       -- default : do nothing, don't instanciate register_generics
       -- clear to 0 to alter 0/1 to meta L/H code
       -- set to 1 to bit flip one bit of the LUT
    filename : string := ""; -- file containing the exclude vectors
    verbose : string := ""   -- set to anything to add some more messages.
  );
end ALU8_tb;

architecture plip of ALU8_tb is
  signal --CarryFlag,
      Cin, Cout, NEG, PassMask, OrXor, Rop2Mx, CMPS : SL := '0';
  signal F : SLV4 := "0000";
  signal SRI, SND, ROP2, SUM : SLV8 := "00000000";
  signal Inst : SLV16 := "0000000000000000";
-- added :
  signal clock : std_logic := '0';
begin
-- added :
  extra: if flip >= 0 generate
    -- this entity MUST be written first,
    -- at the very beginning of the toplevel file:
    rg: entity register_generics generic map(
         gate_select_number => gate_select_number,
         bit_select_number => bit_select_number,
         flip_LUT => flip,
         filename => filename,
         verbose => verbose)
        port map(clock => clock);
  end generate;

  Inst <= F & "000000000000";

  tbdec: entity DEC_ALU8 port map (
    Inst      => Inst,
--    CarryFlag => CarryFlag,
    CMPS      => CMPS,
    Rop2Mx    => Rop2Mx,
    OrXor     => OrXor,
    PassMask  => PassMask,
    NEG       => NEG,
    Cin       => Cin);

  tb: entity ALU8 port map (
     CMPS     => CMPS,
     Rop2Mx   => Rop2Mx,
     OrXor    => OrXor,
     PassMask => PassMask,
     NEG      => NEG,
     Cin      => Cin,
     SRI      => SRI,
     SND      => SND,
     SUM      => SUM,
     ROP2     => ROP2,
     Cout     => Cout);

  bench: process
    variable j, k, errors: integer := 0;
    variable expected : SLV8;

    procedure ShowSignals(i: integer) is
    begin
      report "Error at cycle#" & big_uint_t'image(cycle_counter)
             -- " F=" & SLV_to_bin(std_logic_vector(F)) & " " & 
           & " opc=" & OpcodeS_str_array(i)
           & " SRI=" & SLV_to_bin(SRI)
           & " SND=" & SLV_to_bin(SND)
           & " Cin=" &  std_logic'image(Cin)
           & " Cout=" & std_logic'image(Cout)
           & " SUM=" & SLV_to_bin(SUM)
           & " ROP2=" & SLV_to_bin(ROP2)
           & " expect=" & SLV_to_bin(expected)
           & " Diff=" & SLV_to_bin(ROP2 XOR expected);
--           & " Diff=" & SLV_to_bin(t(8 downto 0) XOR (Cout & SUM));

      -- "early quit" to stop the test as soon as one
      -- error is found and keep run times short
      assert flip /= 1
         severity failure;
      errors:=errors+1;
    end ShowSignals;

    procedure test_opcodes(pj, pk : integer) is
      variable j2, k2 : integer;
      variable C : SL;
      variable t : SLV10;
    begin
      -- Bool:
      F <= Op_AND;
      clock <= '0'; wait for 10 ns;
      clock <= '1'; wait for 10 ns;
      expected := SRI AND SND;
      if expected /= ROP2 then ShowSignals(0); end if;

      F <= Op_OR;
      clock <= '0'; wait for 10 ns;
      clock <= '1'; wait for 10 ns;
      expected := SRI OR SND;
      if expected /= ROP2 then ShowSignals(1); end if;

      F <= Op_XOR;
      clock <= '0'; wait for 10 ns;
      clock <= '1'; wait for 10 ns;
      expected := SRI XOR SND;
      if expected /= ROP2 then ShowSignals(2); end if;

      F <= Op_ANDN;
      clock <= '0'; wait for 10 ns;
      clock <= '1'; wait for 10 ns;
      expected := SRI AND (NOT SND);
      if expected /= ROP2 then ShowSignals(3); end if;

      -- CMPU:
      F <= Op_CMPU;
      clock <= '0'; wait for 10 ns;
      clock <= '1'; wait for 10 ns;
      if pk > pj then C := '0'; else C := '1'; end if;
      if C /= Cout then ShowSignals(4); end if;

      -- CMPS:
      F <= Op_CMPS;
      clock <= '0'; wait for 10 ns;
      clock <= '1'; wait for 10 ns;
      if pk > 127 then k2 := pk-256; else k2 := pk; end if;
      if pj > 127 then j2 := pj-256; else j2 := pj; end if;
      if k2 > j2 then C := '0'; else C := '1'; end if;
      if C /= Cout then ShowSignals(5); end if;

      -- SUB:
      F <= Op_SUB;
      clock <= '0'; wait for 10 ns;
      clock <= '1'; wait for 10 ns;
      t := std_logic_vector(to_unsigned((pj+512)-pk, 10));
      expected := t(7 downto 0);
      if t(8 downto 0) /= ( not Cout & SUM) then ShowSignals(6); end if;

      -- ADD:
      F <= Op_ADD;
      clock <= '0'; wait for 10 ns;
      clock <= '1'; wait for 10 ns;
      t := std_logic_vector(to_unsigned(pj+pk, 10));
      expected := t(7 downto 0);
      if t /= ('0' & Cout & SUM) then ShowSignals(7); end if;

      -- SET
      F <= Op_SET;
      clock <= '0'; wait for 10 ns;
      clock <= '1'; wait for 10 ns;
      expected := SRI;
      if expected /= ROP2 then ShowSignals(8); end if;

--      report " cycle#" & big_uint_t'image(cycle_counter) &
--         " : " & integer'image(pj) & " : " & integer'image(pk);

    end test_opcodes;

    procedure scan_linear is
--      variable j, k: integer := 0; loop counters
    begin
      for lj in 0 to 255 loop
        SRI <= std_logic_vector(to_unsigned(lj, 8));
        for lk in 0 to 255 loop
          SND <= std_logic_vector(to_unsigned(lk, 8));
          test_opcodes(lj, lk);
        end loop;
      end loop;
    end scan_linear;

-- 1121723 cycles
--    procedure reverse_folding_in(x : integer) is
--      variable j : integer := 1;  -- the current power of 2
--      variable k : integer := 0;  -- the inferior limit for the reverse scan
--      variable l : integer := 0;  -- the sub-loop counter for reverse scan
--    begin
--      SRI <= std_logic_vector(to_unsigned(x, 8));
--      loop
--	l := j;
--        loop
--          if (l < 128) then -- 128 appears 2x
--            SND <= std_logic_vector(to_unsigned(    l, 8));
--            test_opcodes(x,     l);
--            SND <= std_logic_vector(to_unsigned(255-l, 8));
--            test_opcodes(x, 255-l);
--          end if;
--          l := l-1;
--          exit when l < k;
--        end loop;
--        k := j+1;
--        j := j+j;
--        exit when j > 128;
--      end loop;
--    end reverse_folding_in;

    -- 931316 cycles !
    procedure reverse_folding_in2(x : integer) is
      variable vj : integer := 1;  -- the current power of 2
      variable vk : integer := 0;  -- the inferior limit for the reverse scan
      variable vl : integer := 0;  -- the sub-loop counter for reverse scan
    begin
      SND <= std_logic_vector(to_unsigned(x, 8));
      loop
        vl := vj;
        loop
          if (vl < 128) then -- 128 appears 2x
            SRI <= std_logic_vector(to_unsigned(    vl, 8));
            test_opcodes(    vl, x);
            SRI <= std_logic_vector(to_unsigned(255-vl, 8));
            test_opcodes(255-vl, x);
          end if;
          vl := vl-1;
          exit when vl < vk;
        end loop;
        vk := vj+1;
        vj := vj+vj;
        exit when vj > 128;
      end loop;
    end reverse_folding_in2;


    procedure reverse_folding_out is
      variable vj : integer := 1;  -- the current power of 2
      variable vk : integer := 0;  -- the inferior limit for the reverse scan
      variable vl : integer := 0;  -- the sub-loop counter for reverse scan
    begin
      loop
	vl := vj;
        loop
          if (vl < 128) then -- 128 appears 2x
            reverse_folding_in2(    vl);
            reverse_folding_in2(255-vl);
--            reverse_folding_in(    vl);
--            reverse_folding_in(255-vl);
          end if;
          vl := vl-1;
          exit when vl < vk;
        end loop;
        vk := vj+1;
        vj := vj+vj;
        exit when vj > 128;
      end loop;
    end reverse_folding_out;


  begin
-- added :
    if gate_select_number=-1 then
      wait;  -- just display the list of gates
    end if;

--    scan_linear;
    reverse_folding_out; -- 10x faster !

    assert errors = 0
      report integer'image(errors) & " UNEXPECTED RESULTS "
      severity failure;

-- added :
    clock<='X'; -- display the histogram
    wait;
  end process;

end plip;
