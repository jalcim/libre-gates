-- LibreGates/tests/test5_ALU8/dec_ALU8_RTL_bool.vhdl
-- created ven. mars  29 11:09:52 CET 2019  by Yann Guidon (whygee@f-cpu.org)
-- version lun. avril  1 02:54:34 CEST 2019
--
-- pre-decodes, amplifies and eventually
-- latches "vertical" control signals for the ALU8
-- See https://hackaday.io/project/27280-ygrec8/log/161090-making-y8-more-energy-efficient-with-a-deglitcher
--
-- This version will synthesise but is not optimal,
-- though it's ok for simulation.
--
-- Released under the GNU AGPLv3 license

-- OPCODES:
-- F3 F2 F1 F0      tested  Pass NEG OrXor ROP2mx Cin CMPS
-- 0  0  0  0  AND    *       0   0           0
-- 0  0  0  1  OR     *           0    1      1
-- 0  0  1  0  XOR    *           0    0      1
-- 0  0  1  1  ANDN   *       0   1           0
-- 0  1  0  0  CMPU   *           1    0           1    0
-- 0  1  0  1  CMPS   *           1    0           1    1
-- 0  1  1  0  SUB    *           1    0           1    0
-- 0  1  1  1  ADD    *           0    0           !    0
-- 1  0  0  0  SET    *       1               0

Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library YGREC8;
    use YGREC8.ygrec8_def.all;
Library SLV;
    use SLV.SLV_utils.all;

entity DEC_ALU8 is
  port(
    Inst: in SLV16;
    Cin, NEG, PassMask, OrXor, Rop2Mx, CMPS : out SL);
end DEC_ALU8;

architecture RTL_bool of DEC_ALU8 is
  alias Opcode: SLV4 is Inst(15 downto 12);
  alias Imm4: SLV4 is Inst(6 downto 3);
  alias FlagIm8: SL is Inst(11);
  alias FlagRI4: SL is Inst(10);
begin
  PassMask <= '1' when  Opcode=Op_SET
         else '0';

  NEG <= '1' when  (Opcode=Op_ANDN
                 or Opcode=Op_CMPU
                 or Opcode=Op_CMPS
                 or Opcode=Op_SUB)
     else '0';

  OrXor <= '1' when  Opcode=Op_OR
      else '0';

  Rop2Mx <= '1' when (Opcode=Op_OR
                  or  Opcode=Op_XOR)
       else '0';

  CMPS <= '1' when  Opcode=Op_CMPS
     else '0';

  Cin <= '1' when  Opcode=Op_CMPU
               or  Opcode=Op_CMPS
               or  Opcode=Op_SUB
               or (Opcode=Op_ADD and FlagIm8='0' and FlagRI4='1' and Imm4="0000")
    else '0';

end RTL_bool;
