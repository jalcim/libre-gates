-- LibreGates/tests/test5_ALU8/dec_ALU8_fast_A3P.vhdl
-- created ven. mars  29 11:09:52 CET 2019  by Yann Guidon (whygee@f-cpu.org)
-- version lun. avril  1 02:54:34 CEST 2019
-- version jeu. avril  4 02:47:34 CEST 2019
-- version jeu. avril  4 23:56:04 CEST 2019
--
-- pre-decodes, amplifies and eventually
-- latches "vertical" control signals for the ALU8
-- See https://hackaday.io/project/27280-ygrec8/log/161090-making-y8-more-energy-efficient-with-a-deglitcher
--
-- Fast/simple version of the decoder, mapped to A3P gates,
-- that will toggle the outputs a lot.
--
-- Released under the GNU AGPLv3 license

-- OPCODES:
-- F3 F2 F1 F0      tested  Pass NEG OrXor ROP2mx Cin CMPS
-- 0  0  0  0  AND    *       0   0           0
-- 0  0  0  1  OR     *           0    1      1
-- 0  0  1  0  XOR    *           0    0      1
-- 0  0  1  1  ANDN   *       0   1           0
-- 0  1  0  0  CMPU   *           1    0           1    0
-- 0  1  0  1  CMPS   *           1    0           1    1
-- 0  1  1  0  SUB    *           1    0           1    0
-- 0  1  1  1  ADD    *           0    0           !    0
-- 1  0  0  0  SET    *       1               0

Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library SLV;
    use SLV.SLV_utils.all;
Library proasic3;
    use proasic3.all;

entity DEC_ALU8 is
  port(
    Inst: in SLV16;
    Cin, NEG, PassMask, OrXor, Rop2Mx, CMPS : out SL);
end DEC_ALU8;

architecture RTL_bool of DEC_ALU8 is
  alias F3: SL is Inst(15);
  alias F2: SL is Inst(14);
  alias F1: SL is Inst(13);
  alias F0: SL is Inst(12);
  alias Opcode: SLV4 is Inst(15 downto 12);
  alias Imm4: SLV4 is Inst(6 downto 3);
  alias FlagIm8: SL is Inst(11);
  alias FlagRI4: SL is Inst(10);
  signal Cin_A, Cin_B, Cin_C : SL;
begin
  Dec1: entity AX1C   port map(A=> F0, B=>F1, C=>F2,  Y=>NEG   ); --  NEG    <= (F0 and F1) xor F2;
  PassMask <=  F3;
  Dec2: entity NOR2   port map(A=> F1, B=>F2,         Y=>OrXor ); --  OrXor  <= not  (F1   or F2);
  Dec3: entity XOR2   port map(A=> F0, B=>F1,         Y=>Rop2Mx); --  Rop2Mx <= F0 xor F1;
  Dec4: entity AND3A  port map(A=> F1, B=>F0, C=>F2,  Y=>CMPS  ); --  CMPS   <= F0 and not F1 and F2;
     -- in the unit testbench,  X-X-X-X-

  Dec5: entity NAND3  port map(A=> F0, B=>F1, C=>F2,  Y=> Cin_A);                 -- Cin_A <= not (F0 and F1 and F2); --=> Opcode=Op_ADD
     -- in the unit testbench,  X-X-X-X-
  Dec6: entity NOR3   port map(A=> Imm4(0), B=> Imm4(1), C=> Imm4(2), Y=> Cin_B); -- Cin_B <= not (Imm4(0) or Imm4(1) or Imm4(2));
  Dec7: entity NOR3A  port map(A=> FlagRI4, B=> Imm4(3), C=> FlagIm8, Y=> Cin_C); -- Cin_C <= not ((not FlagRI4) or Imm4(3) or FlagIm8);
  Dec8: entity AO1    port map(A=> Cin_B,   B=> Cin_C,   C=>  Cin_A,  Y=> Cin  ); -- Cin   <= Cin_A or (Cin_B and Cin_C);
                         --  Cin      <=  not (F0 and F1 and F2)    -- Cin=0 when Opcode=Op_ADD
                         --               or (not FlagIm8 and FlagRI4 and not (Inst(6) or Inst(5) or Inst(4) or Inst(3)));
                         --                   -- except when FlagIm8='0' and FlagRI4='1' and Imm4="0000"
end RTL_bool;
