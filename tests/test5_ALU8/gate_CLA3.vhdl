-- YGREC8/gate_CLA3.vhdl
-- created jeu. mars 28 20:25:29 CET 2019 by Yann Guidon (whygee@f-cpu.org)
-- Released under the GNU AGPLv3 license
--
-- This "complex gate" is an important building block for a Carry-Lookahead
-- adder circuit, here are several versions :

Library ieee;
    use ieee.std_logic_1164.all;
Library work;
    use work.all;
Library SLV;
    use SLV.SLV_utils.all;
Library proasic3;
    use proasic3.all;

entity CLA3 is
  port( A, B, C, D, E : in SL;
    Y : out SL);
end CLA3;

architecture RTL of CLA3 is
  Signal T2, T3: SL;
begin
  Y <= (A AND B  AND C)
    OR (A AND D)
    OR E;
end RTL;

architecture A3P of CLA3 is
  Signal T2, T3: SL;
begin
  cla3_a: entity AND2 port map(A=>A,  B=>D,  Y=>T2); -- T2<=A AND D;
  cla3_b: entity AND3 port map(A=>A,  B=>B,  C=>C, Y=>T3); -- T3<=A AND B  AND C;
  cla3_c: entity OR3  port map(A=>T2, B=>T3, C=>E, Y=>Y);  -- Y<=T2 OR T3 OR E;
end A3P;

architecture ASIC of CLA3 is
  Signal T2, T3: SL;
begin
  cla3_a: entity NAND2  port map(A=>A, B=>D,         Y=>T2); -- T2<=not (A AND D);
  cla3_b: entity NAND3  port map(A=>A, B=>B,  C=>C,  Y=>T3); -- T3<=not (A AND B AND C);
  cla3_c: entity NAND3A port map(A=>E, B=>T2, C=>T3, Y=>Y);  --  Y<=not (T2 AND T3 and not E);
end ASIC;
