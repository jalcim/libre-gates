#!/bin/bash
# file LibreGates/test5_ALU8/test.sh
# created by Yann Guidon / whygee@f-cpu.org
# version ven. nov. 10 03:25:21 CET 2017
# version lun. nov. 13 02:36:33 CET 2017
# version jeu. nov. 16 03:07:00 CET 2017
# version lun. avril 1 02:58:19 CEST 2019
# version sam. nov. 23 20:22:07 CET 2019 : integration in A3P library
# version sam. nov.  7 09:48:07 CET 2020 : includes the new wrapper generator
# version ven. nov. 13 11:58:04 CET 2020 : use parallel.sh

echo -e '\nThis script runs exhaustive self-tests and will take "a while".\nFor your convenience, I tried to run tasks in parallel.\n'

PA3="-P$PWD/../../LibreGates -P$PWD/../../LibreGates/proasic3 -P$PWD/../Y8"

run_test () {
# $1 = MSG_name
# $2 = ALU file
# $3 = DEC file
  rm -rf $1 &&
  mkdir $1 &&
  cd $1 &&
  echo "Starting ALU $1" &&
  ghdl -a $PA3 ../gate_CLA3.vhdl ../$3 ../ALU8.decl.vhdl ../$2 ../ALU8_tb.vhdl &&
  ghdl -e $PA3 ALU8_tb &&
  ./alu8_tb &&
  cd .. &&
  rm -rf $1 &&
  echo "ALU $1 OK"
}

[ -f ../../LibreGates/proasic3/simple/proasic3-obj93.cf ] &&

# Available decoders :
#../DEC/dec_ALU8_RTL_bool.vhdl  => simple version, ok pour simulation
##../DEC/dec_ALU8_fast_bool.vhdl => minimal logic, basic equations
#../DEC/dec_ALU8_fast_A3P.vhdl  => minimal and fastest logic, mapped to A3P but toggles a lot
##../DEC/dec_ALU8_latch_RTL.vhdl => version with toggle reduction
##../DEC/dec_ALU8_latch_bool.vhdl =>
### Some versions are not included in the A3P test suite.
### Check in the YGREC8 source archives if you need them.

# Available ALU versions:
# ALU8_classic.vhdl => basic version using standard adder
# ALU8_CLA.vhdl     => radix-3 Carry Lookahead Adder
# ALU8_A3P.vhdl     => like above and mapped to A3P tiles

{
run_test "simple_bool"    "ALU8_classic.vhdl" "dec_ALU8_RTL_bool.vhdl" &
run_test "CLA_bool"       "ALU8_CLA.vhdl"     "dec_ALU8_RTL_bool.vhdl" &
run_test "A3P_bool"       "ALU8_A3P.vhdl"     "dec_ALU8_RTL_bool.vhdl" &
run_test "A3P_fast_A3P"   "ALU8_A3P.vhdl"     "dec_ALU8_fast_A3P.vhdl" &
} && wait &&

rm -f alu8 alu8_tb work-obj93.cf *.o gatelist.log &&

ghdl -a $PA3 gate_CLA3.vhdl dec_ALU8_fast_A3P.vhdl ALU8.decl.vhdl ALU8_A3P.vhdl ALU8_tb.vhdl &&
ghdl -e $PA3 ALU8_tb &&

echo -n -e "\nThen we check that all the errors can be found " &&
. ../../LibreGates/parallel.sh &&

# disable test of some gates
function fix_lut() {
  declare -a ar=( $* )
  for i in 3 4 5 6 7 ; do
    ar[i]=2
  done
  echo ${ar[*]}
} &&

{ ScanErrors ./alu8_tb fix_lut || {
    echo $'\e[31m'"Something wrong happened !"$'\e[0m'
    false
  }
} &&

# 1 Thread: 34s
# 2 Thread: 20s
# 3 Thread: 18s
# 4 Thread: 18s due to 3 threads that take a long time...

echo -e "\nFinally let's extract the netlist :" &&
rm -f work-obj93.cf *.o alu8 &&
../../LibreGates/wrap_netlist.sh $PA3 ALU8.decl.vhdl > ALU8_Wrapped.vhdl &&
ghdl -a $PA3 gate_CLA3.vhdl dec_ALU8_fast_A3P.vhdl ALU8_A3P.vhdl ALU8_Wrapped.vhdl &&
ghdl -e $PA3 Wrapper &&
../../LibreGates/census.sh &&

echo $'\e[32m'"ALU8 : OK"$'\e[0m'
