-- LibreGates/tests/test6_bigLFSR/LFSR32x.decl.vhdl
-- created mer. nov. 18 12:52:07 CET 2020
-- version ven. nov. 20 18:36:21 CET 2020 : fork/declaration
--
-- Just the declaration of a benchmark circuit that
-- stacks an arbitrary number of LFSR slices.

Library ieee;
    use ieee.std_logic_1164.all;
Library SLV;
    use SLV.SLV_utils.all;

entity LFSR32x is
  generic (
    layers : positive := 10
  );
  port(
    A : in  SLV32;
    Y : out SLV32);
end LFSR32x;
