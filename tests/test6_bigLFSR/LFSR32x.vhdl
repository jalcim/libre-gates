-- LibreGates/tests/test6_bigLFSR/LFSR32x.vhdl
-- created mer. nov. 18 12:52:07 CET 2020
-- version ven. nov. 20 18:36:21 CET 2020 : fork/declaration
--
-- This benchmark circuit stacks an arbitrary number
-- of LFSR slices, with poly 0xFFFFFFFA, to create
-- a huge network of XOR. This tests the scalability
-- of GHDL and the LibreGates library.
--
-- Period is 2^32-1 = 4294967295
--    = 3 x 5 x 17 x 257 x 65537
-- Minimal size of this circuit is : ./mlfsr -glayers=2
--   which creates 3 layers of XOR, thus a period of
--   4294967295/3=1431655765
--  (but who really cares ?)

Library ieee;
    use ieee.std_logic_1164.all;
Library SLV;
    use SLV.SLV_utils.all;
Library proasic3;
    use proasic3.all;

architecture lateral of LFSR32x is
  subtype SLVX is std_logic_vector(layers downto 0);
  type ArSLV29 is array (28 downto 0) of SLVX;
  signal XOV : ArSLV29;
begin

  -- Connect the inputs : 3 layers of XOR are required

  -- L1
  x01: entity xor2 port map(A=>A(0), B=>A(1), Y=>XOV(0)(0));
  l0: for k in 1 to 28 generate
    xl0: entity xor2 port map(A=>A(0), B=>A(k+2), Y=>XOV(k)(0));
  end generate;
  -- L2
  x11: entity xor2 port map(A=>XOV(0)(0), B=>A(2), Y=>XOV(0)(1));
  x13: entity xor2 port map(A=>XOV(0)(0), B=>A(31), Y=>XOV(28)(1));
  -- L3
  x21: entity xor2 port map(A=>XOV(1)(0), B=>XOV(1)(0), Y=>XOV(0)(2));
  x23: entity xor2 port map(A=>XOV(1)(0), B=>A(0), Y=>XOV(28)(2));

  l1: for l in 1 to layers generate
    l2: for k in 1 to 27 generate
      xl1: entity xor2 port map(A=>XOV(0)(l-1), B=>XOV(k+1)(l-1), Y=>XOV(k)(l));
    end generate;
    l3: if l > 2 generate
      xl2: entity xor2 port map(A=>XOV(0)(l-1), B=>XOV(  1)(l-2), Y=>XOV( 0)(l));
      xl3: entity xor2 port map(A=>XOV(0)(l-1), B=>XOV(  0)(l-3), Y=>XOV(28)(l));
    end generate;
  end generate;
  -- "Puh, das war harter Stoff!"

  -- Connect the outputs :
  Y(0)  <= XOV(0)(layers-1);
  Y(1)  <= XOV(0)(layers  );
  Y(2)  <= XOV(1)(layers-1);
  lo: for o in 1 to 28 generate
    Y(o+2) <= XOV(o)(layers);
  end generate;
  Y(31) <= XOV(0)(layers-2);

end lateral;
