File LibreGates/tests/test6_bigLFSR README.txt
created mer. nov. 18 11:36:24 CET 2020

This directory is a benchmark to evaluate the performance profile
and the behaviours of GHDL and the various versions of the libraries
when the design size scales up.

Many design choices, details and tricks are explained at
https://hackaday.io/project/174585/log/185901-chasing-a-unexpected-feature-in-ghdl
https://hackaday.io/project/174585/log/186169-benchmarking-with-a-huge-lfsr

A homogeneous and scalable circuit is chosen, based on a LFSR slice
that is replicated as many times as desired. The ingredients are
XOR2 gates and intermediary signals. As explained in one log,
GHDL "prefers" that signals are organised along the longest possible
vector so the code will look a bit weird at first,
with columns and rows swapped.

The chosen LFSR poynomial is 0xFFFFFFFA because it contains
the most inverting terms (29) for the chosen width of 32 taps,
and a proof code is provided in lfsr.c.
A scaled-down version with 8 taps and poly 0xFA is shown
in the pictures, the 32-bits circuit is identical but
with 24 more continuous XORs.

The number of slice-to-slice assignations is reduced by
reading values in slices further in the "past", when
no inversion term is present. So if you set the slice number
to 1000, you get 29000 XOR and 29000 std_logic.

This test is optional and not run by default because

  1) the results and behaviour depend on the machine:
setting the size too high can nearly crash the system.

  2) the primary motivation was to estimate the overhead
difference between the "simple" and the "detailed" gates,
as well as verify the scalability of the netlist tools.
When weird things appear, they are easier to debug with
an array of XOR is rather than a full CPU core.

When available, gnuplot plots the results to ensure
the behaviour (time vs size and size vs size) is linear.
