
# LibreGates/tests/test6_bigLFSR/benchmark.sh
# created ven. nov. 20 17:26:52 CET 2020 by whygee@f-cpu.org
# version dim. nov. 22 13:31:53 CET 2020 : gnuplot !

function iterate() {
  # $1 : program to run
  # $2 : destination file
  for i in \
     10    14    20    30    40    60    80 \
     100   140   200   300   400   600   800 \
     1000  1400  2000  3000  4000  6000  8000 \
     10000 14000 20000 #30000 40000 60000 80000 \
  do
    echo -n $i " "
    /usr/bin/time -a -o $2 -f "$(( (i+1)*29 )) %M %E" $1 -glayers=$i || return
  done
}

function run_test() {
  rm -f mlfsr *.cf *.o
  ghdl -a $PA3 LFSR32x.decl.vhdl LFSR32x.vhdl &&
  ghdl -e $PA3 LFSR32x  &&
  iterate ./lfsr32x $1
}

[ -f ../../LibreGates/proasic3/simple/proasic3-obj93.cf ] &&

export DRIVE_DUT="Drive_DUT_dummy" &&
rm -f run?.log &&

echo "Simple gate version setup (RAM+time) :" &&
PA3="-P$PWD/../../LibreGates -P$PWD/../../LibreGates/proasic3/simple" &&
run_test run1.log &&
echo &&
echo "Detailed gate version setup (RAM+time) :" &&
PA3="-P$PWD/../../LibreGates -P$PWD/../../LibreGates/proasic3" &&
run_test run2.log &&
echo &&
../../LibreGates/wrap_netlist.sh $PA3 LFSR32x.decl.vhdl > LFSR32x_Wrapped.vhdl &&
rm -f lfsr32x *.cf *.o
ghdl -a $PA3 LFSR32x.decl.vhdl LFSR32x.vhdl LFSR32x_Wrapped.vhdl &&
ghdl -e $PA3 Wrapper &&
iterate ./wrapper run3.log &&

echo $'\e[32m'"Benchmark : OK"$'\e[0m' &&
# optional display:
W=$(which gnuplot) &&
gnuplot -p < run_size.gplt &&
gnuplot -p < run_time.gplt
