/*
  file lfsr.c

  This file implements a 32-bit LFSR that
  generates about 4 billions bits before wraparound.

  gcc -o lfsr lfsr.c && ./lfsr |less

 */

#include <stdio.h>

#define U32 unsigned long int

#define LFSR_POLY 0xFFFFFFFA

U32 LFSR_reg = 0x1; /*2345678; anything except zero */

U32 lfsr() {
  U32 u=LFSR_reg;
  if (LFSR_reg & 1)
    u ^= LFSR_POLY;
  LFSR_reg = (u >> 1) | (u << 31);
  return LFSR_reg;
}

unsigned char binmsg[] = "00000000000000000000000000000000\n";
char * bin_cvt(int n) {
  int k=31;
  do {
    binmsg[k--] = '0'+ (n&1);
    n = n >> 1;
  } while (k >= 0);
  return binmsg;
}

int main() {
  int i, j=0, k;

  for (i=0; i<3000; i++) {
    printf(bin_cvt(lfsr()));
  }
  return 0;
}
