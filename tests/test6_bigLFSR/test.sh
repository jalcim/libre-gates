
# LibreGates/tests/test6_bigLFSR/test.sh
# created ven. nov. 20 17:26:52 CET 2020 by whygee@f-cpu.org
# version mer. déc.  9 06:10:33 CET 2020

[ -f ../../LibreGates/proasic3/simple/proasic3-obj93.cf ] &&

rm -f mlfsr Wrapper lfsr32x *.cf *.o &&
PA3="-P$PWD/../../LibreGates -P$PWD/../../LibreGates/proasic3" &&
export DRIVE_DUT="Drive_DUT" &&
../../LibreGates/wrap_netlist.sh $PA3 LFSR32x.decl.vhdl > LFSR32x_Wrapped.vhdl &&
ghdl -a $PA3 LFSR32x.decl.vhdl LFSR32x.vhdl LFSR32x_Wrapped.vhdl &&
ghdl -e $PA3 Wrapper &&
./wrapper -glayers=4 | sed 's/.*note): //' &&

echo $'\e[32m'"BigLFSR : OK"$'\e[0m'
